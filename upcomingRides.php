<?php 
	
	include_once('header.php');
?>



        
        <div class="responsive-header">
            <div class="res-top">
                <ul>
                    <li><a href="#" title="Home"><i class="flaticon-home"></i></a></li>
                    <li><a href="#" title="Faq's"><i class="flaticon-info"></i></a></li>
                    <li><a href="#" title="Support center"><i class="flaticon-support"></i></a></li>
                    <li><a href="#" title="Login"><i class="flaticon-unlocked"></i></a></li>
                    <li><a href="#" title="New register"><i class="flaticon-checked"></i></a></li>
                    <li class="post-new"><a href="#" title="New post">+Post</a></li>
                </ul>
            </div>
            <div class="logomenu-bar">
                <div class="logo"><a href="index.html" title=""><img src="images/logo-black.png" alt=""></a></div>
                <span class="responsviemenu-btn"><i class="flaticon-menu-1"></i></span> 
            </div>
            <div class="responsive-menu">
                <span class="close-btn"><i class="flaticon-error-1"></i></span>
                <ul>
                    <li><a href="index.html" title="">Home</a></li>
                    <li class="menu-item-has-children"><a href="#" title="">cars</a>
                        <ul class="sub-menu">
                            <li><a href="index-after-login.html" title="">page after login</a></li>
                            <li><a href="cars.html" title="">cars</a></li>
                            <li><a href="car-profile.html" title="">car profile</a></li>
                            <li><a href="car-detail.html" title="">cars detail</a></li>
                            <li><a href="create-route-notification.html" title="">route notification</a></li>
                            <li><a href="submit-car-intro.html" title="">Submit car introduction</a></li>
                            <li><a href="submit-car-for-rent.html" title="">submit car for rent</a></li>
                            <li><a href="submit-route.html" title="">submit car route</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children"><a href="#" title="">rides</a>
                        <ul class="sub-menu">
                            <li><a href="ride-listing.html" title="">ride listing</a></li>
                            <li><a href="ride-request.html" title="">ride request</a></li>
                            <li><a href="ride-detail.html" title="">ride detail</a></li>
                            <li><a href="ride-requests-list.html" title="">ride request list</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children"><a href="#" title="">leasing</a>
                        <ul class="sub-menu">
                            <li><a href="leasing.html" title="">Leasing Page</a></li>
                            <li><a href="leasing-car-step1.html" title="">leasing Step 1</a></li>
                            <li><a href="leasing-car-step2.html" title="">leasing step 2</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children"><a href="#" title="">profiles</a>
                        <ul class="sub-menu">
                            <li><a href="user-profile.html" title="">Profile user</a></li>
                            <li><a href="user-profile-v2.html" title="">Profile user v2</a></li>
                            <li><a href="my-profile.html" title="">My profile</a></li>
                            <li><a href="profile-balance.html" title="">profile balance</a></li>
                            <li><a href="profile-earning.html" title="">profile earning</a></li>
                            <li><a href="profile-edit.html" title="">profile edit page</a></li>
                            <li><a href="profile-invite-friend.html" title="">profile invite friend</a></li>
                            <li><a href="profile-points.html" title="">profile points</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children"><a href="#" title="">more pages</a>
                        <ul class="sub-menu">
                            <li><a href="login.html" title="">Login page</a></li>
                            <li><a href="register.html" title="">Register Page</a></li>
                            <li><a href="how-this-work.html" title="">how it's work</a></li>
                            <li><a href="faq.html" title="">faq's</a></li>
                            <li><a href="points.html" title="">poinst</a></li>
                            <li><a href="coming-soon.html" title="">Coming Soon</a></li>
                            <li><a href="404.html" title="">404 Page</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div><!-- Responsive Header -->
        
		<div class="sub-head blackish ext-low-opacity">
			<div class="bg-image" style="background-image: url(images/resources/subhead-bg.jpg)"></div>
        	<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="cola-head">
							<div class="head-meta">
								<h4><?php echo $data['fullName'];?></h4>
								<p>(<?php echo $data['city'];?>)</p>
							</div>
							
							<div class="info-links">
								<a href="how-this-work.php" title="">how does this work</a>
								<a href="vehicleOwnerSignup.php" title="">submit your car</a>
								
							</div>
						</div>
					</div>
				</div>
			</div>
        </div><!-- subhead -->
		
        
        
        <section>
        	<div class="space-70">
        		<div class="container">
        			<div class="row">
        				<div class="col-lg-12 col-md-12">
							
								<a href="rideHistory.php"><button class="button theme-btn">Ride History -></button></a>
							
							
        					<div class="heading text-left no-margin">
                            	<h5>Upcoming rides</h5>
                            </div>
							
                            <div class="rides-list">
                            	<div class="more-find">
                                	
                                </div>
								<?php
									
									$sql = "SELECT * FROM newbooking 
											WHERE customerId = '".$data['customerId']."' 
											ORDER BY rideStart DESC ";
									
									$ans = mysqli_query($obj->con,$sql);
									
									while($row = mysqli_fetch_array($ans))
									{
										
										$sql_vehicle = "SELECT vt.*,vct.*,vtb.*,vtd.*,vtm.*,vta.*,vts.* ,vtf.*,ot.mobileNumber,ot.firstName,ot.lastName
															
												FROM  vehicle_tbl vt 
												
												LEFT JOIN owner_tbl ot
												ON vt.owner_id = ot.vehicleOwnerId
												
												LEFT JOIN vehicle_category_tbl vct
												ON vt.vehicle_category_id = vct.vehicle_category_id 
												
												LEFT JOIN vehicle_type_tbl vtb
												ON vt.vehicle_type_id = vtb.vehicle_type_id 
												
												LEFT JOIN vehicle_tbl_documents vtd
												ON vt.vehicleId = vtd.vehicleId
									
												LEFT JOIN vehicle_tbl_model vtm 
												ON vt.vehicleId = vtm.vehicleId
									
												LEFT JOIN vehicle_tbl_schedule vts
												ON vt.vehicleId = vts.vehicleId 
												
												LEFT JOIN vehicle_tbl_features vtf
												ON vt.vehicleId = vtf.vehicleId 
												
												LEFT JOIN vehicle_tbl_address vta
												ON vt.vehicleId = vta.vehicleId 
															
												WHERE vt.delete_status = 0 
												AND vt.vehicleId = '".$row['vehicleId']."'
												
												LIMIT 1";
											
											$ans_vehicle = mysqli_query($obj->con,$sql_vehicle);
											$row_vehicle = mysqli_fetch_array($ans_vehicle); 
								?>
								<style>
										.activeBlock {
											background-color: lightgreen;
										}
								</style>
								<?php
									if($row['rideStart'] == 1)
									{
										echo "<div class='rides-listing activeBlock' >";
									}
									
									else
									{
										echo "<div class='rides-listing ' >";
									}
								?>
                                
								
                                	<div class="rider-imag">
                                        <img alt="" src="./vehicle/<?php echo $row_vehicle['photo1_link'];?>">
                                        <i data-toggle="tooltip" title="This user has a rating of 5 stars at least on 3 rides and phone number verified."><img alt="" src="images/icon-14.png"></i>
                                        <div class="rating">
                                            <em><?php echo $row_vehicle['firstName']." ".$row_vehicle['lastName'];?></em>
											<p class="fa fa-phone"> <?php echo $row_vehicle['mobileNumber'];?></p>
                                            <ul class="stars-rating">
                                            	<li><i class="fa fa-star checked"></i></li>
                                                <li><i class="fa fa-star checked"></i></li>
                                                <li><i class="fa fa-star checked"></i></li>
                                                <li><i class="fa fa-star checked"></i></li>
                                                <li><i class="fa fa-star checked"></i></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="rides-meta">
                                    	<h4><?php echo $row['start_date'];?> at <?php echo $row['start_time'];?></h4>
                                        <span>
                                        	<i class="fa fa-location-arrow"></i>
                                            <em><?php echo $row_vehicle['door_number']." , ".$row_vehicle['street']." , ".$row_vehicle['city'];?></em>
											<em>OTP : <?php echo  $row['startOtp']; ?></em>
                                          
                                        </span>
										
                                        <p></p>
                                        
										
											<?php 
												if($row['rideStart'] == 1)
												{
													echo "<p>";
													echo "Starting Kilometer : ".$row['start_km'];
													echo "</p>";
												}
											?>
										
										
											<?php 
												if($row['rideStart'] == 1)
												{
													echo "<p class='fa fa-phone'>";
													echo " Emergency Assistance Number : 9537371017";
													echo "</p>";
												}
											?>
										
                                        <i class="fa fa-warning" data-toggle="tooltip" title="10km fare pelenty if ride is booked within next 3 days range."> <a href="dbclass.php?cancelid=<?php echo $row['booking_id'];?>" onclick="return confirm('Are you sure you want to cancel booking?')">Cancel Ride</a></i>
										
                                    </div>
                                    <div class="price-tag">
									<font style="display:inline-block;font-size:30px;font-style:normal;padding-bottom:18px;color:#F76162;">
									
                                    	<em>&#8377;<?php echo $row['rate'];?>/km</em><br />
									</font>
									<font style="display:inline-block;font-size:30px;font-style:normal;padding-bottom:18px;color:green;">
										<?php
											if($row['rideStart'] == 1)
											{
												echo "<em>Ongoing ride</em>";
											}
										?>
                                    </font>  
                                    </div>
									
								
                                </div>
								
                               <?php
									}
								?> 
                            </div>
                            
        				</div>
                   
        			</div>
        		</div>
        	</div>
        </section>

      
<?php
	include_once('footerTop.php');
	include_once('footer.php');
?>