<?php 
	include_once('header.php');
	
	$sql_vehicleDetail = "SELECT vt.*,vct.*,vtb.*,vtd.*,vtm.*,vta.*,vts.* ,vtf.*,ot.*,ft.*,
						vta.city AS vct_city,vta.street AS street
															
						FROM  vehicle_tbl vt 
						
						LEFT JOIN vehicle_category_tbl vct
						ON vt.vehicle_category_id = vct.vehicle_category_id 
						
						LEFT JOIN vehicle_type_tbl vtb
						ON vt.vehicle_type_id = vtb.vehicle_type_id 
						
						LEFT JOIN vehicle_tbl_documents vtd
						ON vt.vehicleId = vtd.vehicleId
			
						LEFT JOIN vehicle_tbl_model vtm 
						ON vt.vehicleId = vtm.vehicleId
			
						LEFT JOIN vehicle_tbl_schedule vts
						ON vt.vehicleId = vts.vehicleId 
						
						LEFT JOIN vehicle_tbl_features vtf
						ON vt.vehicleId = vtf.vehicleId 
						
						LEFT JOIN vehicle_tbl_address vta
						ON vt.vehicleId = vta.vehicleId 
						
						LEFT JOIN owner_tbl ot
						ON ot.vehicleOwnerId = vt.owner_id 
						
						LEFT JOIN fueltype_tbl ft
						ON ft.fuelType_id = vtf.fuelType_id 
						
						WHERE vt.vehicleId = '".$_COOKIE['bookVehicleId']."' ";
	//die($sql_vehicleDetail);
	$ans_vehicleDetail = mysqli_query($obj->con,$sql_vehicleDetail);
	$row_vehicleDetail = mysqli_fetch_array($ans_vehicleDetail);
		
?>
<style>
	#customers {
	  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
	  border-collapse: collapse;
	  width: 100%;
	}
	
	#customers td, #customers th {
	  border: 1px solid #ddd;
	  padding: 8px;
	}
	
	#customers tr:nth-child(even){background-color: #f2f2f2;}
	
	#customers tr:hover {background-color: #ddd;}
	
	#customers th {
	  padding-top: 12px;
	  padding-bottom: 12px;
	  text-align: left;
	  background-color: #4CAF50;
	  color: white;
	}
</style>

<section>
	<div class="space top-space50">
		<div class="container">
			<div class="row">
				<div class="col-lg-7">
					<div class="detail-sec">
						<div class="detail-head">
							<h1><?php echo $row_vehicleDetail['companyName']." ".$row_vehicleDetail['modelName']."<span> ".$row_vehicleDetail['modelNumber']."</span>";?></h1>
							<ul class="stars-rating">
								<li><i class="fa fa-star checked"></i></li>
								<li><i class="fa fa-star checked"></i></li>
								<li><i class="fa fa-star checked"></i></li>
								<li><i class="fa fa-star checked"></i></li>
								<li><i class="fa fa-star checked"></i></li>
							</ul>
							
							<span>Property of <a href="#" title=""><?php echo $row_vehicleDetail['firstName'];?></a> from <a href="#" title=""><?php echo $row_vehicleDetail['street'].",".$row_vehicleDetail['vct_city'];?>.</a></span>
							<span class="viewrz"><i class="fa fa-eye"></i><?php echo $row_vehicleDetail['views']; ?> people saw this.</span>
						</div>
						<ul class="detail-caro">
							
							<li>
								<img src="./vehicle/<?php echo $row_vehicleDetail['photo1_link'];?>" style="height:390px;" alt="">
							</li>
							
							<li>
								<img src="./vehicle/<?php echo $row_vehicleDetail['photo2_link'];?>" style="height:390px;" alt="">
								
							</li>
							
							<li>
								<img src="./vehicle/<?php echo $row_vehicleDetail['photo3_link'];?>" style="height:390px;" alt="">
								
							</li>
							
						</ul>
						
						
						<div class="Characteristics">
							<h5 class="little-title">Overview:</h5>
							<ul class="featurez">
								<li><i>Model</i><span><?php echo $row_vehicleDetail['companyName']." ".$row_vehicleDetail['modelName'];?></span></li>
								<li><i>year</i><span><?php echo $row_vehicleDetail['modelNumber'];?></span></li>
								<li><i>Fuel</i><span><?php echo $row_vehicleDetail['fuelType_Name'];?></span></li>
								<li><i>km</i><span><?php echo $row_vehicleDetail['approxKM']."+ KM";?></span></li>
								<li><i>Registration number</i><span>5678FCD</span></li>
								<li><i>Seats</i><span><?php echo $row_vehicleDetail['seats'];?></span></li>
								<li><i>Transmission</i><span>
								<?php 
									if($row_vehicleDetail['transmission'] == 0)
										echo "Manual";
									else
										echo "Automatic";
								?></span></li>
								<li><i>Insurance</i><span><?php if($row_vehicleDetail['insurance_link'] == "" ){echo  "None";} else { echo "Yes";}?></span></li>
							</ul>
						</div>
						
						
						<div class="reviewers">
								<h5 class="little-title">reviews TO BE EDITED</h5>
								<ul class="reviewer">
									<li>
										<figure>
											<img src="images/resources/reviewer-3.jpg" alt="">
										</figure>
										<div class="activity-meta">
											<span><a href="#" title="">Derrick H.</a> 02 Oct 2017</span>
											<p>Very cozy and welcoming in Jessica�s car!</p>
											<ul class="stars-rating">
												<li><i class="fa fa-star checked"></i></li>
												<li><i class="fa fa-star checked"></i></li>
												<li><i class="fa fa-star checked"></i></li>
												<li><i class="fa fa-star checked"></i></li>
												<li><i class="fa fa-star checked"></i></li>
											</ul>

										</div>                                                
									</li>
									<li>
										<figure>
											<img src="images/resources/reviewer-4.jpg" alt="">
										</figure>
										<div class="activity-meta">
											<span><a href="#" title="">Mia J.</a> 28 Sept 2017</span>
											<p>Jessica is very outgoing and peaceful. I had such a great time on the full ride with here. 100% recommended!</p>
											<ul class="stars-rating">
												<li><i class="fa fa-star checked"></i></li>
												<li><i class="fa fa-star checked"></i></li>
												<li><i class="fa fa-star checked"></i></li>
												<li><i class="fa fa-star checked"></i></li>
												<li><i class="fa fa-star checked"></i></li>
											</ul>
										</div>                                                
									</li>
									<li>
										<figure>
											<img src="images/resources/reviewer-5.jpg" alt="">
										</figure>
										<div class="activity-meta">
											<span><a href="#" title="">Janet Y.</a> 24 Sept 2017</span>
											<p>Jessica is punctual, which I really appreciate at someone, especially when they are getting picked up. </p>
											<ul class="stars-rating">
												<li><i class="fa fa-star checked"></i></li>
												<li><i class="fa fa-star checked"></i></li>
												<li><i class="fa fa-star checked"></i></li>
												<li><i class="fa fa-star checked"></i></li>
												<li><i class="fa fa-star checked"></i></li>
											</ul>
										</div>                                                
									</li>
									
								</ul>
							</div>
					</div>
				</div>
				<div class="col-lg-5">
					<div class="sidebar right">
						<div class="pickup-time">
							<div class="date-n-time">
								<p>
									<label><i class="fa fa-calendar"></i></label>
									<input id="datetimepicker1" type="text" value="<?php echo $_COOKIE['pickupDate'];?>" disabled>
								</p>  
								<p>  
									<label><i class="fa fa-clock-o"></i></label>
									<input id="datetimepicker2" type="text" value="<?php echo $_COOKIE['pickupTime'];?>" disabled>
								</p>
								<p>
									<label><i class="fa fa-calendar"></i></label>
									<input id="datetimepicker3" type="text" value="<?php echo $_COOKIE['dropDate'];?>" disabled >
								</p>  
								<p>  
									<label><i class="fa fa-clock-o"></i></label>
									<input id="datetimepicker4" type="text" value="<?php echo $_COOKIE['dropTime'];?>" disabled>
								</p>
							</div>
							
							<font size="5"  color="red"   style="font-weight: bold;">
							<table style="width:100%">
							
							<tr >
								<td width="50%" >
									<em>
									<?php 
									
												$security_deposite = $data['security_deposite'];
												$payable_security_deposite = 0;
												if($security_deposite < 6000)
												{
													$payable_security_deposite = 6000 - $security_deposite;
												}
												$total = $security_deposite;
												if($payable_security_deposite > 0)
												{
													echo "Total :";
												}
												
											?> 
									</em>	
								</td>
								<td width="50%" style="float:right;">
									<em>
									<?php 
										if($payable_security_deposite > 0)
										{
											echo "&#8377;".$payable_security_deposite;
										}
									?>
									</em>
								</td>
							</tr>
							<tr>
								<td width="50%">
									<em>Rate:</em>
								</td>
								<td width="50%" style="float:right;">	
									<em>
									<?php
										echo "&#8377;".$row_vehicleDetail['rate']." / km";
									?>
									</em>
									
								</td>
							</tr>
							</table>
							</font>
							
							
								
							

							<a href="bookingDBentry.php" title="" id="confirmBooking" name="confirmBooking" class="theme-btn">+ Rent this car today</a>
							<p>
								Actual price will be calculate and payable after end of the ride with <br />Total km x &#8377;<?php echo $row_vehicleDetail['rate'];?> / km + GST.
							</p>
							
							<p> In case you need more time extension you can add &#8377;200 For 30 minutes.
								
							</p>
						</div>
					</div>	
					
					<div class="Characteristics">
						<h5 class="little-title">Price Break Down:</h5>
						
						<table id="customers">
							
							<tr>
								<td><?php echo "Security Deposite (refundable)";?></td>
								<td>&#8377;<?php echo $payable_security_deposite;?></td>
							</tr>
							<tr>
								<td>Ride Fare</td>
								<td>&#8377;<?php echo $row_vehicleDetail['rate'];?>/ km.</td>
							</tr>
							<tr>
								<td>Time penalty per 30min delay</td>
								<td>&#8377;100</td>
							</tr>
							<tr>
								<td>CGST</td>
								<td>18%</td>
							</tr>
							<tr>
								<td>SGST</td>
								<td>18%</td>
							</tr>
							<tr>
								<td>Total</td>
								<td>&#8377;<?php echo $payable_security_deposite;?> + (Total KM x &#8377;<?php echo $row_vehicleDetail['rate'];?> ) + GST</td>
							</tr>
						</table>
						
					</div>
					<div class="features-list">
							<h5 class="little-title">Features:</h5>
							<ul class="feature-list">
								<?php
								
									if($row_vehicleDetail['abs'] == 1)
									{
										echo "<li><i class='flaticon-checked'></i>Automatic Transmission</li>";
									}
									if($row_vehicleDetail['overheadCarrier'] == 1)
									{
										echo "<li><i class='flaticon-checked'></i>Overhead Carrier</li>";
									}
									if($row_vehicleDetail['self_driving'] == 1)
									{
										echo "<li><i class='flaticon-checked'></i>Self Driving</li>";
									}
									if($row_vehicleDetail['cruise_control'] == 1)
									{
										echo "<li><i class='flaticon-checked'></i>Cruise Control</li>";
									}
								?>
								
							</ul>
						</div>
					
				</div>
			</div>
		</div>
	</div>
</section><!-- points section -->
<?php
	include_once('footerTop.php');
	include_once('footer.php');
?>