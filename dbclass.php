<?php
	
	class dbclass
	{
		var $con, $row_data , $row_vehicle_type;
		
		function __construct()
		{
			$this->con = mysqli_connect('localhost','root','');
			if(!$this->con)
			{
				die("Connection no established.".mysqli_error($this->con));
			}
			
			$this->db = mysqli_select_db($this->con,"rentalservice");
		}
		
		
		
		/*function getLogin($userName,$pwd,$rememberMe)
		{
		
			$sql = "SELECT * FROM  customer_tbl WHERE mobile = '".$userName."' OR  email = '".$userName."' AND password = '".$pwd."' ";
			
			$ans = mysqli_query($this->con,$sql);
			$row = mysqli_fetch_array($ans);
			
			if(mysqli_num_rows($ans) > 0) 
			{	
				//Permanent cookie
				if(isset($rememberMe))
				{
					setcookie(userName, $userName,time() + (86400 * 365)); //30 = 30 days
					
				}
				
				//Session cookie
				else
				{
					setcookie(userName, $userName);
				}
				
				
				header('location:index.php');
			} 
			
			else
			{
				echo "Invalid Username or Password!";
				header('location:login.php');
			}
	
		}*/
		
		function getCustomerData($userName)
		{
			
			$sql = "SELECT * FROM  customer_tbl WHERE  mobile = '".$userName."' OR  email = '".$userName."' LIMIT 1";
			
			$ans = mysqli_query($this->con,$sql);
			
			$row_data = mysqli_fetch_array($ans); 
			
			
			return $row_data;
		}

/*//BEGIN Register Customer

		function registerCustomer($fullName,$password,$email,$mobile)
		{
			$sql = "INSERT INTO customer_tbl(fullName,password,email,mobile) VALUES('$fullName','$password','$email','$mobile')";

			$rs = mysqli_query($this->con,$sql);

			if(!$rs)
			{
				die ("Registration failed:".mysqli_error($this->con));
			}

			else
			{
				header('location:index.php');
			} 

		}

//END Register Customer*/
//BEGIN newslettersubscriberInsert
	function newslettersubscriberInsert($newsLetterSubscriberEmail)
	{
		$sql = "Insert into newslettersubscriber(newsLetterSubscriberEmail) values('$newsLetterSubscriberEmail')";
		
		$rs = mysqli_query($this->con,$sql);
		
		if(!$rs)
			{
				die ("News letter subscription failed:".mysqli_error($this->con));
			}

			else
			{
				header('location:index.php');
			} 
	}
//END newslettersubscriberInsert
//BEGIN customer Profile Edit
	function profileUpdate($cid, $street, $c_city, $c_postalCode, $c_licenseNo, $file,$Prefrence, $dob, $ISSUE, $Expiry)
	{
		$sql = "UPDATE customer_tbl SET street 				= '$street', 
										city				= '$c_city',
										vehiclePrefrence 	= '$Prefrence',
										pinCode 			= '$c_postalCode', 
										licenseNumber 		= '$c_licenseNo' , 
										dateOfBirth			= '$dob',
									   issueDate			= '$ISSUE',
									   expirationDate		= '$Expiry'   	 ";

		if(!empty($_FILES["imgUpload"]["name"]))
		{
			$img=$_FILES["imgUpload"]["name"];

			//echo $img."</br>";
			$ext = pathinfo($img, PATHINFO_EXTENSION);


			//echo $ext."</br>";
			$img = basename($img,".".$ext);
			//echo $img."</br>";

			$img = "drivingLicense".$_COOKIE['userName'].".".$ext;
			//echo $img."</br>";


			$tmp_name=$_FILES["imgUpload"]["tmp_name"];

			if(is_uploaded_file($tmp_name))
			{
				move_uploaded_file($tmp_name,"./customerImage/".$img);
				//copy($tmp_name,"./customerImage/".$img);
				$sql = $sql. ", licenseLink = '".$img."' ";
				$_SESSION['msg'] = "Photo updated successfuly!";
			}
			
		}
		
		$sql = $sql." WHERE customerId = '$cid'";
		echo "$sql";
		$rs = mysqli_query($this->con,$sql);
		
		if(!$rs)
		{
			die('db not selected'.mysqli_error($this->con));
		}
		else
		{
			//echo "update";
			header('location:profile-edit.php');
		}
	}
//END customer Profile Edit

//Cancel Vehicle booking
	function cancelBooking($booking_id)
	{
		$sql = "INSERT INTO deleted_booking (SELECT * FROM newbooking WHERE booking_id = '$booking_id')";
		
		$rs = mysqli_query($this->con,$sql);
		
		$sql2 = "DELETE FROM newbooking WHERE booking_id = '$booking_id' ";
		
		$rs2 = mysqli_query($this->con,$sql2);
		
		if(!$rs)
		{
			die('Booking cancel failed'.mysqli_error($this->con));
		}
		else
		{
			//echo "update";
			header('location:upcomingRides.php');
		}
	}
//END Cancel Vehicle booking
	}//END of CLASS

	$obj = new dbclass();

//BEGIN customer Login

	if(isset($_POST['login_btn']))
	{
		$userName = $_POST['userName'];
		$pwd = $_POST['pwd'];
		$rememberMe = $_POST['rememberMe'];
		
		$obj -> getLogin($userName,$pwd,$rememberMe);
	}

//END customer login

/*//BEGIN Register Customer

	if(isset($_POST['btnCreateAccount']))
	{

		$fullName  = $_POST['cname'];
		$email  = $_POST['email'];
		$password  = $_POST['password'];
		$collabors = $_POST['collabors'];
		$mobile = $_POST['mobile'];

		if(isset($collabors))
		{
			$obj -> registerCustomer($fullName,$password,$email,$mobile);
		}
	}

//END Register Customer*/

//BEGIN newslettersubscriber
	if(isset($_POST['newslettersubscriber_btn']))
	{
		$newsLetterSubscriberEmail  = $_POST['newsLetterSubscriberEmail'];
		
		$obj -> newslettersubscriberInsert($newsLetterSubscriberEmail);
	}
//END newslettersubscriber

//BEGIN Booking vehicle
	if(isset($_POST['bookVehicle_btn']))
	{
		$vehicleOwnerId = $_POST['vehicleOwnerId'];
		$vehicleId  = $_POST['vehicleId'];
		$location = $_POST['my_location'];
		$pickupDate = $_POST['my_datetimepicker1'];
		$pickupTime = $_POST['my_datetimepicker2'];
		$dropDate = $_POST['my_datetimepicker3'];
		$dropTime = $_POST['my_datetimepicker4'];
		$rate = $_POST['rate'];
		
		setcookie('vehicleOwnerId',$vehicleOwnerId);
		setcookie('bookVehicleId',$vehicleId);
		setcookie('redirectOnce','1');
		setcookie('pickupDate',$pickupDate);
		setcookie('pickupTime',$pickupTime);
		setcookie('dropDate',$dropDate);
		setcookie('dropTime',$dropTime);
		setcookie('rate',$rate);
		
		$sql = "UPDATE vehicle_tbl SET views = views + 1 WHERE  vehicleId = '$vehicleId'";
		$ans = mysqli_query($obj->con,$sql);
		
		if(isset($_COOKIE['userName']))
		{
			echo "<script>window.location = 'car-detail.php'</script>";
		}
		
		else
		{
			echo "<script>window.location = 'login.php'</script>";
		}
	}
//END Booking vehicle



//BEGIN Customer Profile Edit
	if(isset($_POST['save_change']))
	{
	    $cid =$_POST['customerId'];
		$street =$_POST['street'];
		$c_city = $_POST['c_city'];
		$c_postalCode = $_POST['c_postalCode'];
		$c_licenseNo =$_POST['c_licenseNo'];
		$file = $_FILES['imgUpload'];
		$Prefrence = implode(',',$_POST['Prefrence']);
		//$c_name =$_POST['fullname'];
		//$c_mobile =$_POST['mobile'];
		//$c_email =$_POST['email'];


				$c_dob_d = $_POST['DOB_D'];
				$c_dob_m = $_POST['DOB_M'];
				$c_dob_y = $_POST['DOB_Y'];
				$dob = $c_dob_y."/".$c_dob_m."/".$c_dob_d;
				$time = strtotime($dob);
				$dob = date('Y-m-d',$time);
				//echo $dob;

			    $ISSUE_d = $_POST['ISSUE_D'];
				$ISSUE_m = $_POST['ISSUE_M'];
				$ISSUE_y = $_POST['ISSUE_Y'];
				$ISSUE = $ISSUE_y."/".$ISSUE_m."/".$ISSUE_d;
				$time = strtotime($ISSUE);
				$ISSUE = date('Y-m-d',$time);

				//echo $ISSUE;

			$c_Expiry_d = $_POST['Expiry_D'];                       //echo "$c_Expiry_d"; echo "<br>";
			$c_Expiry_m = $_POST['Expiry_M'];                        //echo "$c_Expiry_m"; echo "<br>";
			$c_Expiry_y = $_POST['Expiry_Y'];                        //echo "$c_Expiry_y"; echo "<br>";
			$Expiry = $c_Expiry_y."/".$c_Expiry_m."/".$c_Expiry_d;   //echo "$Expiry"; echo "<br>";
			$time = strtotime($Expiry);                              //echo "$time"; echo "<br>";
			$Expiry = date('Y-m-d',$time);							// echo "$Expiry"; echo "<br>";

			//echo $Expiry;

		  $obj -> profileUpdate($cid, $street, $c_city, $c_postalCode, $c_licenseNo, $file, $Prefrence, $dob, $ISSUE, $Expiry,);
							
		  							
		  							
		  											
	}
//END customer Profile Edit

//Cancel Vehicle booking
	if(isset($_GET['cancelid']))
	{
		$booking_id =$_GET['cancelid'];
		
		$obj -> cancelBooking($booking_id);
	}
//END Cancel Vehicle booking
?>