<?php
	session_start();
	

	if( (!isset($_SESSION['userName'])))
	{
		header('location:vehicleOwnerLogin.php');
	}
	
	else
	{
	
		include_once('dbclass.php');
		
	}
		
		$obj = new dbclass();
		$data = $obj -> getVehicleOwnerData($_SESSION['userName']);
		
		
?>
<!DOCTYPE html>
<html lang="en">
<head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Dashboard | RS Admin site
        </title>
        <!-- Icon -->
        <link rel="icon" href="datas/images/icon.ico">
       <!-- Text Editor CSS -->
        <link rel="stylesheet" type="text/css" href="datas/assets/simple-wysiwyg-editor/css/style.css">
        <!-- Bootstrap Core CSS -->
        <link href="datas/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- MetisMenu CSS -->
        <!-- lineicons-gh-pages -->
        <link rel="stylesheet" type="text/css" href="datas/assets/lineicons-gh-pages/styles.css">
        <!-- Animate CSS -->
        <link rel="stylesheet" href="datas/assets/animate.css-master/animate.min.css">
		<!-- Bootstrap-select CSS -->
        <link rel="stylesheet" href="datas/assets/bootstrap-select-master/dist/css/bootstrap-select.css">
		<!-- Bootstrap-multi-select CSS -->
        <link href="datas/assets/bootstrap-multiselect-master/dist/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" >
        <!-- Lou multi-select CSS -->
        <link rel="stylesheet" type="text/css" href="datas/assets/lou-multi-select-cf6d6c6/css/multi-select.css">
        <!-- Bootstrap-Touchspin CSS -->
        <link href="datas/assets/bootstrap-touchspin-master/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" type="text/css" media="all">
        <link href="datas/assets/bootstrap-touchspin-master/src/jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css" media="all">
		<!--  Assets Bootstrap-timepicker min CSS -->
        <link rel="stylesheet" type="text/css" href="datas/assets/bootstrap-timepicker/css/bootstrap-timepicker.min.css" />
        <link rel="stylesheet" type="text/css" href="datas/assets/bootstrap-timepicker/css/bootstrap-timepicker.css">
		<!--  Date Range Picker CSS -->
        <link rel="stylesheet" type="text/css" href="datas/assets/bootstrap-daterangepicker-master/daterangepicker.css">
		<!-- Page Level css -->
		<link rel="stylesheet" type="text/css" href="datas/assets/DataTables-1.10.12/media/css/dataTables.bootstrap.css">
		<!-- Bootstrap-table CSS -->
		<link rel="stylesheet" type="text/css" href="datas/assets/bootstrap-table-develop/src/bootstrap-table.css">
		<!-- Table CSS -->
		<link rel="stylesheet" type="text/css" href="datas/css/tables.css">
        <!-- Sparkline CSS -->
        <link rel="stylesheet" type="text/css" href="datas/assets/chartist-js-develop/dist/chartist.min.css">
        <!-- Charts CSS -->
        <link rel="stylesheet" type="text/css" href="datas/css/charts.css">
        <!-- Chartist Tool Tip CSS -->
        <link rel="stylesheet" type="text/css" href="datas/assets/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css">
        <!-- Full Calendar CSS -->
        <link href="datas/assets/calendar-master/vendors/fullcalendar/fullcalendar.css" rel="stylesheet">
        <!-- Full calendar print CSS -->
        <link href="datas/assets/calendar-master/vendors/fullcalendar/fullcalendar.print.css" rel="stylesheet" media="print">
        <!-- Sweet alert CSS -->
        <link rel="stylesheet" type="text/css" href="datas/min/csk.min.css">
        <link rel="stylesheet" type="text/css" href="datas/css/lite-csk.css">
        <!-- MediaQuery CSS -->
        <link rel="stylesheet" type="text/css" href="datas/css/media-query.css">
        <!-- Dashboard 1 CSS -->
        <link rel="stylesheet" type="text/css" href="datas/css/dashboard1.css">
        <!-- Page Level CSS -->
        <link rel="stylesheet" type="text/css" href="datas/css/profile.css">
        <!-- MediaQuery CSS -->
        <link rel="stylesheet" type="text/css" href="datas/css/media-query.css">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <!-- Fixed navbar -->
        <nav class="navbar navbar-fixed-top notification navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a href="dashboard.php" class="navbar-brand">
                    <span class="csk-brand">
                    <img src="datas/images/icon.png" class="img-responsive" alt="csk-brand icon" >
                    </span>
                    </a>
                </div>
                <a href="#menu-toggle" class="" id="menu-toggle">
                <i class="fa fa-bars" aria-hidden="true">
                </i>
                </a>
                <ul class="settingsmenu">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog pull-right"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="profile.php">Account Settings <span class="glyphicon glyphicon-cog pull-right"></span></a></li>
 <! ––                            <li class="divider"></li>
						</ul>
                        
                    </li>
                </ul>
                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li class="dropdown messages-menu">
                            
                        <!-- Notifications Menu -->
                        <li class="dropdown notifications-menu">
                            <!-- Menu toggle button -->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-bell-o">
                                </i>
                                <div class="notify">
                                    <span class="ring">
                                    </span>
                                    <span class="ring-point">
                                    </span>
                                </div>
                            </a>
                            <ul class="dropdown-menu notification-dropdown">
                                <li class="header">You have 7 notifications
                                </li>
                                <li class="notification-body">
                                    <!-- Inner Menu: contains the notifications -->
                                    <ul class="notification">
                                        <li>
                                            <!-- start notification -->
                                            <a href="#" class="">
                                            <i class="fa fa-user-plus">
                                            </i> 5 new members joined today
                                            </a>
                                        </li>
                                        <li>
                                            <!-- start notification -->
                                            <a href="#" class="">
                                            <i class="fa fa-warning">
                                            </i> Something wrong going on
                                            </a>
                                        </li>
                                        <li>
                                            <!-- start notification -->
                                            <a href="#" class="">
                                            <i class="fa fa-shopping-cart">
                                            </i> 5 new products added
                                            </a>
                                        </li>
                                        <li>
                                            <!-- start notification -->
                                            <a href="#" class="">
                                            <i class="fa fa-usd">
                                            </i> Price change your product
                                            </a>
                                        </li>
                                        <li>
                                            <!-- start notification -->
                                            <a href="#" class="">
                                            <i class="fa fa-archive">
                                            </i> 3 orders not yet dispatched
                                            </a>
                                        </li>
                                        <li>
                                            <!-- start notification -->
                                            <a href="#" class="">
                                            <i class="fa fa-cubes">
                                            </i> 3 orders not yet dispatched
                                            </a>
                                        </li>
                                        <li>
                                            <!-- start notification -->
                                            <a href="#" class="">
                                            <i class="fa fa-user-secret">
                                            </i> 5 new vendors registered
                                            </a>
                                        </li>
                                        <!-- end notification -->
                                    </ul>
                                </li>
                                <div class="footer">
                                    <a href="#">View all
                                    </a>
                                </div>
                            </ul>
                        </li>
                        <!-- User Account Menu -->
                        <li class="dropdown user user-menu">
                            <!-- Menu Toggle Button -->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <!-- The user image in the navbar-->
                                <?php echo "<img src='../vehicleOwnerProfile/".$data['profile']."' class='user-image' alt='User Image' >";?>
                                
                                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            </a>
                            <ul class="dropdown-menu">
                                <!-- The user image in the menu -->
                                <li class="user-header">
                                    <?php echo "<img src='../vehicleOwnerProfile/".$data['profile']."' class='user-image' alt='User Image' >";?>
 <!--                                   <img src="datas/images/avatars/captain-avatar.png" class="user-image" alt="User Image">
-->
                                    <p>
                                       <?php echo $data['firstName']." ".$data['lastName'];?>
                                        <small> <?php echo $data['email'];?>
                                        </small>
                                    </p>
                                </li>
                                <!-- Menu Body
                                <li>
                                    <a href="#account">
                                    <i class="icon icon-li-female-user sIcon">
                                    </i> Account Settings
                                    </a>
                                </li>
                                <li>
                                    <a href="#account">
                                    <i class="icon icon-li-fire sIcon">
                                    </i> Balance
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                    <i class="icon icon-li-config sIcon">
                                    </i> Sellers Info.
                                    </a>
                                </li> -->
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="profile.php" class="btn btn-info">Profile
                                        </a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="logout.php" class="btn btn-danger">Sign out
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!-- # navbar-custom-menu -->
            </div>
            <!-- # container-fluid -->
        </nav>
        