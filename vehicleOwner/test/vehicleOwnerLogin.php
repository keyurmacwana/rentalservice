
<!DOCTYPE html>
<html lang="en">
	
<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<title>Sign In | RS - 
		owner | Retal Services</title>
		<!-- Icon -->
		<link rel="icon" href="datas/images/icon.ico">
		<!-- Bootstrap Core CSS -->
		<link href="datas/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<!-- Animate CSS -->
		<link rel="stylesheet" href="datas/assets/animate.css-master/animate.min.css">
		<!-- Theme  CSS -->
		<link rel="stylesheet" type="text/css" href="datas/min/csk.min.css">
		<!-- MediaQuery CSS -->
		<link rel="stylesheet" type="text/css" href="datas/css/media-query.css">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
	 	
		
		$("#btnLogin").click(function(e) { 
		e.preventDefault();
        var proceed1 = true;
        //simple validation at client's end
        //loop through each field and we simply change border color to red for invalid fields       
        $("#login-form input[required=true]").each(function(){			
            $(this).css('border-color',''); 
            if(!$.trim($(this).val())){ //if this field is empty 
                $(this).css('border-color','red'); //change border color to red   
                proceed1 = false; //set do not proceed flag
            }						            			
        });
       
        if(proceed1) //everything looks good! proceed...
        {
            //get input field values data to be sent to server
            post_data1 = {              
				'userName'    : $('input[name=userName]').val(),				
				'password'    : $('input[name=password]').val()
            };
			
			console.log(post_data1);
            //Ajax post data to server
            $.post('vehicleOwnerLogin_jquery.php', post_data1, function(response){  	
				//console.log(response);
                if(response.type == 'error'){ //load json data from server and output message 
					 
                    output = '<div class="error" style="background: #FFE8E8;padding: 5px 10px 5px 10px;margin: 0px 0px 5px 0px;border: none;font-weight: bold;color: #FF0000;border-left: 3px solid #FF0000;">'+response.text+'</div>';
					 $("#login-form  input[required=true]").val(''); 
                }else{
                    output = '<div class="success" style="background: #D8FFC0;padding: 5px 10px 5px 10px;margin: 0px 0px 5px 0px;border: none;font-weight: bold;color: #2E6800;border-left: 3px solid #2E6800;">'+response.text+'</div>';
                    //reset values in all input fields
                    
                    $("#login-form #loginsub").slideUp().delay(2000).slideDown(500); //hide form after success
					
						$("#login-form  input[required=true]").val(''); 
					window.location.href = 'checkout.php';
                }
                $("#login-form #logincustomerresult").hide().html(output).slideDown();
            }, 'json');
        }
    });
    
    //reset previously set border colors and hide all message on .keyup()
    $("#login-form  input[required=true]").keyup(function() { 
        $(this).css('border-color',''); 
        $("#logincustomerresult").slideUp();
    });
});
</script>
	</head>
	<body>
		<!--Container Starts -->
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-md-push-4 col-xs-12 sign-box">
					<div class="panel panel-default pale-panel-border-top">
						<div class="panel-heading text-center">
							<h3 class="nm np">
							Vehicle Owner Login
							</h3>
						</div>
						
               			<div class="box-authentication" id="login-form">
						<div id="logincustomerresult"></div>
						<div id="loginsub">
				
						<div class="panel-body">
							<br>
							<form class="form" id="form-login" method="post">
								<input id="userName" name="userName" type="text"  placeholder="Username" class="form-control" required=true />
								<input id="password" name="password" type="password" required=true class="form-control" placeholder="Password" />	
						
								
								
						
								<button type="submit" id="btnLogin" name="btnLogin" class="center btn btn-success btn-lg btn-block" data-dismiss="modal">
								<i class="fa fa-lock"></i> Secure Login
								</button>
							</form>
							<form action="vehicleOwnerSignup.php">
								<button type="submit" id="btnSignup" name="btnSignup" class="center btn btn-warning btn-lg btn-block" data-dismiss="modal">
								<i class="fa fa-rocket"></i> Request Sign Up
								</button>
							</form>
							</div>
							</div>
							</div>
							
							<label><?php $msg;?></label>
							<div class="text-right">
								<small><a href="forgot.php?msg=" class="text-danger">Forgot Password?</a></small>
							</div>
							<hr>
							<!--<div class="text-center">
								<h4>
								Don't have an Account ? <br><a href="sign-up.html">Sign Up here </a>
								</h4>
							</div>-->
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- #Container Ends -->
		<!-- GoogleApi jQuery -->
		<script type="text/javascript" src="datas/assets/jquery-1.12.4/jquery-1.12.4.js"></script>
		<!-- Bootstrap Core JavaScript -->
		<script src="datas/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="datas/js/sisur.js"></script>
	</body>


</html>