<?php
	include_once('header.php');
?>

<!-- Main Body content starts here -->
        <div id="wrapper">
            <!-- Sidebar -->
            <div id="sidebar-wrapper">
                <aside class="sidebar">
                    <nav class="sidebar-nav" id="sidebarscroll">
                        <ul class="metismenu ripple" id="menu">
						
								<li  >
									<a href="dashboard.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-folder-open fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Dashboard</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li >
									<a href="ownerWallet.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-money fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Wallet</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li >
									<a href="upcomingRides.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-tachometer fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Upcoming Rides</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li >
									<a href="upcomingRides.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-clock-o  fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Rides History</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li>
									<a href="registerVehicle.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-plus fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Add Vehicles</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								<li style="background-color:#505464;">
									<a href="vehicleOffDay.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-calendar fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Reserve Vehicle</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								<li >
									<a href="Terms_and_Condition.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-legal notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Terms & Conditions</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li >
									<a href="ownerFAQ.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-question-circle fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">FAQ</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
	
								
                            
                        </ul>
                    </nav>
                </aside>
            </div>
            <!-- # Sidebar-wrapper -->
            
			<!-- Page Content-wrapper -->
			<div id="page-content-wrapper">
<?php



//To DELETE Record
	if(isset($_GET['d']))
	{
		$obj -> deleteVehicleHoliday($_GET['d']);
	}
//END DELETE Record

//TO EDIT record
	if(isset($_GET['vehicleHolidayId']))
	{
		$sql_vehicleHoliday_edit = "SELECT * FROM  vehicleholiday_tbl WHERE vehicleHolidayId='".$_GET['vehicleHolidayId']."'";
		$ans_vehicleHoliday_edit = mysqli_query($obj->con,$sql_vehicleHoliday_edit);
		$row_vehicleHoliday_edit = mysqli_fetch_array($ans_vehicleHoliday_edit);
	}
//END EDIT record
?>
			
				<!-- Breadcrumb  -->
				<div class="row csk-breadcrumb">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
						<h4 class="page-title">Unavailable Vehicle</h4>
					</div>
					<div class="col-lg-9 col-md-8 col-sm-8 hidden-xs">
						<ol class="breadcrumb">
							<li><a href="dashboard.php">Dashboard</a></li>
							<li><a href="#">Unavailable Vehicle</a></li>
						</ol>
					</div>
				</div>
				<!-- #Breadcrumb -->
				<!-- row -->
				
				<!-- row -->
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-defaut">
							<div class="panel-heading">
								<h3 class="panel-title">Unavailable Vehicle for Specific Days</h3>
							</div>
							<div class="panel-body">
								<form action="dbclass.php" method="post">
									<input type="hidden" name="vehicleOwnerId" id="vehicleOwnerId" value="<?php echo $data['vehicleOwnerId'];?>" />
									<?php 
										if(isset($_GET['vehicleHolidayId']))
										{ 
											echo "<input type='hidden' name='vehicleHolidayId' id='vehicleHolidayId' value='".$row_vehicleHoliday_edit['vehicleHolidayId']."'>";
										} 
									?>
									
									<div class="form-body">
										
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label">License Plate Number</label>
													<select name="vehicleId" id="vehicleId" class="form-control">
														<option value="0">---Select one---</option>
														<?php
															//To fetch owner vehicle
																$sql_owner_vehicle = "SELECT vt.*,vtm.*
																 FROM vehicle_tbl vt 
																 LEFT JOIN vehicle_tbl_model vtm
																 ON vtm.vehicleId = vt.vehicleId
																 WHERE vt.owner_id='".$data['vehicleOwnerId']."' ";		
																$ans_owner_vehicle = mysqli_query($obj->con,$sql_owner_vehicle);
															//END fetch owner vehicle
															while($row_owner_vehicle = mysqli_fetch_array($ans_owner_vehicle))
															{
														?>
														<option value="<?php echo $row_owner_vehicle['vehicleId']; ?>" <?php if(isset($_GET['vehicleHolidayId'])){if($row_vehicleHoliday_edit['vehicleId'] == $row_owner_vehicle['vehicleId']){echo "selected";}}?>>
														<?php echo $row_owner_vehicle['licenseNumberPlate']; ?>
														</option>
													</select>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label">Model(AJAX model name)</label>
													<select name="vehicle_id" id="vehicle_id" class="form-control">
														<option value="0">---Select one---</option>
														
														<option 
														<?php if(isset($_GET['vehicleHolidayId'])){if($row_vehicleHoliday_edit['vehicleId'] == $row_owner_vehicle['vehicleId']){echo "selected";}}?>>
														<?php echo $row_owner_vehicle['companyName'].",".$row_owner_vehicle['modelName'].",".$row_owner_vehicle['modelNumber']; ?>
														</option>
														<?php
															}
														?>
													</select>
												</div>
											</div>
										</div>
										
										<div class="row">
											<div class="col-md-3">
												<label class="control-label">Off Day From</label>
												<input type="date" name="startDate" id="endDate" value="<?php if(isset($_GET['vehicleHolidayId'])){echo $row_vehicleHoliday_edit['startDate'];}?>">
											</div>
											<div class="col-md-3">
												<label class="control-label">Off Day To</label>
												<input type="date" name="endDate" id="endDate" value="<?php if(isset($_GET['vehicleHolidayId'])){echo $row_vehicleHoliday_edit['endDate'];}?>">
											</div>
										</div>
										
										<div class="row">
											<div class="col-md-12">
												<label class="control-label">Reason</label>
												<input type="text" name="reason" id="reason" value="<?php if(isset($_GET['vehicleHolidayId'])){echo $row_vehicleHoliday_edit['reason'];}?>">
											</div>
										</div>
										<div class="row">
											<div class="col-md-1">
												<label class="control-label">Total Days(Ajax/JS)</label>
												<input type="text" placeholder="To be Edited">
											</div>
										</div>
									</div>
									<div class="form-actions">
										<button type="submit" class="btn btn-success" name="vehicleOffDay" id="vehicleOffDay"> <i class="fa fa-check"></i> Save</button>
										<button type="button" class="btn btn-danger">Cancel</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- #row -->
				<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default panel-with-options">
			
				<h3 >&emsp;Upcoming off day</h3>
			
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered datatable">
						<thead>
							<tr>
								<th>SR NO.</th>
								<th>License Plate</th>
								<th>Model</th>
								<th>Reason</th>
								<th>From</th>
								<th>To</th>
								<th>Total</th>
								<th>Action</th>
							</tr>
						</thead>
						<tfoot>
						<tr>
								<th>SR NO.</th>
								<th>License Plate</th>
								<th>Model</th>
								<th>Reason</th>
								<th>From</th>
								<th>To</th>
								<th>Total</th>
								<th>Action</th>
							</tr>
						</tfoot>
						<tbody>
							
							<?php 
								//To fetch table data
									$sql_vehicleholiday_tbl = "SELECT vht.*,vt.* ,vtm.*
									
																FROM  vehicleholiday_tbl vht
																
																LEFT JOIN vehicle_tbl vt
																ON vt.vehicleId = vht.vehicleId
																
																LEFT JOIN vehicle_tbl_model vtm
																ON vtm.vehicleId = vt.vehicleId
																
																WHERE vt.owner_id = '".$data['vehicleOwnerId']."'
																AND vht.delete_status = 0";	
														
									$ans_vehicleholiday_tbl = mysqli_query($obj->con,$sql_vehicleholiday_tbl);
								//END fetch table data
								$counter = 1;
								while($row_vehicleholiday_tbl = mysqli_fetch_array($ans_vehicleholiday_tbl))
								{
							?>
								<tr>
									<td><?php	echo $counter ++; ?></td>
									<td><?php   echo $row_vehicleholiday_tbl['licenseNumberPlate']; ?>
									</td>
											
									<td><?php   echo $row_vehicleholiday_tbl['companyName'].",".$row_vehicleholiday_tbl['modelName'].",".$row_vehicleholiday_tbl['modelNumber']; ?>
									</td>
									<td><?php	echo $row_vehicleholiday_tbl['reason']; ?></td>
									<td><?php 	echo date("d-m-Y", strtotime($row_vehicleholiday_tbl['startDate']));?>
									</td>
									<td><?php	echo date("d-m-Y", strtotime($row_vehicleholiday_tbl['endDate']));?>
									</td>
									<td><?php	echo $row_vehicleholiday_tbl['totalDays']; ?></td>
									<td>
									<a href="vehicleOffDay.php?vehicleHolidayId=<?php echo $row_vehicleholiday_tbl['vehicleHolidayId']; ?>" title="EDIT"><i class="fa fa-pencil fa-1x "></i></a> || 
									<a href="vehicleOffDay.php?d=<?php echo $row_vehicleholiday_tbl['vehicleHolidayId']; ?>" title="DELETE"><i class="fa fa-trash fa-1x"></i></a>
									</td>
								</tr>
							<?php
								}
							?>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
	

<?php
	include_once('footer.php');
?>