<!DOCTYPE html>
<html lang="en">
	
<!-- Mirrored from cskadmin.com/v3.2/sign-up.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 18 Mar 2017 13:40:22 GMT -->
<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<title>Sign Up | CSK - Admin Template | Zeasts</title>
		<!-- Icon -->
		<link rel="icon" href="datas/images/icon.ico">
		<!-- Bootstrap Core CSS -->
		<link href="datas/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<!-- Animate CSS -->
		<link rel="stylesheet" href="datas/assets/animate.css-master/animate.min.css">
		<!-- Theme  CSS -->
		<link rel="stylesheet" type="text/css" href="datas/min/csk.min.css">
		<!-- MediaQuery CSS -->
		<link rel="stylesheet" type="text/css" href="datas/css/media-query.css">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<!--Container Starts -->
		<div class="container">
			<!--row  -->
			<div class="row">
				<div class="col-md-4 col-md-push-4 col-xs-12 sign-box">
					<div class="panel panel-default pale-panel-border-top">
						<div class="panel-heading">
							<h3 class="text-center">Vehicle Owner Sign Up</h3>
						</div>
						<div class="panel-body">
							<div class="form signup">
								<form method="post" action="dbclass.php" >
									<input type="text" placeholder="First Name" required="" name="userName" id="userName"/>
									<input type="text" placeholder="First Name" required="" name="firstName" id="firstName"/>
									<input type="text" placeholder="Last Name" required="" name="lastName" id="lastName"/>
									<input type="email" placeholder="Email" required="" name="email" id="email"/>
									<input type="password" placeholder="Password" required="" name="pwd1" id="pwd1"/>
									<input type="password" placeholder="Confirm Password" required="" name="pwd2" id="pwd2"/>
									<button type="submit" class="btn btn-block btn-info" name="btnSignUp" id="btnSignUp">SignUp</button>
								</form>
								<form method="post" action="vehicleOwnerLogin.php">
									<button type="submit" class="btn btn-block btn-info ">Login In</button>
								</form>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			<!-- #row -->
		</div>
		<!-- #Container Ends -->
		<!-- GoogleApi jQuery -->
		<script type="text/javascript" src="datas/assets/jquery-1.12.4/jquery-1.12.4.js"></script>
		<!-- Bootstrap Core JavaScript -->
		<script src="datas/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="datas/js/sisur.js"></script>
	</body>

<!-- Mirrored from cskadmin.com/v3.2/sign-up.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 18 Mar 2017 13:40:22 GMT -->
</html>