<?php
	include_once('header.php');
?>
<!-- Main Body content starts here -->
        <div id="wrapper">
            <!-- Sidebar -->
            <div id="sidebar-wrapper">
                <aside class="sidebar">
                    <nav class="sidebar-nav" id="sidebarscroll">
                        <ul class="metismenu ripple" id="menu">
						
								<li  >
									<a href="dashboard.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-folder-open fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Dashboard</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li >
									<a href="ownerWallet.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-money fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Wallet</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li >
									<a href="upcomingRides.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-tachometer fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Upcoming Rides</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li >
									<a href="upcomingRides.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-clock-o  fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Rides History</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li>
									<a href="registerVehicle.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-plus fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Add Vehicles</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								<li >
									<a href="vehicleOffDay.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-calendar fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Reserve Vehicle</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								<li style="background-color:#505464;">
									<a href="Terms_and_Condition.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-legal notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Terms & Conditions</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li >
									<a href="ownerFAQ.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-question-circle fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">FAQ</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
	
								
                            
                        </ul>
                    </nav>
                </aside>
            </div>
            <!-- # Sidebar-wrapper -->
            
			<!-- Page Content-wrapper -->
			<div id="page-content-wrapper">
<?php
//To fetch table data
	$sql_tnc = "SELECT * FROM  termsandconditions_tbl WHERE termFor = '0' ORDER BY termCategory";		
	$ans_tnc = mysqli_query($obj->con,$sql_tnc);
//END fetch table data
?>
			
				
				
				<!-- #row -->
				<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default panel-with-options">
			
				<h3 >&emsp;Terms and condition</h3>
				
					<div class="col-md-12">
						<!-- START STRIPED TABLE SAMPLE -->
						<div class="panel panel-success">
							<div class="panel-heading ui-draggable-handle">
								<h3 class="panel-title">Terms and condition list</h3>
							</div>
							<div id="collapsepanel4" class="panel-collapse collapse in">
								<div class="panel-body">
									<table class="table table-striped">
										<thead>
											<tr>
												<td width="20%">Category</td>
												 <td width="10%">Sr.no</td>
												 
    											 <td width="70%">Detail</td>
												
											</tr>
										</thead>
										<tbody>
											<?php
												$counter = 1;
												$counter2 = 1;
												while($row_tnc = mysqli_fetch_array($ans_tnc))
												{
											?>
											<tr>
												<?php $counter++;?>
												<td><?php if($counter > 2)
															{
																if($row_tnc['termCategory'] == $temp)
																{}
																else
																{
																	echo $row_tnc['termCategory'];
																	$counter2 = 1;
																}
																
															}

															else
															{
																echo $row_tnc['termCategory'];
																$temp = $row_tnc['termCategory'];
															}
													?>
													
												</td>
												<td><?php  echo $counter2++;?></td>	
												<td><?php echo $row_tnc['termDetail'];?></td>
											</tr>
											<?php
												}
											?>
											
							
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<!-- END STRIPED TABLE SAMPLE -->
 </div>
			
			
		</div>
	</div>
</div>
	

<?php
	include_once('footer.php');
?>