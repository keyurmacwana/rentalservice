
<?php
	
	class dbclass
	{
		var $con, $row_data;
		
		function __construct()
		{
			$this->con = mysqli_connect('localhost','root','');
			if(!$this->con)
			{
				die("Connection no established.".mysqli_error($this->con));
			}
			
			$this->db = mysqli_select_db($this->con,"rentalservice");
		}
		
		function getLogin($userName,$password)
		{
			
			$sql = "SELECT * FROM  owner_tbl WHERE userName = '".$userName."' AND password = '".$password."' ";
			
			$ans = mysqli_query($this->con,$sql);
			$row = mysqli_fetch_array($ans);
			
			if(mysqli_num_rows($ans) > 0) 
			{								
				session_start();
				
				$_SESSION['userName'] = $userName;
				$_SESSION['password'] = "";

				header('location:dashboard.php');
			} 
			
			else
			{
				echo "Invalid Username or Password!";
				header('location:vehicleOwnerLogin.php');
			}
	
		}
		
		function getVehicleOwnerData($userName)
		{
			
			$sql = "SELECT * FROM  owner_tbl WHERE username = '".$userName."' LIMIT 1";
			
			$ans = mysqli_query($this->con,$sql);
			
			$row_data = mysqli_fetch_array($ans); 
			
			return $row_data;
		}
		
//BEGIN update VehicleOwner
		function updateVehicleOwner($vehicleOwnerId,$firstName,$lastName,$email,$street,$state,$mobileNumber,$pincode,$city,$file)
		{
			$sql = "UPDATE owner_tbl SET firstName = '$firstName' , lastName = '$lastName' ,email = '$email' ,street = '$street' ,state = '$state' ,mobileNumber = '$mobileNumber' ,pincode = '$pincode' ,city = '$city'";
			
			if(!empty($_FILES["imgUpload"]["name"]))
			{
				$img=$_FILES["imgUpload"]["name"];

				//echo $img."</br>";
				$ext = pathinfo($img, PATHINFO_EXTENSION);


				//echo $ext."</br>";
				//$img = basename($img,".".$ext);
				//echo $img."</br>";

				$img = $_SESSION['userName']."profilePhoto.".$ext;
				//echo $img."</br>";


				$tmp_name=$_FILES["imgUpload"]["tmp_name"];

				if(is_uploaded_file($tmp_name))
				{
					move_uploaded_file($tmp_name,"../vehicleOwnerProfile/".$img);
					//copy($tmp_name,"../vehicleOwnerProfile/".$img);
					$sql = $sql. ", profile = '".$img."' ";
					$_SESSION['msg'] = "Photo updated successfuly!";
				}
				
			}
			
			$sql = $sql."WHERE vehicleOwnerId='$vehicleOwnerId'";
			
			$rs = mysqli_query($this->con,$sql);
			
			if(!$rs)
			{
				die ("update failed:".mysqli_error($this->con));
			}

			else
			{
			header('location:profile.php');

//				echo $sql;
				
			} 
		}
//END update VehicleOwner

//BEGIN change password

	function changePwd($userName,$pwd1)
		{
			$sql = "UPDATE owner_tbl SET password = '$pwd1' WHERE userName = '$userName'";
			
			$rs = mysqli_query($this->con,$sql);
			
			
			if($rs)
			{
				header('location:profile.php');
			
			}
			
			else
			{
				echo "Password update failed";
			}	
		}
//END change password
//BEGIN Password Recovery
		function forgotPassword($userName,$recoveryEmail)
		{
			$sql = "SELECT * FROM  owner_tbl WHERE username = '".$userName."' AND email = '".$recoveryEmail."' ";

			$ans = mysqli_query($this->con,$sql);
			$row = mysqli_fetch_array($ans);
			
			if(mysqli_num_rows($ans) > 0)
			{
				$userName = base64_encode($userName);
				$timeStamp = time();
				$link = "http://localhost/rentalservice/vehicleOwner/passwordRecover.php?userName=".$userName;
				$link .= "&var1=".$timeStamp;
				$to = $recoveryEmail;
				$subject = "Password recovery request!";
				$message = "You have requested for password recovery. If you did not apply for this please change your password immediately and repot the issue.
Your password Reset link is: ".$link."
This link will expire in 10 minutes";
//				$headers = "From: your@email-address.com";

				if (mail($to, $subject, $message)) {
					header('Location:vehicleOwnerLogin.php?msg=An Email has been sent, please check your email inbox!');
				}
				else
				{
					header('Location:forgot.php?msg=email sending failed!');
				}
				
			}

			else{
				header('Location:forgot.php?msg=Username or Email incorrect!');
			}
		}
//END password recovery
		
//BEING change password using link
	
	function passwordReset($pwd,$pwd2,$userName)
		{
			
			$sql = "UPDATE owner_tbl SET password = '$pwd' WHERE username = '$userName'";
			
			$rs = mysqli_query($this->con,$sql);
			
			if(!$rs)
			{
				die ("update failed:".mysqli_error($this->con));
			}

			else
			{
			header('location:vehicleOwnerLogin.php?passwordUpdated');

//				echo $sql;
				
			} 
		}

//END change password using link
	function signupRequest($userName,$firstName,$lastName,$email,$pwd1)
	{
			$sql = "INSERT INTO vehicleOwnerSignupRequest_tbl(userName,firstName,lastName,email,password) VALUES('$userName','$firstName','$lastName','$email','$pwd1')";
		
			$rs = mysqli_query($this->con,$sql);
			
			if(!$rs)
			{
				die ("Uesrname Already taken.".mysqli_error($this->con));
			}

			else
			{
				$userName = base64_encode($userName);
				$timeStamp = time();
					$link = "http://localhost/rentalservice/vehicleOwner/emailVerification.php?userName=".$userName;
				$link .= "&var1=".$timeStamp;
				$to = $email;
				$subject = "Email Verification!";
				$message = "THANK YOU for applying. Please click link to verify and become our partner.
Your account will be genrated in 2 working days after verifying the link.
Your Verification link is: ".$link."
This link will expire in 10 minutes";
//				$headers = "From: your@email-address.com";

				if (mail($to, $subject, $message)) {
?>
				<html>
					<body>
						Alert pop-up to display sucess msg and redirect to login
					</body>
				</html>
				
<?php
				}
				else
				{
					
				}
				
			} 
	}
//BEGIN Signup request

	

//END signup request
		
		
//BEGIN Vehicle register

	function vehicleRegister($photo1_link,$photo2_link,$photo3_link,$rcBook_link,$insurance_link,$puc_link,$vehicle_type_id,$vehicle_category_id,$companyName,$modelName,$modelNumber,$licenseNumberPlate,$street,$city,$state,$weekdayStart,$weekdayEnd,$weekendStart,$weekendEnd,$door_number,$fuelType_id,$transmission,$seats,$luggageCapacity,$abs,$rate,$multiDayRenting,$overheadCarrier,$self_driving,$cruise_control)
	{
		//fetching owner id
			session_start();
			$sql_owner_id = "SELECT * FROM  owner_tbl WHERE userName='".$_SESSION['userName']."'";		
			$ans_owner_id = mysqli_query($this->con,$sql_owner_id);
			$row_owner_id = mysqli_fetch_array($ans_owner_id);
			$vehicleOwnerId = $row_owner_id['vehicleOwnerId'];
		//END fetching owner id	
		/*create vehicle id*/	$vehicleId = $vehicleOwnerId.$licenseNumberPlate;
			
		//	sql_vehicle_tbl
			$sql_vehicle_tbl = "INSERT INTO vehicle_tbl(vehicleId,owner_id,vehicle_type_id,vehicle_category_id,licenseNumberPlate,rate) VALUES('$vehicleId','$vehicleOwnerId','$vehicle_type_id','$vehicle_category_id','$licenseNumberPlate','$rate')";
			
			$rs_vehicle_tbl = mysqli_query($this->con,$sql_vehicle_tbl);
		
		//	sql_vehicle_tbl_model
			$sql_vehicle_tbl_model = "INSERT INTO vehicle_tbl_model(vehicleId,companyName,modelName,modelNumber) VALUES('$vehicleId','$companyName','$modelName','$modelNumber')";
			$rs_vehicle_tbl_model = mysqli_query($this->con,$sql_vehicle_tbl_model);
		
		//	sql_vehicle_tbl_address
			$sql_vehicle_tbl_address = "INSERT INTO vehicle_tbl_address(vehicleId,street,city,state,door_number) VALUES('$vehicleId','$street','$city','$state','$door_number')";
			$rs_vehicle_tbl_address = mysqli_query($this->con,$sql_vehicle_tbl_address);
			
		//	vehicle_tbl_schedule
			$sql_vehicle_tbl_schedule = "INSERT INTO vehicle_tbl_schedule(vehicleId,weekdayStart,weekdayEnd,weekendStart,weekendEnd,multiDayRenting) VALUES('$vehicleId','$weekdayStart','$weekdayEnd','$weekendStart','$weekendEnd','$multiDayRenting')";
			$rs_vehicle_tbl_schedule = mysqli_query($this->con,$sql_vehicle_tbl_schedule);
			
		//	vehicle_tbl_features
			$sql_vehicle_tbl_features = "INSERT INTO vehicle_tbl_features(vehicleId,fuelType_id,seats,transmission,abs,overheadCarrier,self_driving,luggageCapacity,cruise_control) VALUES('$vehicleId','$fuelType_id','$seats','$transmission','$abs','$overheadCarrier','$self_driving','$luggageCapacity','$cruise_control')";
			$rs_vehicle_tbl_features = mysqli_query($this->con,$sql_vehicle_tbl_features);
																				
		//vehicle_tbl_documents

			$sql_vehicle_tbl_documents = "INSERT INTO vehicle_tbl_documents(vehicleId,photo1_link,photo2_link,photo3_link,rcBook_link,insurance_link,puc_link) VALUES('$vehicleId'";
			if(!empty($_FILES["photo1_link"]["name"]))
			{
				$img=$_FILES["photo1_link"]["name"];

				//echo $img."</br>";
				$ext = pathinfo($img, PATHINFO_EXTENSION);


				//echo $ext."</br>";
				$img = basename($img,".".$ext);
				//echo $img."</br>";

				$img = rand(1111111111,9999999999).$img.rand(1111111111,9999999999).".".$ext;
				//echo $img."</br>";


				$tmp_name=$_FILES["photo1_link"]["tmp_name"];

				if(is_uploaded_file($tmp_name))
				{
					copy($tmp_name,"../vehicle/".$img);
					$sql_vehicle_tbl_documents = $sql_vehicle_tbl_documents.",'$img'";
					
				}
					
			}
			
			else
			{
				$sql_vehicle_tbl_documents = $sql_vehicle_tbl_documents.",''";
			}
			
			if(!empty($_FILES["photo2_link"]["name"]))
			{
				$img=$_FILES["photo2_link"]["name"];

				//echo $img."</br>";
				$ext = pathinfo($img, PATHINFO_EXTENSION);


				//echo $ext."</br>";
				$img = basename($img,".".$ext);
				//echo $img."</br>";

				$img = rand(1111111111,9999999999).$img.rand(1111111111,9999999999).".".$ext;
				//echo $img."</br>";


				$tmp_name=$_FILES["photo2_link"]["tmp_name"];

				if(is_uploaded_file($tmp_name))
				{
					copy($tmp_name,"../vehicle/".$img);
					$sql_vehicle_tbl_documents = $sql_vehicle_tbl_documents.",'$img'";
					
				}
					
			}
			
			else
			{
				$sql_vehicle_tbl_documents = $sql_vehicle_tbl_documents.",''";
			}
			
			if(!empty($_FILES["photo3_link"]["name"]))
			{
				$img=$_FILES["photo3_link"]["name"];

				//echo $img."</br>";
				$ext = pathinfo($img, PATHINFO_EXTENSION);


				//echo $ext."</br>";
				$img = basename($img,".".$ext);
				//echo $img."</br>";

				$img = rand(1111111111,9999999999).$img.rand(1111111111,9999999999).".".$ext;
				//echo $img."</br>";


				$tmp_name=$_FILES["photo3_link"]["tmp_name"];

				if(is_uploaded_file($tmp_name))
				{
					copy($tmp_name,"../vehicle/".$img);
					$sql_vehicle_tbl_documents = $sql_vehicle_tbl_documents.",'$img'";
					
				}
					
			}
			
			else
			{
				$sql_vehicle_tbl_documents = $sql_vehicle_tbl_documents.",''";
			}
			
			if(!empty($_FILES["rcBook_link"]["name"]))
			{
				$img=$_FILES["rcBook_link"]["name"];

				//echo $img."</br>";
				$ext = pathinfo($img, PATHINFO_EXTENSION);


				//echo $ext."</br>";
				$img = basename($img,".".$ext);
				//echo $img."</br>";

				$img = rand(1111111111,9999999999).$img.rand(1111111111,9999999999).".".$ext;
				//echo $img."</br>";


				$tmp_name=$_FILES["rcBook_link"]["tmp_name"];

				if(is_uploaded_file($tmp_name))
				{
					copy($tmp_name,"../vehicle/".$img);
					$sql_vehicle_tbl_documents = $sql_vehicle_tbl_documents.",'$img'";
					
				}
					
			}
			
			else
			{
				$sql_vehicle_tbl_documents = $sql_vehicle_tbl_documents.",''";
			}
			
			if(!empty($_FILES["insurance_link"]["name"]))
			{
				$img=$_FILES["insurance_link"]["name"];

				//echo $img."</br>";
				$ext = pathinfo($img, PATHINFO_EXTENSION);


				//echo $ext."</br>";
				$img = basename($img,".".$ext);
				//echo $img."</br>";

				$img = rand(1111111111,9999999999).$img.rand(1111111111,9999999999).".".$ext;
				//echo $img."</br>";


				$tmp_name=$_FILES["insurance_link"]["tmp_name"];

				if(is_uploaded_file($tmp_name))
				{
					copy($tmp_name,"../vehicle/".$img);
					$sql_vehicle_tbl_documents = $sql_vehicle_tbl_documents.",'$img'";
					
				}
					
			}
			
			else
			{
				$sql_vehicle_tbl_documents = $sql_vehicle_tbl_documents.",''";
			}
			
			if(!empty($_FILES["puc_link"]["name"]))
			{
				$img=$_FILES["puc_link"]["name"];

				//echo $img."</br>";
				$ext = pathinfo($img, PATHINFO_EXTENSION);


				//echo $ext."</br>";
				$img = basename($img,".".$ext);
				//echo $img."</br>";

				$img = rand(1111111111,9999999999).$img.rand(1111111111,9999999999).".".$ext;
				//echo $img."</br>";


				$tmp_name=$_FILES["puc_link"]["tmp_name"];

				if(is_uploaded_file($tmp_name))
				{
					copy($tmp_name,"../vehicle/".$img);
					$sql_vehicle_tbl_documents = $sql_vehicle_tbl_documents.",'$img'";
					
				}
					
			}
			
			else
			{
				$sql_vehicle_tbl_documents = $sql_vehicle_tbl_documents.",''";
			}
						
			
			
			$sql_vehicle_tbl_documents = $sql_vehicle_tbl_documents.")";
			$rs_vehicle_tbl_documents = mysqli_query($this->con,$sql_vehicle_tbl_documents);
			
			if(!$rs_vehicle_tbl_documents)
			{
				echo $sql_vehicle_tbl_documents."<br>";
				die ("Insert failed:".mysqli_error($this->con));
			}

			else
			{
				header('location:registerVehicle.php');

			} 
	
	}
//END vehicle register

//BEGIN vehicle Edit
	function vehicleRegisterEdit($vehicleId,$photo1_link,$photo2_link,$photo3_link,$rcBook_link,$insurance_link,$puc_link,$vehicle_type_id,$vehicle_category_id,$companyName,$modelName,$modelNumber,$licenseNumberPlate,$street,$city,$state,$weekdayStart,$weekdayEnd,$weekendStart,$weekendEnd,$door_number,$fuelType_id,$transmission,$seats,$luggageCapacity,$abs,$rate,$multiDayRenting,$overheadCarrier,$self_driving,$cruise_control)
	{
	
		//	sql_vehicle_tbl
			$sql_vehicle_tbl = "UPDATE vehicle_tbl 
								SET vehicle_type_id = '$vehicle_type_id',
								vehicle_category_id = '$vehicle_category_id',
								licenseNumberPlate = '$licenseNumberPlate',
								rate = '$rate'
								WHERE vehicleId = '$vehicleId'";
			
			$rs_vehicle_tbl = mysqli_query($this->con,$sql_vehicle_tbl);
		
		//	sql_vehicle_tbl_model
			$sql_vehicle_tbl_model = "UPDATE vehicle_tbl_model
									SET companyName = '$companyName',
									modelName = '$modelName',
									modelNumber = '$modelNumber'
									WHERE vehicleId = '$vehicleId'";
			$rs_vehicle_tbl_model = mysqli_query($this->con,$sql_vehicle_tbl_model);
		
		//	sql_vehicle_tbl_address
			$sql_vehicle_tbl_address = "UPDATE vehicle_tbl_address
										street = '$street',
										city = '$city',
										state = '$state',
										door_number = '$door_number'
										WHERE vehicleId = '$vehicleId'";
			$rs_vehicle_tbl_address = mysqli_query($this->con,$sql_vehicle_tbl_address);
			
		//	vehicle_tbl_schedule
			$sql_vehicle_tbl_schedule = "UPDATE  vehicle_tbl_schedule
										SET weekdayStart = '$weekdayStart' ,
										weekdayEnd ='$weekdayEnd', 
										weekendStart = '$weekendStart', 
										weekendEnd ='$weekendEnd', 
										multiDayRenting = '$multiDayRenting'
										WHERE vehicleId = '$vehicleId'";
			$rs_vehicle_tbl_schedule = mysqli_query($this->con,$sql_vehicle_tbl_schedule);
			
		//	vehicle_tbl_features
			$sql_vehicle_tbl_features = "UPDATE vehicle_tbl_features
										SET fuelType_id ='$fuelType_id',
										seats ='$seats', 
										transmission = '$transmission', 
										abs ='$abs',
										overheadCarrier = '$overheadCarrier',
										self_driving = '$self_driving',
										luggageCapacity = '$luggageCapacity',
										cruise_control = '$cruise_control'
										WHERE vehicleId = '$vehicleId'";
			$rs_vehicle_tbl_features = mysqli_query($this->con,$sql_vehicle_tbl_features);
																				
		//vehicle_tbl_documents

			$sql_vehicle_tbl_documents = "UPDATE  vehicle_tbl_documents
											SET ";				
			if(!empty($_FILES["photo1_link"]["name"]))
			{
				$img=$_FILES["photo1_link"]["name"];

				//echo $img."</br>";
				$ext = pathinfo($img, PATHINFO_EXTENSION);


				//echo $ext."</br>";
				$img = basename($img,".".$ext);
				//echo $img."</br>";

				$img = rand(1111111111,9999999999).$img.rand(1111111111,9999999999).".".$ext;
				//echo $img."</br>";


				$tmp_name=$_FILES["photo1_link"]["tmp_name"];

				if(is_uploaded_file($tmp_name))
				{
					copy($tmp_name,"../vehicle/".$img);
					$sql_vehicle_tbl_documents = $sql_vehicle_tbl_documents." photo1_link ='$img'";
					
				}
					
			}
			
			else
			{
				$sql_vehicle_tbl_documents = $sql_vehicle_tbl_documents." photo1_link =''";
			}
			
			if(!empty($_FILES["photo2_link"]["name"]))
			{
				$img=$_FILES["photo2_link"]["name"];

				//echo $img."</br>";
				$ext = pathinfo($img, PATHINFO_EXTENSION);


				//echo $ext."</br>";
				$img = basename($img,".".$ext);
				//echo $img."</br>";

				$img = rand(1111111111,9999999999).$img.rand(1111111111,9999999999).".".$ext;
				//echo $img."</br>";


				$tmp_name=$_FILES["photo2_link"]["tmp_name"];

				if(is_uploaded_file($tmp_name))
				{
					copy($tmp_name,"../vehicle/".$img);
					$sql_vehicle_tbl_documents = $sql_vehicle_tbl_documents.",photo2_link ='$img'";
					
				}
					
			}
			
			else
			{
				$sql_vehicle_tbl_documents = $sql_vehicle_tbl_documents.",photo2_link =''";
			}
			
			if(!empty($_FILES["photo3_link"]["name"]))
			{
				$img=$_FILES["photo3_link"]["name"];

				//echo $img."</br>";
				$ext = pathinfo($img, PATHINFO_EXTENSION);


				//echo $ext."</br>";
				$img = basename($img,".".$ext);
				//echo $img."</br>";

				$img = rand(1111111111,9999999999).$img.rand(1111111111,9999999999).".".$ext;
				//echo $img."</br>";


				$tmp_name=$_FILES["photo3_link"]["tmp_name"];

				if(is_uploaded_file($tmp_name))
				{
					copy($tmp_name,"../vehicle/".$img);
					$sql_vehicle_tbl_documents = $sql_vehicle_tbl_documents.",photo3_link ='$img'";
					
				}
					
			}
			
			else
			{
				$sql_vehicle_tbl_documents = $sql_vehicle_tbl_documents.",photo3_link =''";
			}
			
			if(!empty($_FILES["rcBook_link"]["name"]))
			{
				$img=$_FILES["rcBook_link"]["name"];

				//echo $img."</br>";
				$ext = pathinfo($img, PATHINFO_EXTENSION);


				//echo $ext."</br>";
				$img = basename($img,".".$ext);
				//echo $img."</br>";

				$img = rand(1111111111,9999999999).$img.rand(1111111111,9999999999).".".$ext;
				//echo $img."</br>";


				$tmp_name=$_FILES["rcBook_link"]["tmp_name"];

				if(is_uploaded_file($tmp_name))
				{
					copy($tmp_name,"../vehicle/".$img);
					$sql_vehicle_tbl_documents = $sql_vehicle_tbl_documents.",rcBook_link ='$img'";
					
				}
					
			}
			
			else
			{
				$sql_vehicle_tbl_documents = $sql_vehicle_tbl_documents.",rcBook_link =''";
			}
			
			if(!empty($_FILES["insurance_link"]["name"]))
			{
				$img=$_FILES["insurance_link"]["name"];

				//echo $img."</br>";
				$ext = pathinfo($img, PATHINFO_EXTENSION);


				//echo $ext."</br>";
				$img = basename($img,".".$ext);
				//echo $img."</br>";

				$img = rand(1111111111,9999999999).$img.rand(1111111111,9999999999).".".$ext;
				//echo $img."</br>";


				$tmp_name=$_FILES["insurance_link"]["tmp_name"];

				if(is_uploaded_file($tmp_name))
				{
					copy($tmp_name,"../vehicle/".$img);
					$sql_vehicle_tbl_documents = $sql_vehicle_tbl_documents.",insurance_link ='$img'";
					
				}
					
			}
			
			else
			{
				$sql_vehicle_tbl_documents = $sql_vehicle_tbl_documents.",insurance_link =''";
			}
			
			if(!empty($_FILES["puc_link"]["name"]))
			{
				$img=$_FILES["puc_link"]["name"];

				//echo $img."</br>";
				$ext = pathinfo($img, PATHINFO_EXTENSION);


				//echo $ext."</br>";
				$img = basename($img,".".$ext);
				//echo $img."</br>";

				$img = rand(1111111111,9999999999).$img.rand(1111111111,9999999999).".".$ext;
				//echo $img."</br>";


				$tmp_name=$_FILES["puc_link"]["tmp_name"];

				if(is_uploaded_file($tmp_name))
				{
					copy($tmp_name,"../vehicle/".$img);
					$sql_vehicle_tbl_documents = $sql_vehicle_tbl_documents.",puc_link ='$img'";
					
				}
					
			}
			
			else
			{
				$sql_vehicle_tbl_documents = $sql_vehicle_tbl_documents.",puc_link =''";
			}
						
			
			
			$sql_vehicle_tbl_documents = $sql_vehicle_tbl_documents." WHERE vehicleId = '$vehicleId'";
			//die($sql_vehicle_tbl_documents);
			$rs = mysqli_query($this->con,$sql_vehicle_tbl_documents);
			
			if(!$rs)
			{
				echo $sql_vehicle_tbl_documents."<br>";
				die ("Update failed:".mysqli_error($this->con));
			}

			else
			{
				header('location:registerVehicle.php');

			} 
	
	}
	
//End vehicle Edit
//BEGIN Change vehicle status
	function changeVehicleStatus($is_active,$vehicleId)
	{
		
		$sql = "UPDATE vehicle_tbl SET is_active = '$is_active' WHERE vehicleId='$vehicleId'";
		
		$rs = mysqli_query($this->con,$sql);
			
			if(!$rs)
			{
				die ("update failed:".mysqli_error($this->con));
			}

			else
			{
				echo "<script>window.location = 'registerVehicle.php'</script>";
				
			} 
	}
//END vehicle status

//BEGIN Delete vehicle 
	function deleteVehicle($vehicleId)
	{	

		$sql = "UPDATE vehicle_tbl SET delete_status = 0 WHERE vehicleId='$vehicleId'";
		$rs = mysqli_query($this->con,$sql);
			
		if(!$rs)
		{
			die ("Delete failed:".mysqli_error($this->con));
		}

		else
		{
			echo "<script>window.location = 'registerVehicle.php'</script>";
			
		} 
	}
//END delete vehicle

//BEGIN vehicle off day insert
	function vehicleOffDay($vehicleId,$startDate,$endDate,$reason,$totalDays)
	{
		$sql = "INSERT INTO vehicleholiday_tbl(vehicleId,startDate,endDate,reason,totalDays) VALUES('$vehicleId','$startDate','$endDate','$reason','$totalDays')";
			
			$rs = mysqli_query($this->con,$sql);
			
			if(!$rs)
			{
				die ("Holiday booking unsucessful.".mysqli_error($this->con));
			}
			
			else
			{
				echo "<script>window.location = 'vehicleOffDay.php'</script>";
			}
	}
//END vehicle off day insert

//BEGIN vehicle off day delete
	function deleteVehicleHoliday($vehicleHolidayId)
	{
		$sql = "UPDATE vehicleholiday_tbl 
				SET delete_status = 1
				WHERE vehicleHolidayId = '$vehicleHolidayId'";
			
			$rs = mysqli_query($this->con,$sql);
			
			if(!$rs)
			{
				die ("Day not removed.".mysqli_error($this->con));
			}
			
			else
			{
				echo "<script>window.location = 'vehicleOffDay.php'</script>";
			}
	}
//END vehicle off day delete

//BEGIN vehicle off day EDIT
	function vehicleOffDayEdit($vehicleId,$startDate,$endDate,$reason,$totalDays,$vehicleHolidayId)
	{
		$sql = "UPDATE vehicleholiday_tbl SET startDate = '$startDate' , endDate = '$endDate' ,reason = '$reason' ,totalDays = '$totalDays' WHERE vehicleHolidayId= '$vehicleHolidayId'";
			
			$rs = mysqli_query($this->con,$sql);
			
			if(!$rs)
			{
				die ("Holiday booking update unsucessful.".mysqli_error($this->con));
			}
			
			else
			{
				echo "<script>window.location = 'vehicleOffDay.php'</script>";
			}
	}
//END vehicle off day EDIT

//Start Start Ride
	function startRide($booking_id,$startOtp,$start_km)
	{
		$sql = "SELECT * FROM newbooking WHERE startOtp = $startOtp AND booking_id = '$booking_id' ";
		$ans = mysqli_query($this->con,$sql);
		$row = mysqli_fetch_array($ans);
		
		if(mysqli_num_rows($ans) > 0) 
		{								
			$sql2 = "UPDATE newbooking
					SET start_km = '$start_km', rideStart = 1
					WHERE booking_id = '$booking_id'";
			
			$rs2 = mysqli_query($this->con,$sql2);
			
			if(!$rs2)
			{
				die ("Problem Encountered.".mysqli_error($this->con));
			}
			
			else
			{
				echo "<script>window.location = 'upcomingRides.php?sucessStart=1'</script>";
			}
			
		} 
		
		else
		{
			header('location:upcomingRides.php?invalidOtp=1');
		}
	
		
			
			
	}
//End Start Ride

//Start END ride
	function endRide($booking_id,$end_km)
	{
		$sql = "SELECT * FROM newbooking WHERE booking_id = '$booking_id'";
		$ans = mysqli_query($this->con,$sql);
		$row = mysqli_fetch_array($ans);
		
		$startKm = $row['start_km'];
		$endKm = $end_km;
		$rate = $row['rate'];
		$cost = ($end_km - $startKm) * $rate;
		
		$sql2 = "UPDATE newbooking SET end_km = '$endKm' , amount_paid = '$cost' WHERE booking_id = '$booking_id' ";
		$rs2 = mysqli_query($this->con,$sql2);
		
		$sql3 = "INSERT INTO booking_history (SELECT * FROM newbooking WHERE booking_id = '$booking_id')";
		$rs3 = mysqli_query($this->con,$sql3);
		
		$sql4 = "DELETE FROM newbooking WHERE booking_id = '$booking_id'";
		$rs4 = mysqli_query($this->con,$sql4);
		
		if(!$rs2)
		{
			die ("Problem Encountered.".mysqli_error($this->con));
		}
		
		else
		{
			echo "<script>window.location = 'upcomingRides.php?sucessEnd=".$booking_id."'</script>";
		}
	}
//END END Ride
	}//END OF CLASS
	
	$obj = new dbclass();
	
	if(isset($_POST['btnLogin']))
	{

		$userName = $_POST['userName'];
		$password = $_POST['password'];
		
		
		$obj -> getLogin($userName,$password);	
		
		
	}
	
//BEGIN Forgot password
	
	if(isset($_POST['forgotPassword']))
	{
		$userName = $_POST['userName'];
		$recoveryEmail = $_POST['recoveryEmail'];
		
		$obj ->  forgotPassword($userName,$recoveryEmail);
	}
//END Forgot password

//BEGIN password reset
	if (isset($_POST['passwordReset'])) {
		
		$pwd =$_POST['pwd'];
		$pwd2 =$_POST['pwd2'];
		$userName = $_POST['userName'];
		$userName = base64_decode($userName);

		if( $pwd == $pwd2 ){
		
			$obj -> passwordReset($pwd,$pwd2,$userName);
					
		}
		else{

			die('password do not match');
		}
	}
//END password reset

//BEGIN Sign up

	if(isset($_POST['btnSignUp']))
	{
		$userName =$_POST['userName'];
		$firstName =$_POST['firstName'];
		$lastName = $_POST['lastName'];
		$email = $_POST['email'];
		$pwd1 = $_POST['pwd1'];
		$pwd2 = $_POST['pwd2'];



		if( $pwd1 == $pwd2 ){
		
	
		
			
			$obj -> signupRequest($userName,$firstName,$lastName,$email,$pwd1);
					
		}
		else{

			die('password do not match');
		}
	
	}
	
//END Sign up

//BEGIN UPDATE vehicle owner
	if(isset($_POST['updateVehicleOwner']))
	{
		$vehicleOwnerId = $_POST['vehicleOwnerId'];
		$file = $_FILES['imgUpload'];
		$firstName =$_POST['firstName'];
		$lastName = $_POST['lastName'];
		$email = $_POST['email'];
		$street =$_POST['street'];
		$state =$_POST['state'];
		$mobileNumber = $_POST['mobileNumber'];
		$pincode = $_POST['pincode'];
		$city = $_POST['city'];
		
		$obj -> updateVehicleOwner($vehicleOwnerId,$firstName,$lastName,$email,$street,$state,$mobileNumber,$pincode,$city,$file);
		
		
	}
//END UPDATE vehicle owner

//BEGIN change password
	
	if(isset($_POST['changePwd']))
	{
		$userName = $_POST['userName'];
		$pwd1 = $_POST['pwd1'];
		$pwd2 = $_POST['pwd2'];
		
		if($pwd1 == $pwd2)
		{
			$obj -> changePwd($userName,$pwd1);
		}
		
		else
		{
		
			echo "password donot match";
			header('location:profile.php');
		}
		
		
	}
	
//END change password

//BEGIN VEHICLE register
	if(isset($_POST['vehicleRegister_btn']))
	{
		$vehicleId = $_POST['vehicleId'];
		$photo1_link = $_FILES['photo1_link'];
		$photo2_link = $_FILES['photo2_link'];
		$photo3_link = $_FILES['photo3_link'];
		$rcBook_link = $_FILES['rcBook_link'];
		$insurance_link = $_FILES['insurance_link'];
		$puc_link = $_FILES['puc_link'];
		$vehicle_type_id = $_POST['vehicle_type_id'];
		$vehicle_category_id = $_POST['vehicle_category_id'];
		$companyName = $_POST['companyName'];
		$modelName = $_POST['modelName'];
		$modelNumber = $_POST['modelNumber'];
		$licenseNumberPlate = $_POST['licenseNumberPlate'];
		$street = $_POST['street'];
		$city = $_POST['city'];
		$state = $_POST['state'];
		$weekendEnd = $_POST['weekendEnd'];
		$weekendStart = $_POST['weekendStart'];
		$weekdayEnd = $_POST['weekdayEnd'];
		$weekdayStart = $_POST['weekdayStart'];
		$door_number = $_POST['door_number'];
		$fuelType_id = $_POST['fuelType_id'];
		$transmission = $_POST['transmission'];
		$seats = $_POST['seats'];
		$luggageCapacity = $_POST['luggageCapacity'];
		$abs = $_POST['abs'];
		$rate = $_POST['rate'];
		$multiDayRenting = $_POST['multiDayRenting'];
		$overheadCarrier = $_POST['overheadCarrier'];
		$self_driving = $_POST['self_driving'];
		$cruise_control = $_POST['cruise_control'];
		
		
		if(!isset($_POST['vehicleId']))
		{
			$obj -> vehicleRegister($photo1_link,$photo2_link,$photo3_link,$rcBook_link,$insurance_link,$puc_link,$vehicle_type_id,$vehicle_category_id,$companyName,$modelName,$modelNumber,$licenseNumberPlate,$street,$city,$state,$weekdayStart,$weekdayEnd,$weekendStart,$weekendEnd,$door_number,$fuelType_id,$transmission,$seats,$luggageCapacity,$abs,$rate,$multiDayRenting,$overheadCarrier,$self_driving,$cruise_control);
		}
		
		else
		{
			$obj -> vehicleRegisterEdit($vehicleId,$photo1_link,$photo2_link,$photo3_link,$rcBook_link,$insurance_link,$puc_link,$vehicle_type_id,$vehicle_category_id,$companyName,$modelName,$modelNumber,$licenseNumberPlate,$street,$city,$state,$weekdayStart,$weekdayEnd,$weekendStart,$weekendEnd,$door_number,$fuelType_id,$transmission,$seats,$luggageCapacity,$abs,$rate,$multiDayRenting,$overheadCarrier,$self_driving,$cruise_control);
		
		}
		
	}

//END VEHICLE register

//BEGIN vehicleOffDay
	if(isset($_POST['vehicleOffDay']))
	{
		$vehicleHolidayId = $_POST['vehicleHolidayId'];
		
		$vehicleId = $_POST['vehicleId'];
		$startDate = $_POST['startDate'];
		$endDate = $_POST['endDate'];
		$reason = $_POST['reason'];
	
	//BEGIN Function to count days between 2 dates
		$totalDays = strtotime($endDate) - strtotime($startDate);   
		// 1 day = 24 hours 
		// 24 * 60 * 60 = 86400 seconds 
		$totalDays = abs(round($totalDays / 86400)); 
	//END Function to count days between 2 dates
		
		if(!isset($vehicleHolidayId))
		{
			
			$obj -> vehicleOffDay($vehicleId,$startDate,$endDate,$reason,$totalDays);
		}
		
		else
		{
			$obj -> vehicleOffDayEdit($vehicleId,$startDate,$endDate,$reason,$totalDays,$vehicleHolidayId);	
		}
	}
//END vehicleOffDay	
//Start Start Vehicle
	if(isset($_POST['start_Ride_btn']))
	{
		$startOtp = $_POST['startOtp'];
		$start_km = $_POST['start_km'];
		$booking_id = $_POST['booking_id'];
		
		$obj -> startRide($booking_id,$startOtp,$start_km);
	}
//End Start Vehicle

//Start END Vehicle
	if(isset($_POST['end_ride_btn']))
	{
		
		$end_km = $_POST['end_km'];
		$booking_id = $_POST['booking_id'];
		
		$obj -> endRide($booking_id,$end_km);
	}
//End END Vehicle

?>
