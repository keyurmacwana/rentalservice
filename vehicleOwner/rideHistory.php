<?php
	include_once('header.php');
?>



<!-- Main Body content starts here -->
        <div id="wrapper">
            <!-- Sidebar -->
            <div id="sidebar-wrapper">
                <aside class="sidebar">
                    <nav class="sidebar-nav" id="sidebarscroll">
                        <ul class="metismenu ripple" id="menu">
						
								<li  >
									<a href="dashboard.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-folder-open fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Dashboard</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li >
									<a href="ownerWallet.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-money fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Wallet</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li >
									<a href="upcomingRides.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-tachometer fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Upcoming Rides</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li style="background-color:#505464;">
									<a href="upcomingRides.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-clock-o  fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Rides History</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li >
									<a href="registerVehicle.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-plus fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Add Vehicles</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								<li >
									<a href="vehicleOffDay.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-calendar fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Reserve Vehicle</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								<li >
									<a href="termsAndConditions.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-legal notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Terms & Conditions</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li >
									<a href="ownerFAQ.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-question-circle fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">FAQ</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
	
								
                            
                        </ul>
                    </nav>
                </aside>
            </div>
            <!-- # Sidebar-wrapper -->
            
			<!-- Page Content-wrapper -->
			<div id="page-content-wrapper">

			
				<!-- Breadcrumb  -->
				<div class="row csk-breadcrumb">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
						<h4 class="page-title">Upcoming Rides</h4>
					</div>
					<div class="col-lg-9 col-md-8 col-sm-8 hidden-xs">
						<ol class="breadcrumb">
							<li><a href="dashboard.php">Dashboard</a></li>
							<li><a href="#">Start Ride</a></li>
						</ol>
					</div>
				</div>
				<!-- #Breadcrumb -->
				<!-- row -->
				
				<!-- row -->
				
				
				<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default panel-with-options">
			
				<h3 >&emsp;Upcoming Rides</h3>
			
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered datatable">
						<thead>
							<tr>
								<th>SR NO.</th>
								<th>Earned</th>
								<th>Total Kilometers</th>
								<th>Model</th>
								<th>Start Date and Time</th>
								<th>End Date and Time</th>
								<th>Rate</th>
								<th>Customer Name</th>
								
								
							</tr>
						</thead>
						<tfoot>
						<tr>
								<th>SR NO.</th>
								<th>Earned</th>
								<th>Total Kilometers</th>
								<th>Model</th>
								<th>Start Date and Time</th>
								<th>End Date and Time</th>
								<th>Rate</th>
								<th>Customer Name</th>
								
							</tr>
						</tfoot>
						<tbody>
							
							<?php 
								//To fetch table data
									$sql_history = "SELECT	owner.*,booking.*,customer.*,vehicle.*,vt_model.*
									
															FROM booking_history booking
															
															LEFT JOIN vehicle_tbl vehicle 
															ON vehicle.vehicleId = booking.vehicleId
															
															LEFT JOIN vehicle_tbl_model vt_model 
															ON vt_model.vehicleId = vehicle.vehicleId
															
															LEFT JOIN owner_tbl owner
															ON owner.vehicleOwnerId = booking.vehicleOwnerId
															
															LEFT JOIN customer_tbl customer 
															ON customer.customerId = booking.customerId
															
															WHERE owner.vehicleOwnerId = '".$data['vehicleOwnerId']."'";	
													
																
														
									$ans_history = mysqli_query($obj->con,$sql_history);
								//END fetch table data
								$counter = 1;
								while($row_history = mysqli_fetch_array($ans_history))
								{
									
							?>
								<tr>
									<td><?php	echo $counter ++; ?></td>
									<td><?php   
											$total_km = $row_history['end_km'] - $row_history['start_km'];
											$amount = $total_km * $row_history['rate'];
											echo "&#8377;".$amount;
										?>
									</td>
									<td><?php echo $total_km;?>
									</td>
										
									<td><?php echo $row_history['modelNumber'].",".$row_history['modelName'].",".$row_history['companyName']; ?>
									</td>
									<td><?php echo $row_history['actual_startTime']; ?>
									</td>
									<td>
										<?php echo $row_history['actual_endTime']; ?>
									</td>
									<td><?php echo $row_history['rate'];?>
									</td>
									<td><?php echo $row_history['fullName'];?>
									
								</tr>
							<?php
								}
							?>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
	

<?php
	include_once('footer.php');
?>