</div>
        </div>
        <!-- # Page-content-wrapper -->
        </div>
        <!-- # Wrapper -->
        <!-- GoogleApi jQuery -->
        <script type="text/javascript" src="datas/assets/jquery-1.12.4/jquery-1.12.4.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="datas/bootstrap/js/bootstrap.min.js"></script>
		<!-- Jquery-ui JS -->
        <script src="datas/js/jquery-ui.js"></script>
        <!-- Metis menu min.JS -->
        <script type="text/javascript" src="datas/assets/metisMenu-master/dist/metisMenu.min.js"></script>
        <!-- Slim Scroll -->
        <script type="text/javascript" src="datas/assets/jQuery-slimScroll-1.3.8/jquery.slimscroll.js"></script>
        <script type="text/javascript" src="datas/js/app.js"></script>
		
		<!-- Page Level JS -->
		<script type="text/javascript" src="datas/assets/bootstrap-table-develop/dist/bootstrap-table.min.js"></script>
		<script type="text/javascript" src="datas/assets/DataTables-1.10.12/media/js/jquery.dataTables.js"></script>
		<script type="text/javascript" src="datas/assets/DataTables-1.10.12/media/js/dataTables.bootstrap.js"></script>
		<script>
			$(function() {
				var $table = $('#table-transform');
				$('#transform').click(function() {
						$table.bootstrapTable();
				});
				$('#destroy').click(function() {
						$table.bootstrapTable('destroy');
				});
			});
		</script>
		<script type="text/javascript" class="init">
			$(document).ready(function() {
				$('.datatable').DataTable();
			});
		</script>
        <!-- High Chats JS -->
        <script type="text/javascript" src="datas/assets/Highcharts-4.2.5/js/highcharts.js"></script>
        <script src="datas/assets/Highcharts-4.2.5/js/highcharts-more.js"></script>
        <!-- Highchart-data JS -->
        <script type="text/javascript" src="datas/js/highcharts-data.js"></script>
        <!-- 3D chart JS -->
        <script src="datas/assets/Highcharts-4.2.5/js/highcharts-3d.js"></script>
        <script type="text/javascript" src="datas/assets/chartist-js-develop/dist/chartist.js"></script>
        <!-- jQuery Sparklines -->
        <script src="datas/js/jquery.sparkline.min.js"></script>
        <script type="text/javascript" src="datas/js/project-dashboard.js"></script>
		<!-- Date and range picker JS -->
        <script type="text/javascript" src="datas/assets/bootstrap-daterangepicker-master/moment.min.js"></script>
        <script type="text/javascript" src="datas/assets/bootstrap-daterangepicker-master/daterangepicker.js"></script>
		 <!-- Time Picker JS -->
        <script type="text/javascript" src="datas/assets/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
        <script src="datas/js/timepicker.js"></script>
        <script type="text/javascript" src="datas/assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
		<!-- Touch Spin JS -->
        <script src="datas/assets/bootstrap-touchspin-master/dist/jquery.bootstrap-touchspin.min.js"></script>
        <script src="datas/assets/bootstrap-touchspin-master/src/jquery.bootstrap-touchspin.js"></script>
        <!-- Forms JS  -->
        <script type="text/javascript" src="datas/js/forms.js"></script>
        <script type="text/javascript" src="datas/js/formelements.js"></script>
		<!-- Multi Select JS -->
        <script type="text/javascript" src="datas/assets/bootstrap-multiselect-master/dist/js/bootstrap-multiselect.js"></script>
        <!-- Lou Jquery.multi-select JS -->
        <script type="text/javascript" src="datas/assets/lou-multi-select-cf6d6c6/js/jquery.multi-select.js"></script>
        <!-- Bootstrap-select-master JS -->
        <script src="datas/assets/bootstrap-select-master/dist/js/bootstrap-select.js"></script>
        <!-- Button-select JS -->
        <script src="datas/js/buttonselect.js"></script>
        <!-- Input Mask JS -->
        <script type="text/javascript" src="datas/assets/masked-input/jquery.maskedinput.js"></script>
    </body>


</html>