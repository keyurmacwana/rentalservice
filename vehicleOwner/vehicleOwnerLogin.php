
<!DOCTYPE html>
<html lang="en">
	
<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<title>Sign In | RS - 
		owner | Retal Services</title>
		<!-- Icon -->
		<link rel="icon" href="datas/images/icon.ico">
		<!-- Bootstrap Core CSS -->
		<link href="datas/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<!-- Animate CSS -->
		<link rel="stylesheet" href="datas/assets/animate.css-master/animate.min.css">
		<!-- Theme  CSS -->
		<link rel="stylesheet" type="text/css" href="datas/min/csk.min.css">
		<!-- MediaQuery CSS -->
		<link rel="stylesheet" type="text/css" href="datas/css/media-query.css">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<!--Container Starts -->
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-md-push-4 col-xs-12 sign-box">
					<div class="panel panel-default pale-panel-border-top">
						<div class="panel-heading text-center">
							<h3 class="nm np">
							Vehicle Owner Login
							</h3>
						</div>
						<div class="panel-body">
							<br>
							<form class="form" action="dbclass.php" method="post">
								<input id="userName" name="userName" type="text" data-length="5,15" placeholder="Username" required="required" />
								<input id="password" name="password" type="password" required="required" placeholder="Password" />	
						
								
								
						
								<button type="submit" id="btnLogin" name="btnLogin" class="center btn btn-success btn-lg btn-block" data-dismiss="modal">
								<i class="fa fa-lock"></i> Secure Login
								</button>
							</form>
							<form action="vehicleOwnerSignup.php">
								<button type="submit" id="btnSignup" name="btnSignup" class="center btn btn-warning btn-lg btn-block" data-dismiss="modal">
								<i class="fa fa-rocket"></i> Request Sign Up
								</button>
							</form>
							
							<label><?php $msg;?></label>
							<div class="text-right">
								<small><a href="forgot.php?msg=" class="text-danger">Forgot Password?</a></small>
							</div>
							<hr>
							<!--<div class="text-center">
								<h4>
								Don't have an Account ? <br><a href="sign-up.html">Sign Up here </a>
								</h4>
							</div>-->
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- #Container Ends -->
		<!-- GoogleApi jQuery -->
		<script type="text/javascript" src="datas/assets/jquery-1.12.4/jquery-1.12.4.js"></script>
		<!-- Bootstrap Core JavaScript -->
		<script src="datas/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="datas/js/sisur.js"></script>
	</body>


</html>