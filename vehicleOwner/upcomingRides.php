<?php
	include_once('header.php');
?>

<?php
	if(isset($_GET['invalidOtp']))
	{
?>
<script>
	 alert("Invalid OTP")
</script>
<?php
	}
?>

<?php
	if(isset($_GET['sucessStart']))
	{
?>
<script>
	 alert("Trip Started")
</script>
<?php
	}
?>


<!-- Main Body content starts here -->
        <div id="wrapper">
            <!-- Sidebar -->
            <div id="sidebar-wrapper">
                <aside class="sidebar">
                    <nav class="sidebar-nav" id="sidebarscroll">
                        <ul class="metismenu ripple" id="menu">
						
								<li  >
									<a href="dashboard.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-folder-open fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Dashboard</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li >
									<a href="ownerWallet.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-money fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Wallet</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li style="background-color:#505464;">
									<a href="upcomingRides.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-tachometer fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Upcoming Rides</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li >
									<a href="rideHistory.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-clock-o  fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Rides History</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li >
									<a href="registerVehicle.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-plus fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Add Vehicles</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								<li >
									<a href="vehicleOffDay.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-calendar fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Reserve Vehicle</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								<li >
									<a href="termsAndConditions.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-legal notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Terms & Conditions</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li >
									<a href="ownerFAQ.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-question-circle fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">FAQ</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
	
								
                            
                        </ul>
                    </nav>
                </aside>
            </div>
            <!-- # Sidebar-wrapper -->
            
			<!-- Page Content-wrapper -->
			<div id="page-content-wrapper">

			
				<!-- Breadcrumb  -->
				<div class="row csk-breadcrumb">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
						<h4 class="page-title">Upcoming Rides</h4>
					</div>
					<div class="col-lg-9 col-md-8 col-sm-8 hidden-xs">
						<ol class="breadcrumb">
							<li><a href="dashboard.php">Dashboard</a></li>
							<li><a href="#">Start Ride</a></li>
						</ol>
					</div>
				</div>
				<!-- #Breadcrumb -->
				<!-- row -->
				
				<!-- row -->
				<?php
			if(isset($_GET['startRide_btn']))
				{
				?>
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-defaut">
							<div class="panel-heading">
								<h3 class="panel-title">Start Ride</h3>
							</div>
							<div class="panel-body">
								<form action="dbclass.php" method="post">
									<div class="form-body">
										
										<div class="row">
											<div class="col-md-3">
												<div class="form-group">
													<label class="control-label">OTP :</label>
													<input type="number" id="startOtp" name="startOtp">
													<input type="hidden" id="booking_id" name="booking_id" value="<?php echo $_GET['startRide_btn']; ?>">
												</div>
											</div>
											
										</div>
										<div class="row">
											<div class="col-md-3">
												<div class="form-group">
													<label class="control-label">Kilometer Reading</label>
													<input type="number" id="start_km" name="start_km">
												</div>
											</div>
											
										</div>
										<div class="row">
											<div class="col-md-3">
												<div class="form-group">
													<label class="control-label">Expected Return Time and Date</label>
													<input type="text" value="<?php echo $_GET['end_time']." on ".$_GET['end_date']?>" disabled>
												</div>
											</div>
											
										</div>
										
										
									</div>
									<div class="form-actions">
										<button type="submit" class="btn btn-success" name="start_Ride_btn" id="start_Ride_btn"> <i class="fa fa-check"></i> Start Ride</button>
										<button type="button" class="btn btn-danger">Cancel</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<?php
			}
				?>
				<!-- #row -->
				
				<?php
			if(isset($_GET['sucessEnd']))
				{
					$sql_invoice = "SELECT history.*,vehicle_model.*
									
									FROM booking_history history
									
									LEFT JOIN  vehicle_tbl_model vehicle_model
									ON vehicle_model.vehicleId = history.vehicleId
									
									WHERE history.booking_id = '".$_GET['sucessEnd']."'";
									
					//die($sql_invoice);
					$ans_invoice = mysqli_query($obj->con,$sql_invoice);
					$row_invoice = mysqli_fetch_array($ans_invoice);
		
				?>
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-defaut">
							<div class="panel-heading">
								<h3 class="panel-title">Start Ride</h3>
							</div>
							<div class="panel-body">
								<form action="dbclass.php" method="post">
									<div class="form-body">
										
										<div class="row">
											<div class="col-md-3">
												<div class="form-group">
													<label class="control-label">Total Amount</label>
													<input type="number" value="<?php echo $row_invoice['amount_paid'];?>" disabled>
												</div>
											</div>
																						
										</div>
										<div class="row">
											<div class="col-md-3">
												<div class="form-group">
													<label class="control-label">Vehicle</label>
													<input type="text" value="<?php echo $row_invoice['companyName']." ".$row_invoice['modelName']." ".$row_invoice['modelNumber'];?>" disabled>
												</div>
											</div>
											<div class="col-md-3">
												<div class="form-group">
													<label class="control-label">Kilometer Driven</label>
													<input type="number" value="<?php $total = $row_invoice['end_km'] - $row_invoice['start_km'];echo $total;?>" disabled>
												</div>
											</div>
											<div class="col-md-3">
												<div class="form-group">
													<label class="control-label">Rate</label>
													<input type="number" value="<?php echo $row_invoice['rate'];?>" disabled>
												</div>
											</div>
											
										</div>
										<div class="row">
											<div class="col-md-3">
												<div class="form-group">
													<label class="control-label">Start Time</label>
													<input type="text" value="<?php echo $row_invoice['actual_startTime'];?>">
												</div>
											</div>
											<div class="col-md-3">
												<div class="form-group">
													<label class="control-label">End Time</label>
													<input type="text" value="<?php echo $row_invoice['actual_endTime'];?>">
												</div>
											</div>
											
										</div>
										
										
									</div>
									
								</form>
							</div>
						</div>
					</div>
				</div>
				<?php
			}
				?>
				
				<!-- row -->
				<?php
			if(isset($_GET['endRide_btn']))
				{
				?>
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-defaut">
							<div class="panel-heading">
								<h3 class="panel-title">End Ride</h3>
							</div>
							<div class="panel-body">
								<form action="dbclass.php" method="post">
									<div class="form-body">
										<div class="row">
											<div class="col-md-3">
												<div class="form-group">
													<label class="control-label">Kilometer Reading</label>
													<input type="number" id="end_km" name="end_km">
													<input type="hidden" id="booking_id" name="booking_id" value="<?php echo $_GET['endRide_btn']?>">
												</div>
											</div>
											
										</div>
										
										
										
									</div>
									<div class="form-actions">
										<button type="submit" class="btn btn-success" name="end_ride_btn" id="end_ride_btn"> <i class="fa fa-check"></i> End Ride</button>
										<button type="button" class="btn btn-danger">Cancel</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<?php
			}
				?>
				
				<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default panel-with-options">
			
				<h3 >&emsp;Upcoming Rides</h3>
			
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered datatable">
						<thead>
							<tr>
								<th>SR NO.</th>
								<th>Status</th>
								<th>Action</th>
								<th>Model</th>
								<th>Start Date and Time</th>
								<th>End Date and Time</th>
								<th>Rate</th>
								<th>Customer Name</th>
								<th>Contact</th>
								
							</tr>
						</thead>
						<tfoot>
						<tr>
								<th>SR NO.</th>
								<th>Status</th>
								<th>Action</th>
								<th>Model</th>
								<th>Start Date and Time</th>
								<th>End Date and Time</th>
								<th>Rate</th>
								<th>Customer Name</th>
								<th>Contact</th>
							</tr>
						</tfoot>
						<tbody>
							
							<?php 
								//To fetch table data
									$sql_upcoming_booking = "SELECT	owner.*,booking.*,customer.*,vehicle.*,vt_model.*
									
															FROM newbooking booking
															
															LEFT JOIN vehicle_tbl vehicle 
															ON vehicle.vehicleId = booking.vehicleId
															
															LEFT JOIN vehicle_tbl_model vt_model 
															ON vt_model.vehicleId = vehicle.vehicleId
															
															LEFT JOIN owner_tbl owner
															ON owner.vehicleOwnerId = booking.vehicleOwnerId
															
															LEFT JOIN customer_tbl customer 
															ON customer.customerId = booking.customerId
															
															WHERE owner.vehicleOwnerId = '".$data['vehicleOwnerId']."'";	
													
																
														
									$ans_upcoming_booking = mysqli_query($obj->con,$sql_upcoming_booking);
								//END fetch table data
								$counter = 1;
								while($row_upcoming_booking = mysqli_fetch_array($ans_upcoming_booking))
								{
									
							?>
								<tr>
									<td><?php	echo $counter ++; ?></td>
									<td><?php   if($row_upcoming_booking['rideStart'] == 1)
												{
													echo "<span class='label label-success'>Active</span>(".$row_upcoming_booking['actual_startTime'].")";
												}
												else
												{
													echo "<span class='label label-fail'>Upcoming</span>";
												}
										?>
									</td>
									<td><?php	 if($row_upcoming_booking['rideStart'] == 1)
												{
													echo "<a href='upcomingRides.php?endRide_btn=".$row_upcoming_booking['booking_id']."'><button class='btn btn-danger' style='width: 100%; height: 20%;'>End Ride</button></a>";
												}
												else
												{
													echo "<a href='upcomingRides.php?startRide_btn=".$row_upcoming_booking['booking_id']."&end_time=".$row_upcoming_booking['end_time']."&end_date=".$row_upcoming_booking['end_date']."'><button class='btn btn-success' style='width: 100%; height: 20%;'>Start Ride</button></a>";
												}
										 ?>
									</td>	
									<td><?php echo $row_upcoming_booking['modelNumber'].",".$row_upcoming_booking['modelName'].",".$row_upcoming_booking['companyName']; ?>
									</td>
									<td><?php echo $row_upcoming_booking['start_time']." on ".$row_upcoming_booking['start_date']; ?>
									</td>
									<td>
										<?php echo $row_upcoming_booking['end_time']." on ".$row_upcoming_booking['end_date']; ?>
									</td>
									<td><?php echo $row_upcoming_booking['rate'];?>
									</td>
									<td><?php echo $row_upcoming_booking['fullName'];?>
									<td><?php echo $row_upcoming_booking['mobile'];?>
									</td>
								</tr>
							<?php
								}
							?>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
	

<?php
	include_once('footer.php');
?>