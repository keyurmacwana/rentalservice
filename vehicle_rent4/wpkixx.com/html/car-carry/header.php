<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from wpkixx.com/html/car-carry/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 19 Feb 2020 06:54:34 GMT -->
<head>
    <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="" />
    <meta name="keywords" content="" />
	<title>Car Carry</title>
    <link rel="icon" type="image/png" href="images/fav.png">

    <link rel="stylesheet" href="css/apps.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/color.css">
    <link rel="stylesheet" href="css/responsive.css">
	
	<!-- REVOLUTION STYLE SHEETS -->
	<link rel="stylesheet" type="text/css" href="css/revolution/settings.css" />
	<link rel="stylesheet" type="text/css" href="css/revolution/navigation.css" />
	<link rel="stylesheet" type="text/css" href="css/revolution/pe-icon-7-stroke/css/pe-icon-7-stroke.css">

</head>
<body>
	<div class="site-layout">
		
<?php 
	include_once('beforeLogin.php');

	//Check for after login
?>
		
		<div class="topbar">
        	<div class="container">
            	<div class="row">
                	<div class="col-md-12">
                        <header>
                            <div class="logo">
                                <a href="index.html" title="index.html"><img src="images/logo-black.png" alt=""></a>
                            </div>
                            <nav>
                                <ul class="main-menu">
                                    <li><a href="index.html" title="">Home</a>
										<ul>
											<li><a href="index-after-login.html" title="">page after login</a></li>
										</ul>
									</li>
                                    <li><a href="#" title="">cars</a>
										<ul>
											<li><a href="cars.html" title="">cars</a></li>
											<li><a href="car-profile.html" title="">car profile</a></li>
											<li><a href="car-detail.html" title="">cars detail</a></li>
											<li><a href="create-route-notification.html" title="">route notification</a></li>
											<li><a href="submit-car-intro.html" title="">Submit car introduction</a></li>
											<li><a href="submit-car-for-rent.html" title="">submit car for rent</a></li>
											<li><a href="submit-route.html" title="">submit car route</a></li>
										</ul>
									</li>
                                    <li><a href="#" title="">rides</a>
										<ul>
											<li><a href="ride-listing.html" title="">ride listing</a></li>
											<li><a href="ride-request.html" title="">ride request</a></li>
											<li><a href="ride-detail.html" title="">ride detail</a></li>
											<li><a href="ride-requests-list.html" title="">ride request list</a></li>
										</ul>
									</li>
									<li><a href="#" title="">leasing</a>
										<ul>
											<li><a href="leasing.html" title="">Leasing Page</a></li>
											<li><a href="leasing-car-step1.html" title="">leasing Step 1</a></li>
											<li><a href="leasing-car-step2.html" title="">leasing step 2</a></li>
										</ul>
									</li>
									<li><a href="#" title="">profiles</a>
										<ul>
											<li><a href="user-profile.html" title="">Profile user</a></li>
											<li><a href="user-profile-v2.html" title="">Profile user v2</a></li>
											<li><a href="my-profile.html" title="">My profile</a></li>
											<li><a href="profile-balance.html" title="">profile balance</a></li>
											<li><a href="profile-earning.html" title="">profile earning</a></li>
											<li><a href="profile-edit.html" title="">profile edit page</a></li>
											<li><a href="profile-invite-friend.html" title="">profile invite friend</a></li>
											<li><a href="profile-points.html" title="">profile points</a></li>
										</ul>
									</li>
									<li><a href="#" title=""><i class="fa fa-ellipsis-v"></i></a>
										<ul>
											<li><a href="login.php" title="">Login page</a></li>
											<li><a href="register.html" title="">Register Page</a></li>
											<li><a href="how-this-work.html" title="">how it's work</a></li>
											<li><a href="faq.html" title="">faq's</a></li>
											<li><a href="points.html" title="">poinst</a></li>
											<li><a href="coming-soon.html" title="">Coming Soon</a></li>
											<li><a href="404.html" title="">404 Page</a></li>
											
										</ul>
									</li>
                                </ul>
                            </nav>
                            <div class="time-info">
								<i class="flaticon-phone-call-1"></i>
								<span>Whatsapp now <em>+1-124-3456-2</em></span>
							</div>
                        </header>
                    </div>
                </div>
            </div>
        </div><!-- topbar header -->
        
		<div class="responsive-header">
			<div class="res-top">
				<ul>
					<li><a href="#" title="Home"><i class="flaticon-home"></i></a></li>
					<li><a href="#" title="Faq's"><i class="flaticon-info"></i></a></li>
					<li><a href="#" title="Support center"><i class="flaticon-support"></i></a></li>
					<li><a href="#" title="Login"><i class="flaticon-unlocked"></i></a></li>
					<li><a href="#" title="New register"><i class="flaticon-checked"></i></a></li>
					<li class="post-new"><a href="#" title="New post">+Post</a></li>
				</ul>
			</div>
			<div class="logomenu-bar">
				<div class="logo"><a href="index.html" title=""><img src="images/logo-black.png" alt=""></a></div>
				<span class="responsviemenu-btn"><i class="flaticon-menu-1"></i></span> 
			</div>
			<div class="responsive-menu">
				<span class="close-btn"><i class="flaticon-error-1"></i></span>
				<ul>
					<li><a href="index.html" title="">Home</a></li>
					<li class="menu-item-has-children"><a href="#" title="">cars</a>
						<ul class="sub-menu">
							<li><a href="index-after-login.html" title="">page after login</a></li>
							<li><a href="cars.html" title="">cars</a></li>
							<li><a href="car-profile.html" title="">car profile</a></li>
							<li><a href="car-detail.html" title="">cars detail</a></li>
							<li><a href="create-route-notification.html" title="">route notification</a></li>
							<li><a href="submit-car-intro.html" title="">Submit car introduction</a></li>
							<li><a href="submit-car-for-rent.html" title="">submit car for rent</a></li>
							<li><a href="submit-route.html" title="">submit car route</a></li>
						</ul>
					</li>
					<li class="menu-item-has-children"><a href="#" title="">rides</a>
						<ul class="sub-menu">
							<li><a href="ride-listing.html" title="">ride listing</a></li>
							<li><a href="ride-request.html" title="">ride request</a></li>
							<li><a href="ride-detail.html" title="">ride detail</a></li>
							<li><a href="ride-requests-list.html" title="">ride request list</a></li>
						</ul>
					</li>
					<li class="menu-item-has-children"><a href="#" title="">leasing</a>
						<ul class="sub-menu">
							<li><a href="leasing.html" title="">Leasing Page</a></li>
							<li><a href="leasing-car-step1.html" title="">leasing Step 1</a></li>
							<li><a href="leasing-car-step2.html" title="">leasing step 2</a></li>
						</ul>
					</li>
					<li class="menu-item-has-children"><a href="#" title="">profiles</a>
						<ul class="sub-menu">
							<li><a href="user-profile.html" title="">Profile user</a></li>
							<li><a href="user-profile-v2.html" title="">Profile user v2</a></li>
							<li><a href="my-profile.html" title="">My profile</a></li>
							<li><a href="profile-balance.html" title="">profile balance</a></li>
							<li><a href="profile-earning.html" title="">profile earning</a></li>
							<li><a href="profile-edit.html" title="">profile edit page</a></li>
							<li><a href="profile-invite-friend.html" title="">profile invite friend</a></li>
							<li><a href="profile-points.html" title="">profile points</a></li>
						</ul>
					</li>
					<li class="menu-item-has-children"><a href="#" title="">more pages</a>
						<ul class="sub-menu">
							<li><a href="login.php" title="">Login page</a></li>
							<li><a href="register.html" title="">Register Page</a></li>
							<li><a href="how-this-work.html" title="">how it's work</a></li>
							<li><a href="faq.html" title="">faq's</a></li>
							<li><a href="points.html" title="">poinst</a></li>
							<li><a href="coming-soon.html" title="">Coming Soon</a></li>
							<li><a href="404.html" title="">404 Page</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div><!-- Responsive Header -->