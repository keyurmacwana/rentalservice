	<!DOCTYPE html>
<html lang="en">


<head>

    <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="" />
    <meta name="keywords" content="" />
	<title>RentHire Vehicles</title>
    <link rel="icon" type="image/png" href="images/favicon.ico">

    <link rel="stylesheet" href="css/apps.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/color.css">
    <link rel="stylesheet" href="css/responsive.css">
	
	<!-- REVOLUTION STYLE SHEETS -->
	<link rel="stylesheet" type="text/css" href="css/revolution/settings.css" />
	<link rel="stylesheet" type="text/css" href="css/revolution/navigation.css" />
	<link rel="stylesheet" type="text/css" href="css/revolution/pe-icon-7-stroke/css/pe-icon-7-stroke.css">

	<style>
		.logoCustomDefault {
		  display: block;
		  text-indent: -9999px;
		  width: 150px;
		  height: 50px;
		  background: url(images/logo-black.svg);
		  background-size: 150px 50px;
		}

		.logoCustomSecondary {
		  display: block;
		  text-indent: -9999px;
		  width: 150px;
		  height: 50px;
		  background: url(images/logo-black-secondary.svg);
		  background-size: 150px 50px;
		}
	</style>
</head>
<body>
	<div class="site-layout">
		
<?php 
	
	include_once('dbclass.php');
	if(isset($_COOKIE['userName']))
	{
		
		$obj = new dbclass();
		$data = $obj -> getCustomerData($_COOKIE['userName']);
//		die($data['fullName']);
		include_once('afterLogin.php');
		
		if(isset($_COOKIE['bookVehicleId']))
		{
			
			if($_COOKIE['redirectOnce'] == 1)
			{
				
				setcookie('redirectOnce','0');
				
				echo "<script>window.location = 'car-detail.php'</script>";
			}
		}
	}
	
	else
	{
		include_once('beforeLogin.php');
	}
	
	
	$sql_vType = "SELECT * FROM vehicle_type_tbl WHERE delete_status = 0";
	$rs_vType = mysqli_query($obj->con,$sql_vType);
	if(!$rs_vType)
	{
		die('No Vehicle Type FOUUND.'.mysqli_error($obj->con));
	}
	
	
	
?>
		
		<div class="topbar" >
        	<div class="container" >
            	<div class="row">
                	<div class="col-md-12">
                        <header>
                            <div class="logo">
                                <a href="index.php" title="index.php" class="logoCustomSecondary">hireRent</a>
                            </div>
                            <nav>
                                <ul class="main-menu">
                                    <li><a href="index.php" title="" >HOME</a></li>
                                    <?php 
										while($row_vType = mysqli_fetch_array($rs_vType))
										{?>
											<li><a href="bookCar.php?vt_id=<?php echo $row_vType['vehicle_type_id']; ?>" title="" ><?php echo $row_vType['vehicle_type']; ?></a>
											<ul>
										<?php
											$sql_vSubCategory = "SELECT * FROM vehicle_category_tbl WHERE vehicle_type_id = '".$row_vType['vehicle_type_id']."' AND delete_status = 0";
											$rs_vSubCategory = mysqli_query($obj->con,$sql_vSubCategory);
											if(!$rs_vSubCategory)
											{
												die('No Vehicle Type FOUUND.'.mysqli_error($obj->con));
											}
											while($row_vSubCategory = mysqli_fetch_array($rs_vSubCategory))
											{?>
												<li><a href="bookCar.php?vc_id=<?php echo $row_vSubCategory['vehicle_category_id']; ?>" title=""><?php echo $row_vSubCategory['vehicle_category']; ?></a></li>
											<?php
											}
											echo "</ul>";
											echo "</li>";
										}
									?>
										
                                    <li><a href="index.php" title="" >ABOUT</a>
									<li><a href="contactus.php" title="" >CONTACT</a>
									<li><a href="#" title=""><i class="fa fa-ellipsis-v"></i></a>
										<ul>
											<li><a href="register.php" title="">Register Page</a></li>
											<li><a href="how-this-work.php" title="">how it's work</a></li>
											<li><a href="faq.php" title="">faq's</a></li>
											<li><a href="points.php" title="">poinst</a></li>
											<li><a href="coming-soon.php" title="">Coming Soon</a></li>
											<li><a href="404.php" title="">404 Page</a></li>
										</ul>
									</li>
                                </ul>
                            </nav>
                            <div class="time-info">
								<a href="https://wa.me/919998896188" target="_blank"><i class="fa fa-whatsapp fa-2x"></i></a>
								<span >Whatsapp now <br>+91-9979274670</span>
							</div>
                        </header>
                    </div>
                </div>
            </div>
        </div><!-- topbar header -->
        
		<div class="responsive-header">
			<div class="res-top">
				<ul>
					<li><a href="#" title="Home"><i class="flaticon-home"></i></a></li>
					<li><a href="#" title="Faq's"><i class="flaticon-info"></i></a></li>
					<li><a href="#" title="Support center"><i class="flaticon-support"></i></a></li>
					<li><a href="#" title="Login"><i class="flaticon-unlocked"></i></a></li>
					<li><a href="#" title="New register"><i class="flaticon-checked"></i></a></li>
					<li class="post-new"><a href="#" title="New post">+Post</a></li>
				</ul>
			</div>
			<div class="logomenu-bar">
				<div class="logo"><a href="index.php" title=""><img src="images/logo-black.png" alt=""></a></div>
				<span class="responsviemenu-btn"><i class="flaticon-menu-1"></i></span> 
			</div>
			<div class="responsive-menu">
				<span class="close-btn"><i class="flaticon-error-1"></i></span>
				<ul>
					<li><a href="index.php" title="">Home</a></li>
					<?php 
						while($row_vType = mysqli_fetch_array($rs_vType))
						{?>
							<li class="menu-item-has-children"><a href="#" title=""><?php echo $row_vType['vehicle_type']; ?></a></li>
						<?php
						}
					?>
					<li class="menu-item-has-children"><a href="#" title="">cars</a>
						<ul class="sub-menu">
							<li><a href="index-after-login.php" title="">page after login</a></li>
							<li><a href="cars.php" title="">cars</a></li>
							<li><a href="car-profile.php" title="">car profile</a></li>
							<li><a href="car-detail.php" title="">cars detail</a></li>
							<li><a href="create-route-notification.php" title="">route notification</a></li>
							<li><a href="submit-car-intro.php" title="">Submit car introduction</a></li>
							<li><a href="submit-car-for-rent.php" title="">submit car for rent</a></li>
							<li><a href="submit-route.php" title="">submit car route</a></li>
						</ul>
					</li>
					
				</ul>
			</div>
		</div><!-- Responsive Header -->