<?<?php 
    include_once('header.php');
?>

<section>
        	<div class="featured-area blackish low-opacity">
            	<div class="top-area">
                	<img src="images/resources/top-banner.jpg" alt="">
                    <h2>How does this work?</h2>
                </div>
            </div>
        </section><!-- top banner  -->
        
        <section>
        	<div class="space">
        		<div class="container">
        			<div class="row">
        				<div class="col-md-12">
                        	<div class="step1">
                            	<span class="step-title">Step 1: Reserve</span>	
                                <div class="karaydar">
                                    <h2>Renter</h2>
                                    <div class="how-to">
                                        <div class="steps ext-topgap">
                                            <div class="step">
                                                <h4>Start “digging”</h4>
                                                <p>Look up for the perfect car and then fill up the form for rental. It’s that simple!</p>
                                                <span class="tag">Renter</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="car-malik">
                                    <h2>Owner</h2>
                                    <div class="how-to">
                                        <div class="steps">
                                            <div class="step">
                                                <h4>Rent your car</h4>
                                                <p>Set your price for as a car owner and start earning some money with Car East.</p>
                                                <span class="tag">Owner</span>
                                            </div>
                                        </div>
                                        <div class="steps">
                                            <div class="step">
                                                <h4>Accept the rental booking</h4>
                                                <p>Accept or deny the rental booking, up to you, but don’t forget to update the calendar.</p>
                                                <span class="tag">Owner</span>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <div class="step2">
                            	<span class="step-title">Step 2: Pickup</span>	
                                <div class="karaydar">
                                    <div class="how-to">
                                        <div class="steps ext-topgap">
                                            <div class="step">
                                                <h4>Have a great ride</h4>
                                                <p>Have a great ride with our 100km / day. In case you need more you can always ask for extra.</p>
                                                <span class="tag">Renter</span>
                                            </div>
                                        </div>
                                        <div class="steps no-topgap">
                                            <div class="step">
                                                <h4>Prepare the car</h4>
                                                <p>Be sure you return the car with the same level of fuel as you received it.</p>
                                                <span class="tag">Renter</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="car-malik">
                                    <div class="how-to">
                                        <div class="steps center">
                                            <div class="step">
                                                <h4>Meet up</h4>
                                                <p>Verify the driving license of the renter. Verify the car together, give the renter the car keys and fill out the contract.</p>
                                                <span class="tag">Owner & Renter</span>
                                            </div>
                                        </div>
                                        <div class="steps ext-topgap2">
                                            <div class="step">
                                                <h4>Enjoy the money</h4>
                                                <p>Relax yourself as your car is 100% secured with all risks included, in case anything happens.</p>
                                                <span class="tag">Owner</span>
                                            </div>
                                        </div> 
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="submit-btn">
                            	<a href="#" title="" class="theme-btn long">+ rent a car</a>
                                <a href="./vehicleOwner/vehicleOwnerSignup.php" title="" class="theme-btn long">+ upload your car</a>
                            </div>
                        </div>
        			</div>
        		</div>
        	</div>
        </section>
<?php
    include_once('footerTop.php');
    include_once('footer.php');
?>