-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 12, 2020 at 01:00 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rentalservice`
--

-- --------------------------------------------------------

--
-- Table structure for table `owner_master`
--

CREATE TABLE `owner_master` (
  `oid` bigint(20) NOT NULL,
  `mobileNumber` bigint(10) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `lname` int(50) NOT NULL,
  `street` int(100) NOT NULL,
  `pincode` bigint(20) NOT NULL,
  `doc` timestamp NOT NULL DEFAULT current_timestamp(),
  `adminSignature` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `city` varchar(30) NOT NULL,
  `state` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `owner_master`
--

INSERT INTO `owner_master` (`oid`, `mobileNumber`, `fname`, `lname`, `street`, `pincode`, `doc`, `adminSignature`, `email`, `city`, `state`) VALUES
(4, 0, '', 0, 0, 0, '2020-02-12 11:20:19', '0', 'admin', '', ''),
(5, 0, '', 0, 0, 0, '2020-02-12 11:47:40', 'admin', 'admin', '', ''),
(6, 0, '', 0, 0, 0, '2020-02-12 11:47:51', 'admin', 'admin', '', ''),
(7, 0, '', 0, 0, 0, '2020-02-12 11:49:17', 'admin', 'admin', '', 'aefadawd'),
(8, 0, '', 0, 0, 0, '2020-02-12 11:51:41', 'admin', 'admin@abc.com', '', 'Andhra Pradesh');

-- --------------------------------------------------------

--
-- Table structure for table `register_tbl`
--

CREATE TABLE `register_tbl` (
  `register_id` bigint(20) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile` bigint(10) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(25) NOT NULL,
  `profile` varchar(2000) NOT NULL,
  `added_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `register_tbl`
--

INSERT INTO `register_tbl` (`register_id`, `full_name`, `email`, `mobile`, `username`, `password`, `profile`, `added_date`) VALUES
(1, 'Keyur Macwana', 'keyurmacwana@gmail.com', 90, 'admin', '1234', '7034519889pexels-photo-2204532444976851.jpeg', '2020-01-13 05:37:39');

-- --------------------------------------------------------

--
-- Table structure for table `vehiclecatagorycar`
--

CREATE TABLE `vehiclecatagorycar` (
  `id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `rate` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vehiclecatagorycar`
--

INSERT INTO `vehiclecatagorycar` (`id`, `type`, `rate`) VALUES
(1, 'hatchback', 0),
(2, 'sedan', 0),
(3, 'SUV', 0),
(4, 'MUV', 0),
(5, 'convertibles', 0),
(6, 'station wagon', 0),
(7, 'coupe', 0),
(8, 'pick-up', 0);

-- --------------------------------------------------------

--
-- Table structure for table `vehiclecatagorymopad`
--

CREATE TABLE `vehiclecatagorymopad` (
  `id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `rate` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vehiclecatagorymopad`
--

INSERT INTO `vehiclecatagorymopad` (`id`, `type`, `rate`) VALUES
(1, 'economical', 0),
(2, 'cruiser', 0),
(3, 'sport', 0),
(4, 'touring', 0),
(5, 'sport touring', 0),
(6, 'dual purpose', 0),
(7, 'scooters', 0);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_category_tbl`
--

CREATE TABLE `vehicle_category_tbl` (
  `vehicle_category_id` bigint(20) NOT NULL,
  `vehicle_type_id` bigint(20) NOT NULL,
  `vehicle_category` varchar(100) NOT NULL,
  `rate` int(5) NOT NULL,
  `added_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_active` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vehicle_category_tbl`
--

INSERT INTO `vehicle_category_tbl` (`vehicle_category_id`, `vehicle_type_id`, `vehicle_category`, `rate`, `added_date`, `is_active`) VALUES
(23, 1, 'mopad', 0, '2020-01-28 06:16:00', 1),
(24, 1, 'bike', 5500, '2020-01-28 06:20:15', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_type_tbl`
--

CREATE TABLE `vehicle_type_tbl` (
  `vehicle_type_id` bigint(20) NOT NULL,
  `vehicle_type` varchar(100) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 0,
  `added_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `active_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vehicle_type_tbl`
--

INSERT INTO `vehicle_type_tbl` (`vehicle_type_id`, `vehicle_type`, `is_active`, `added_date`, `active_date`) VALUES
(1, 'TWO WHEELER', 1, '2020-01-25 07:48:33', '0000-00-00 00:00:00'),
(2, 'FOUR WHEELER', 1, '2020-01-25 08:09:48', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `owner_master`
--
ALTER TABLE `owner_master`
  ADD PRIMARY KEY (`oid`);

--
-- Indexes for table `register_tbl`
--
ALTER TABLE `register_tbl`
  ADD PRIMARY KEY (`register_id`);

--
-- Indexes for table `vehiclecatagorycar`
--
ALTER TABLE `vehiclecatagorycar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehiclecatagorymopad`
--
ALTER TABLE `vehiclecatagorymopad`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicle_category_tbl`
--
ALTER TABLE `vehicle_category_tbl`
  ADD PRIMARY KEY (`vehicle_category_id`),
  ADD KEY `vehicle_type_id` (`vehicle_type_id`);

--
-- Indexes for table `vehicle_type_tbl`
--
ALTER TABLE `vehicle_type_tbl`
  ADD PRIMARY KEY (`vehicle_type_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `owner_master`
--
ALTER TABLE `owner_master`
  MODIFY `oid` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `register_tbl`
--
ALTER TABLE `register_tbl`
  MODIFY `register_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vehiclecatagorycar`
--
ALTER TABLE `vehiclecatagorycar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `vehiclecatagorymopad`
--
ALTER TABLE `vehiclecatagorymopad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `vehicle_category_tbl`
--
ALTER TABLE `vehicle_category_tbl`
  MODIFY `vehicle_category_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `vehicle_type_tbl`
--
ALTER TABLE `vehicle_type_tbl`
  MODIFY `vehicle_type_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `vehicle_category_tbl`
--
ALTER TABLE `vehicle_category_tbl`
  ADD CONSTRAINT `vehicle_category_tbl_ibfk_1` FOREIGN KEY (`vehicle_type_id`) REFERENCES `vehicle_type_tbl` (`vehicle_type_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
