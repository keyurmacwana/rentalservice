<?php 
	include_once('header.php');
?>
<div class="sub-head blackish ext-low-opacity">
			<div class="bg-image" style="background-image: url(images/resources/subhead-bg.jpg)"></div>
        	<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="cola-head">
							<div class="head-meta">
								<h4><?php echo $data['fullName'];?></h4>
								
							</div>
							<div class="info-links">
								<a href="how-this-work.php" style="color:#FF0000;"title="">how does this work</a>
								<a href="../vehicleOwner/vehicleOwnerSignup.php" title="">submit your car</a>
								<span><i class="fa fa-phone"></i>call <?php echo $data['mobile'];?></span>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div><!-- subhead -->
        
        <section>
        	<div class="space">
        		<div class="container">
        			<div class="row">
                    	<div class="col-md-12">
                        	<div class="profile-edit">
                            	<div class="row">
                                	<div class="col-lg-3">
                                    	<div class="profile-dp">
                                        	<figure><img src="images/resources/addImage.png" alt=""></figure>
                                            <div class="profile-info">
												<span class="profile-name"><?php echo $data['fullName'];?></span>
												
											</div>
<!--                                            <ul class="verification">
                                            	<li><i class="flaticon-world"></i>Online on 4 Oct, 2017 at 08:36</li>
                                                <li><i class="flaticon-tick"></i>Account connected on Facebook</li>
                                                <li><i class="flaticon-tick"></i>Phone number verified</li>
                                            </ul>
-->
                                           
                                        </div>
                                    </div>
                                    <div class="col-lg-9 col-md-12">
                                    	<div class="edit-profile">
                                        	<form method="post" class="cola-form">
                                            	<div class="row">
                                                	<div class="col-md-8">
                                                    	<label>Full Name ( as on driving license):</label>
                                                		<input type="text" placeholder="<?php echo $data['fullName'];?>">
                                                    </div>
                                                    <div class="col-lg-4 col-md-12">
                                                    	<label>Date of birth:</label>
                                                        <div class="row">
                                                    	<div class="col-md-4">
                                                            <select>
                                                            	<option data-display="D">D</option>
                                                                <option>sun</option>
                                                                <option>mon</option>
                                                                <option>tue</option>
                                                                <option>wed</option>
                                                                <option>thir</option>
                                                                <option>fri</option>
                                                                <option>sat</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <select>
                                                                <option data-display="M">M</option>
                                                                <option>Jan</option>
                                                                <option>Feb</option>
                                                                <option>Mar</option>
                                                                <option>April</option>
                                                                <option>May</option>
                                                                <option>June</option>
                                                                <option>July</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <select  style="overflow:auto">
                                                                <option data-display="Y">Y</option>
																<option>	1983	</option>
																<option>	1984	</option>
																<option>	1985	</option>
																<option>	1986	</option>
																<option>	1987	</option>
																<option>	1988	</option>
																<option>	1989	</option>
																<option>	1990	</option>
																<option>	1991	</option>
																<option>	1992	</option>
																<option>	1993	</option>
																<option>	1994	</option>
																<option>	1995	</option>
																<option>	1996	</option>
																<option>	1997	</option>
																<option>	1998	</option>
																<option>	1999	</option>
																<option>	2000	</option>
																<option>	2001	</option>
																<option>	2002	</option>
																<option>	2003	</option>
																<option>	2004	</option>
																<option>	2005	</option>
																<option>	2006	</option>
																<option>	2007	</option>
																<option>	2008	</option>
																<option>	2009	</option>
																<option>	2010	</option>
																<option>	2011	</option>

                                                                <option>1980</option>
                                                                <option>1981</option>
                                                                <option>1982</option>
                                                                <option>1983</option>
                                                            </select>
                                                        </div>
                                                        </div>
                                                    </div>
												</div>
												<div class="row">
                                                   
                                                    <div class="col-md-8">
                                                    	<label>Street & number:</label>
                                                		<input type="text" placeholder="Eg:291,Model Town Apt.">
                                                    </div>
                                                    <div class="col-md-2">
                                                    	<label>city:</label>
                                                        <input type="text" placeholder="">
                                                    </div>
													<div class="col-md-2">
                                                    	<label>Postal Code:</label>
                                                        <input type="text" placeholder="">
                                                    </div>
												</div>
												<div class="row">
                                                    <div class="col-md-4">
                                                    	<label>Mobile: <a href="#" title="" class="update-nmbr">update</a></label>
                                                		<input type="text" placeholder="<?php echo $data['mobile'];?>" disabled>
                                                    </div>
                                                    <div class="col-md-4">
                                                    	<label>Email:<a href="#" title="" class="update-nmbr">update</a></label>
                                                		<input type="text" placeholder="<?php echo $data['email'];?>" disabled>
                                                    </div>
                                                    <div class="col-md-4">
                                                    	<label>Number driving license: <i class="fa fa-info-circle"></i></label>
                                                		<input type="text" placeholder="">
                                                    </div>
												</div>
												<div class="row">
                                                    <div class="col-md-4">
                                                    	<div class="dp-edit">
                                                        	<span>Upload Driving License:</span>
                                                            <img src="images/resources/drivingLicnseStock.png" alt="">
                                                             <input type="file">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-12">
                                                    	<label>driver's license issuance date:</label>
                                                        <div class="row">
                                                    	<div class="col-md-4">
                                                            <select>
                                                            	<option data-display="D">D</option>
                                                                <option>sun</option>
                                                                <option>mon</option>
                                                                <option>tue</option>
                                                                <option>wed</option>
                                                                <option>thir</option>
                                                                <option>fri</option>
                                                                <option>sat</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <select>
                                                                <option data-display="M">M</option>
                                                                <option>Jan</option>
                                                                <option>Feb</option>
                                                                <option>Mar</option>
                                                                <option>April</option>
                                                                <option>May</option>
                                                                <option>June</option>
                                                                <option>July</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <select>
                                                                <option data-display="Y">Y</option>
                                                                <option>1980</option>
                                                                <option>1981</option>
                                                                <option>1982</option>
                                                                <option>1983</option>
                                                            </select>
                                                        </div>
                                                        </div>
                                                    </div>
													 
                                                    <div class="col-lg-4 col-md-12">
                                                    	<label>driver's license Expiry date</label>
                                                        <div class="row">
                                                    	<div class="col-md-4">
                                                            <select>
                                                            	<option data-display="D">D</option>
                                                                <option>sun</option>
                                                                <option>mon</option>
                                                                <option>tue</option>
                                                                <option>wed</option>
                                                                <option>thir</option>
                                                                <option>fri</option>
                                                                <option>sat</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <select>
                                                                <option data-display="M">M</option>
                                                                <option>Jan</option>
                                                                <option>Feb</option>
                                                                <option>Mar</option>
                                                                <option>April</option>
                                                                <option>May</option>
                                                                <option>June</option>
                                                                <option>July</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <select>
                                                                <option data-display="Y">Y</option>
                                                                <option>1980</option>
                                                                <option>1981</option>
                                                                <option>1982</option>
                                                                <option>1983</option>
                                                            </select>
                                                        </div>
                                                        </div>
                                                    </div>
													<div class="col-md-4">
                                                    		<div class="notification-rec">
                                                        	<span>Vehicle Prefrence:</span>
                                                            
                                                            	<p>
                                                                    <input type="checkbox">
                                                                    <label></label>
                                                                    2 Wheeler
                                                                </p>
                                                                <p>
                                                                    <input type="checkbox">
                                                                    <label></label>
                                                                	4 Wheeler
                                                                </p>
                                                                
                                                        </div>
                                                    </div>
												</div>
                                                <div class="row">   
                                                    <button type="submit" class="theme-btn long">save change</button>
                                                </div>
                                            </form>
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
        		</div>
        	</div>
        </section><!-- points section -->
<?php
	include_once('footer.php');
?>