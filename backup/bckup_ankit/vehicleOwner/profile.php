<?php
	include_once('header.php');
?>
<!-- Main Body content starts here -->
        <div id="wrapper">
            <!-- Sidebar -->
            <div id="sidebar-wrapper">
                <aside class="sidebar">
                    <nav class="sidebar-nav" id="sidebarscroll">
                        <ul class="metismenu ripple" id="menu">
						
								<li  >
									<a href="dashboard.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-folder-open fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Dashboard</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li >
									<a href="ownerWallet.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-money fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Wallet</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li >
									<a href="ownerFAQ.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-tachometer fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">On Going Rides</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li >
									<a href="ownerFAQ.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-clock-o  fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Rides History</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li >
									<a href="registerVehicle.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-plus fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Add Vehicles</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								<li >
									<a href="vehicleOffDay.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-calendar fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Reserve Vehicle</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								<li >
									<a href="termsAndConditions.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-legal notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Terms & Conditions</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li >
									<a href="ownerFAQ.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-question-circle fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">FAQ</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
                            
                        </ul>
                    </nav>
                </aside>
            </div>
            <!-- # Sidebar-wrapper -->
            
			<!-- Page Content-wrapper -->
			<div id="page-content-wrapper">

<?php
	clearstatcache();
	
?>
<?php
//	include_once('dbclass.php');
//	$ss = ;
	//echo $ss;
	$sql = "SELECT * FROM owner_tbl where userName = '".$_SESSION['userName']."'";
//	echo "<h1>".$sql."</h1>";
	$obj = new dbclass();
	$rs = mysqli_query($obj->con,$sql);
	//echo $rs;
	$row = mysqli_fetch_array($rs);
?>

<!-- Breacrumb -->
				<div class="row csk-breadcrumb">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
						<h4 class="page-title">Profile</h4>
					</div>
					<div class="col-lg-9 col-md-8 col-sm-8 hidden-xs">
						<ol class="breadcrumb">
							<li><a href="dashboard.php">Dashboard</a></li>
							<li><a href="#">Profile</a></li>
						</ol>
					</div>
				</div>
				<!-- # Breadcrumb -->
				<!-- Profile -->
				<div class="profile">
					<div class="row">
						<div class="col-md-12 profile">
							<div class="cover-footer">
								<div class="google-panel">
									<div class="panel panel-danger contacts">
										<div class="panel-heading">
											<div class="cover-picture"></div>
											<div class="profile-line">
												<div class="profile-picture">
													<?php echo "<img src='../vehicleOwnerProfile/".$data['profile']."' class='img-responsive' alt='avatar' width='100px' height='50px'>" ;?>
												</div>
												<h3><?php echo $_SESSION['userName'];?></h3>
											</div>
											
										</div>
										<div class="panel-tabs">
											<ul class="nav nav-tabs">
												<li class="active"><a href="#tab1default" data-toggle="tab">Settings</a></li>
												<li><a href="#tab2default" data-toggle="tab">Change Password</a></li>
											</ul>
										</div>
										<div class="panel-body">
											<div class="tab-content">
												<div class="tab-pane fade in active" id="tab1default">
													<main id="home" class="contents">
													<div class="col-md-6">
														<?php echo $_SESSION['msg'];$_SESSION['msg'] = ""?>
														<form action="dbclass.php" method="post" enctype="multipart/form-data">
														<input type="hidden" id="vehicleOwnerId" name="vehicleOwnerId" class="form-control" value="<?php echo $row['vehicleOwnerId']; ?>" >
														
																<div class="form-group">
																	<label class="control-label">Change image</label>
																	<input type="file" id="imgUpload" name="imgUpload" class="form-control"   accept="image/*">
																</div>

																<div class="form-group">
																	<label class="control-label">User Name</label>
																	<input type="text"  class="form-control" value="<?php echo $row['userName']; ?>" disabled="disabled">
																	
																</div>
																<div class="form-group">
																	<label class="control-label">First Name</label>
																	<input type="text" id="firstName" name="firstName" class="form-control" value="<?php echo $row['firstName'] ?>">
																</div>
																<div class="form-group">
																	<label class="control-label">Street</label>
																	<input type="text" id="street" name="street" class="form-control" value="<?php echo $row['street'] ?>">
																</div>
																<div class="form-group">
																	<label class="control-label">State</label>
																	<input type="text" id="state" name="state" class="form-control" value="<?php echo $row['state'] ?>">
																</div>
																
													</div>
													<div class="col-md-6">
																<div class="form-group">
																	<label class="control-label">Email </label>
																	<input type="email" id="email" name="email" class="form-control" value="<?php echo $row['email'] ?>">
																</div>
																<div class="form-group">
																	<label class="control-label">Mobile</label>
																	<input type="text" id="mobileNumber" name="mobileNumber" class="form-control" value="<?php echo $row['mobileNumber'] ?>">
																</div>
																<div class="form-group">
																	<label class="control-label">Last Name</label>
																	<input type="text" id="lastName" name="lastName" class="form-control" value="<?php echo $row['lastName'] ?>">
																</div>
																<div class="form-group">
																	<label class="control-label">City</label>
																	<input type="text" id="city" name="city" class="form-control" value="<?php echo $row['city'] ?>">
																</div>
																<div class="form-group">
																	<label class="control-label">pincode</label>
																	<input type="text" id="pincode" name="pincode" class="form-control" value="<?php echo $row['pincode'] ?>">
																</div>
																
																<hr>
													</div>
													
													<div class="col-md-12">
														<div>
																<button type="submit" id="updateVehicleOwner" name="updateVehicleOwner" class="btn btn-success">Save Changes</button>
														</div>

														</form>
													</div>
													</main>
												</div>
												<div class="tab-pane fade" id="tab2default">
													<div class="row">
														<div class="col-md-6">
															<form action="dbclass.php" method="post">
																				<div class="form-group">
																					<label class="control-label">Enter Password</label>
																					<input type="password" id="pwd1" name="pwd1" class="form-control">
																				</div>
																				<div class="form-group">
																					<label class="control-label">ReEnter Password</label>
																					<input type="password" id="pwd2" name="pwd2" class="form-control">
																				</div>
																				<div class="form-group">
																					<button type="changePwd" id="changePwd" name="changePwd" class="btn btn-success">Change Password</button>
																				</div>
																				<input type="hidden" name="userName" id="userName" <?php echo "value='".$_SESSION['userName']."'";?> >
															</form>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

<?php
	include_once('footer.php');
?>