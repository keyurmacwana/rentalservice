<?php
	include_once('header.php');

//fetching owner id

	$sql_owner_id = "SELECT * FROM  owner_tbl WHERE userName='".$_SESSION['userName']."'";		
	$ans_owner_id = mysqli_query($obj->con,$sql_owner_id);
	$row_owner_id = mysqli_fetch_array($ans_owner_id);
	$vehicleOwnerId = $row_owner_id['vehicleOwnerId'];
	
//END fetching owner id	
		
//To change status of vehicle
	if(isset($_GET['stat']))
	{
		$obj -> changeVehicleStatus($_GET['stat'],$_GET['vehicleId']);
	}
//END change status of vehicle

//To Delete Vehicle
	if(isset($_GET['d']))
	{
		$obj -> deleteVehicle($_GET['d']);
	}
//END Delete vehicle

//TO edit data
	if(isset($_GET['vid']))
	{
		//value="<?php if(isset($_GET['vid'])){echo $row_vehicle_edit['companyName'];"
		$sql_vehicle_edit = "SELECT * FROM  vehicle_tbl WHERE vehicleId='".$_GET['vid']."'";
				
		$ans_vehicle_edit = mysqli_query($obj->con,$sql_vehicle_edit);
		$row_vehicle_edit = mysqli_fetch_array($ans_vehicle_edit);
		
	}
//END edit data
?>
<!-- Main Body content starts here -->
        <div id="wrapper">
            <!-- Sidebar -->
            <div id="sidebar-wrapper">
                <aside class="sidebar">
                    <nav class="sidebar-nav" id="sidebarscroll">
                        <ul class="metismenu ripple" id="menu">
						
								<li  >
									<a href="dashboard.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-folder-open fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Dashboard</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li >
									<a href="ownerWallet.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-money fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Wallet</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li >
									<a href="ownerFAQ.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-tachometer fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">On Going Rides</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li >
									<a href="ownerFAQ.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-clock-o  fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Rides History</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li style="background-color:#505464;">
									<a href="registerVehicle.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-plus fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Add Vehicles</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								<li >
									<a href="vehicleOffDay.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-calendar fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Reserve Vehicle</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								<li >
									<a href="termsAndConditions.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-legal notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Terms & Conditions</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li >
									<a href="ownerFAQ.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-question-circle fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">FAQ</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
	
								
                            
                        </ul>
                    </nav>
                </aside>
            </div>
            <!-- # Sidebar-wrapper -->
            
			<!-- Page Content-wrapper -->
			<div id="page-content-wrapper">

			
				<!-- Breadcrumb  -->
				<div class="row csk-breadcrumb">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
						<h4 class="page-title">Register Vehicle Owner</h4>
					</div>
					<div class="col-lg-9 col-md-8 col-sm-8 hidden-xs">
						<ol class="breadcrumb">
							<li><a href="dashboard.php">Dashboard</a></li>
							<li><a href="#">Register Vehicle Owner</a></li>
						</ol>
					</div>
				</div>
				<!-- #Breadcrumb -->
				<!-- row -->
				
				<!-- row -->
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-defaut">
								<div class="panel-heading">
									<h3 class="panel-title">Register Vehicle Owner</h3>
								</div>
								<div class="panel-body">
									<form action="dbclass.php" method="post" enctype="multipart/form-data">
										
										<?php 
											if(isset($_GET['vid']))
											{ 
												echo "<input type='hidden' name='vehicleId' id='vehicleId' value='".$row_vehicle_edit['vehicleId']."'>";
											} 
										?>
										<div class="form-body">
											
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label">Vehicle Type</label>
														<select name="vehicle_type_id" id="vehicle_type_id" class="form-control">
															<option value="0">---Select one---</option>
															<?php
																//To fetch vehicle type	
																	$sql_vehicle_type = "SELECT * FROM  vehicle_type_tbl WHERE delete_status = 0";		
																	$ans_vehicle_type = mysqli_query($obj->con,$sql_vehicle_type);
																//END fetch vehicle type
																while($row_vehicle_type = mysqli_fetch_array($ans_vehicle_type))
																{
															?>
															<option value="<?php echo $row_vehicle_type['vehicle_type_id']; ?>" <?php if(isset($_GET['vid'])){if($row_vehicle_edit['vehicle_type_id'] == $row_vehicle_type['vehicle_type_id']){echo "selected";}}?> >
															 <?php echo $row_vehicle_type['vehicle_type']; ?>
															</option>
															<?php
																}
															?>
															
														</select>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label">Vehicle Category</label>
														
														<select name="vehicle_category_id" id="vehicle_category_id" class="form-control">
															<option value="0">---Select one---</option>
															<?php
																
																	
																//To fetch vehicle category	
																	$sql_vehicle_category = "SELECT * FROM vehicle_category_tbl WHERE delete_status = 0";		
																	$ans_vehicle_category = mysqli_query($obj->con,$sql_vehicle_category);
																//END fetch vehicle category
																while($row_vehicle_category = mysqli_fetch_array($ans_vehicle_category))
																{
															?>
															<option value="<?php echo $row_vehicle_category['vehicle_category_id']; ?>" <?php if(isset($_GET['vid'])){if($row_vehicle_edit['vehicle_category_id'] == $row_vehicle_category['vehicle_category_id']){echo "selected";}}?>>
															<?php echo $row_vehicle_category['vehicle_category']; ?>
															</option>
															<?php
																}
															?>
															
														</select>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label">Company</label>
														<input type="text" id="companyName" name="companyName" class="form-control"  value="<?php if(isset($_GET['vid'])){echo $row_vehicle_edit['companyName'];}?>" >
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label">Model</label>
														<input type="text" id="modelName" name="modelName" class="form-control"  value="<?php if(isset($_GET['vid'])){echo $row_vehicle_edit['modelName'];}?>" >
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label">Model Number</label>
														<input type="text" id="modelNumber" name="modelNumber" class="form-control"  value="<?php if(isset($_GET['vid'])){echo $row_vehicle_edit['modelNumber'];}?>"  >
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label">License plate number</label>
														<input type="text" id="licenseNumberPlate" name="licenseNumberPlate" class="form-control"  value="<?php if(isset($_GET['vid'])){echo $row_vehicle_edit['licenseNumberPlate'];}?>" >
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<label class="control-label">Street</label>
														<input type="text" id="street" name="street" class="form-control" value="<?php if(isset($_GET['vid'])){echo $row_vehicle_edit['street'];}?>" >
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-group">
														<label class="control-label">City</label>
														<input type="text" id="city" name="city" class="form-control" value="<?php if(isset($_GET['vid'])){echo $row_vehicle_edit['city'];}?>"  >
													</div>
												</div>
												<div class="col-md-3">
													<div class="form-group">
														<label class="control-label">State</label>
														<input type="text" id="state" name="state" class="form-control" value="<?php if(isset($_GET['vid'])){echo $row_vehicle_edit['state'];}?>"  >
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-3">
													<div class="form-group">
														<label class="control-label">WeekDay Start Time</label>
														<input type="time" id="weekdayStart" name="weekdayStart" value="<?php if(isset($_GET['vid'])){echo $row_vehicle_edit['weekdayStart'];}?>">
													</div>
													
												</div>
												<div class="col-md-3">
													<div class="form-group">
														<label class="control-label">WeekDay End Time</label>
														<input type="time" id="weekdayEnd" name="weekdayEnd" value="<?php if(isset($_GET['vid'])){echo $row_vehicle_edit['weekdayEnd'];}?>">
													</div>
													
												</div>
												
												<div class="col-md-3">
													<div class="form-group">
														<label class="control-label">WeekEnd Start Time</label>
														<input type="time" id="weekendStart" name="weekendStart" value="<?php if(isset($_GET['vid'])){echo $row_vehicle_edit['weekendStart'];}?>">
													</div>
													
												</div>
												<div class="col-md-3">
													<div class="form-group">
														<label class="control-label">WeekEnd End Time</label>
														<input type="time" id="weekendEnd" name="weekendEnd" value="<?php if(isset($_GET['vid'])){echo $row_vehicle_edit['weekendEnd'];}?>">
													</div>
													
												</div>
												
											</div>
											<div class="row">
												<div class="col-md-6">
													<label class="control-label">Insert GOOGLE LOCATION HERE</label>
													<input type="text">
												</div>
											</div>
											
											<div class="row">
												<div class="col-md-2">
									<div class="row">
									<label class="control-label">Upload Vehicle image #1</label>
                                   
                                       <img src="./datas/images/plus.png" id="realimage1" height="100px" width="100px" onclick="triggerclick()">
										
                                        <input type="file" name="imgUpload1" id="imgUpload1" onchange="displayimage(this)" style="display: none">
                                    </div>
												</div>
												<div class="col-md-2">
													<div class="row">
									<label class="control-label">Upload Vehicle image #2</label>
                                   
                                         <img 
										src="./datas/images/plus.png" id="realimage2" height="100px" width="100px" onclick="triggerclick2()">
										
										
                                        <input type="file" name="imgUpload2" id="imgUpload2" onchange="displayimage2(this)" style="display: none">
                                    </div>
												</div>
												<div class="col-md-2">
													<div class="row">
									<label class="control-label">Upload Vehicle image #3</label>
                                   
                                        <img 
										src="./datas/images/plus.png" id="realimage3" height="100px" width="100px" onclick="triggerclick3()">
										
                                        <input type="file" name="imgUpload3" id="imgUpload3" onchange="displayimage3(this)" style="display: none">
                                    </div>
												</div>
													
												
											</div>
											<div class="row">
												<div class="col-md-2">
													<div class="form-group">
														<label class="control-label">Upload RC Book</label>
														<img src="./datas/images/plus.png" id="realimage4" height="100px" width="100px" onclick="triggerclick4()">
														<input type="file" id="rcBook_link4" name="rcBook_link" class="form-control"   accept="image/*" onchange="displayimage4(this)" style="display: none">
													</div>
												</div>
												<div class="col-md-2">
													<div class="form-group">
														<label class="control-label">Upload Insurance</label>
														<img src="./datas/images/plus.png" id="realimage5" height="100px" width="100px" onclick="triggerclick5()">
														<input type="file" id="rcBook_link5" name="rcBook_link" class="form-control"   accept="image/*" onchange="displayimage5(this)" style="display: none">
													</div>
												</div>
												<div class="col-md-2">
													<div class="form-group">
														<label class="control-label">Upload PUC</label>
														<img src="./datas/images/plus.png" id="realimage6" height="100px" width="100px" onclick="triggerclick6()">
														<input type="file" id="rcBook_link6" name="rcBook_link" class="form-control"   accept="image/*" onchange="displayimage6(this)" style="display: none">
													</div>
												</div>
											</div>
										</div>
										
									
										
										<div class="form-actions">
											<button type="submit" class="btn btn-success" name="vehicleRegister" id="vehicleRegister"> <i class="fa fa-check"></i> Save</button>
											<button type="button" class="btn btn-danger">Cancel</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				<!-- #row -->
				<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default panel-with-options">
			
				<h3 >&emsp;Registered Vehicle Owner List</h3>
			
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered datatable">
						<thead>
							<tr>
								<th>SR NO.</th>
								<th>Status</th>
								<th>Type-Category</th>
								<th>Model</th>
								<th>License Plate</th>
								<th>Address</th>
								<th>Week Day</th>
								<th>Week End</th>
								<th>Date Added</th>
								<th>Action</th>
							</tr>
						</thead>
						<tfoot>
						<tr>
								<th>SR NO.</th>
								<th>Status</th>
								<th>Type-Category</th>
								<th>Model</th>
								<th>License Plate</th>
								<th>Address</th>
								<th>Week Day</th>
								<th>Week End</th>
								<th>Date Added</th>
								<th>Action</th>
							</tr>
						</tfoot>
						<tbody>
							
							<?php 
								//To fetch table data
									$sql_vehicle_tbl = "SELECT vt.*,vct.vehicle_category,vtt.vehicle_type 
														FROM  vehicle_tbl vt 
														LEFT JOIN vehicle_type_tbl vtt 
														ON vt.vehicle_type_id = vtt.vehicle_type_id 
														LEFT JOIN vehicle_category_tbl vct 
														ON vt.vehicle_category_id = vct.vehicle_category_id 
														WHERE vt.owner_id = '$vehicleOwnerId'";	
									
									$ans_vehicle_tbl = mysqli_query($obj->con,$sql_vehicle_tbl);
								//END fetch table data

								$counter = 1;
								while($row_vehicle_tbl = mysqli_fetch_array($ans_vehicle_tbl))
								{
							?>
								<tr>
									<td><?php echo $counter ++; ?></td>
									<td>
									<?php 
									if($row_vehicle_tbl['is_active'] == 1)
									{?>
										<span class="label label-success"><a href="registerVehicle.php?stat=1&vehicleId=<?php echo $row_vehicle_tbl['vehicleId'];?>" style="color:white;">Active</a></span>
									<?php
									}
									else
									{?>
										<span class="label label-danger"><a href="registerVehicle.php?stat=0&vehicleId=<?php echo $row_vehicle_tbl['vehicleId'];?>" style="color:white;">De-Active</a></span>
									<?php
									}
									?>
									</td>
									<td><?php echo $row_vehicle_tbl['vehicle_type']."-".$row_vehicle_tbl['vehicle_category'];?></td>
							
									
									<td><?php echo $row_vehicle_tbl['companyName'].",".$row_vehicle_tbl['modelName'].",".$row_vehicle_tbl['modelNumber'] ?></td>
									
									<td><?php echo $row_vehicle_tbl['licenseNumberPlate'] ?></td>
									<td><?php echo $row_vehicle_tbl['street'].",".$row_vehicle_tbl['state'] ?></td>
									<td><?php echo $row_vehicle_tbl['weekdayStart']." to ".$row_vehicle_tbl['weekdayEnd'] ?></td>
									<td><?php echo $row_vehicle_tbl['weekendStart']." to ".$row_vehicle_tbl['weekendEnd'] ?></td>
									<td><?php echo date("d-m-Y", strtotime($row_vehicle_tbl['dateCreated'])); ?></td>
									<td>
									<a href="registerVehicle.php?vid=<?php echo $row_vehicle_tbl['vehicleId']; ?>" title="EDIT"><i class="fa fa-pencil fa-1x "></i></a> || 
									<a href="registerVehicle.php?d=<?php echo $row_vehicle_tbl['vehicleId']; ?>" title="DELETE"><i class="fa fa-trash fa-1x"></i></a>
									</td>
								</tr>
							<?php
								}
							?>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
	 <script type="text/javascript">
	 
        function triggerclick()
		{
            document.querySelector('#imgUpload1').click();
        }
        function displayimage(e)
		{
            if(e.files[0])
			{
                var reader = new FileReader();
                reader.onload = function(e)
				{
                    document.querySelector('#realimage1').setAttribute('src', e.target.result);
                }
                reader.readAsDataURL(e.files[0]);
            }
        }
		
		 function triggerclick2()
		{
            document.querySelector('#imgUpload2').click();
        }
        function displayimage2(e)
		{
            if(e.files[0])
			{
                var reader = new FileReader();
                reader.onload = function(e)
				{
                    document.querySelector('#realimage2').setAttribute('src', e.target.result);
                }
                reader.readAsDataURL(e.files[0]);
            }
        }
		
		
		function triggerclick3()
		{
            document.querySelector('#imgUpload3').click();
        }
        function displayimage3(e)
		{
            if(e.files[0])
			{
                var reader = new FileReader();
                reader.onload = function(e)
				{
                    document.querySelector('#realimage3').setAttribute('src', e.target.result);
                }
                reader.readAsDataURL(e.files[0]);
            }
        }


        function triggerclick4()
		{
            document.querySelector('#rcBook_link4').click();
        }
        function displayimage4(e)
		{
            if(e.files[0])
			{
                var reader = new FileReader();
                reader.onload = function(e)
				{
                    document.querySelector('#realimage4').setAttribute('src', e.target.result);
                }
                reader.readAsDataURL(e.files[0]);
            }
        }



        function triggerclick5()
		{
            document.querySelector('#rcBook_link5').click();
        }
        function displayimage5(e)
		{
            if(e.files[0])
			{
                var reader = new FileReader();
                reader.onload = function(e)
				{
                    document.querySelector('#realimage5').setAttribute('src', e.target.result);
                }
                reader.readAsDataURL(e.files[0]);
            }
        }


        function triggerclick6()
		{
            document.querySelector('#rcBook_link6').click();
        }
        function displayimage6(e)
		{
            if(e.files[0])
			{
                var reader = new FileReader();
                reader.onload = function(e)
				{
                    document.querySelector('#realimage6').setAttribute('src', e.target.result);
                }
                reader.readAsDataURL(e.files[0]);
            }
        }
		
		
        </script>
		 <script type="text/javascript">
		 
		 
       

        <script type="text/javascript">
        function triggerclickmulti(){
            document.querySelector('#gallery-photo-add').click();
        }
        </script>
        <script type="text/javascript">
            function  change_subcategory(){
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.open("GET","ajax.php?subcat=" + document.getElementById("category").value,false);
                xmlhttp.send(null);
                document.getElementById("psize").innerHTML=xmlhttp.responseText;
            }
        </script>
	

<?php
	include_once('footer.php');
?>