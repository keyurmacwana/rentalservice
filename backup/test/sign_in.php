<?php setcookie("caid","null"); ?>
<!DOCTYPE html>
<html lang="en">
	
<!-- Mirrored from cskadmin.com/v3.2/sign-in.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 18 Mar 2017 13:38:04 GMT -->
<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<title>Sign In</title>
		<!-- Icon -->
		<link rel="icon" href="datas/images/icon.ico">
		<!-- Bootstrap Core CSS -->
		<!-- <link href="datas/bootstrap/css/bootstrap.min.css" rel="stylesheet"> -->
		<!-- Animate CSS -->
		<link rel="stylesheet" href="datas/assets/animate.css-master/animate.min.css">
		<!-- Theme  CSS -->
		<link rel="stylesheet" type="text/css" href="datas/min/csk.min.css">
		<!-- MediaQuery CSS -->
		<link rel="stylesheet" type="text/css" href="datas/css/media-query.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

<style>
body {font-family: Arial, Helvetica, sans-serif;}

/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
  background-color: #fefefe;
  margin: auto;
  padding: 20px;
  border: 1px solid #888;
  width: 80%;
}

/* The Close Button */
.close {
  color: #aaaaaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}
</style>
	</head>
	<body>
		<!--Container Starts -->
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-md-push-4 col-xs-12 sign-box">
					<div class="panel panel-default pale-panel-border-top">
						<div class="panel-heading text-center">
							<h3 class="nm np">
							Sign In
							</h3>
							<font color="red"><?php if(isset($_GET['msg'])){ echo "id or Password incorrect."; }?></font>
						</div>
			<div class="panel-body">
				<br>
				<form class="form" action="dbclass.php" method="post">
					<input id="UserName" name="UserName" type="text" data-length="5,50" placeholder="Mobile number or email" required="required" />

					<input id="UserPassword" name="UserPassword" type="password" required="required" placeholder="Password" />
					<div class="input-group">
						<div class="input-group-prepend">
							<div class="input-group-text" style=" width: 50px; ">
							 <input id="rememberme" name="rememberme" type="checkbox" value="1"/>
							</div> 
						</div>
							<div class="input-group-append">
								<span class="form-control" id="basic-addon2">Remember Me</span>
							</div>
					</div>
					</div>
					<button type="submit" name="BtnLogin" id="BtnLogin" class="center btn btn-success btn-lg btn-block" data-dismiss="modal">
					<i class="fa fa-lock"></i> Secure Login
					</button>
				</form>

					<button id="myBtn" class="center btn btn-danger btn-sm btn-block" data-dismiss="modal">Forgot Password?</button>
			</div>
					</div>
				</div>
			</div>
		</div>
		<div id="myModal" class="modal">

  <!-- Modal content -->
  	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-push-4 col-xs-12 sign-box">
				<div class="panel panel-default pale-panel-border-top">
					<div class="panel-heading text-center">
						<span class="close">&times;</span>
						<form action="dbclass.php" method="post">
							<P>Enter your email</P>
							<input type="email" name="TextForgotPass" id="TextForgotPass">
							<input type="submit" name="BtnForgotPass" id="BtnForgotPass" class="center btn btn-success" value="Send link">
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
		<!-- #Container Ends -->
		<!-- GoogleApi jQuery -->
		<!-- <script type="text/javascript" src="datas/assets/jquery-1.12.4/jquery-1.12.4.js"></script> -->
		<!-- Bootstrap Core JavaScript -->
		<!-- <script src="datas/bootstrap/js/bootstrap.min.js"></script> -->
		<!-- <script type="text/javascript" src="datas/js/sisur.js"></script -->
		
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<script>
// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
  modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>

	</body>

<!-- Mirrored from cskadmin.com/v3.2/sign-in.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 18 Mar 2017 13:38:04 GMT -->
</html>