<?<?php 
    include_once('header.php');
?>

<div class="sub-head blackish ext-low-opacity">
			<div class="bg-image" style="background-image: url(images/resources/subhead-bg.jpg)"></div>
        	<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="cola-head">
							<div class="head-meta">
								<h4>Questions? Look here.</h4>
							</div>
							<p>( Can’t find an answer? Call us at (012) 345 - 6789 or email us at <a href="http://wpkixx.com/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="6e0d01001a0f0d1a2e0d0f1c0d0f1c1c17400d0103">[email&#160;protected]</a> )</p>
							<div class="info-links">
								<a href="how-this-work.php" title="" style="color:#FF0000;">how does this work</a>
								<a href="../vehicleOwner/vehicleOwnerSignup.php" title="" style="color:white;">submit your car</a>
								<span><i class="fa fa-phone"></i>call (012) 345 - 6789</span>
							</div>
							
							
						</div>
					</div>
				</div>
			</div>
        </div><!-- subhead -->
                
        <section>
        	<div class="space">
        		<div class="container">
        			<div class="row">
        				<div class="col-md-12">
                        	<div class="faq-sec">
                            	<div class="row">
                                	<div class="col-lg-3 col-md-3 col-sm-4">
                                    	<aside class="faq-meta">
                                        	<h4>table of contents</h4>
                                            <ul class="table-list">
                                            	<li><a href="#" title="">general</a></li>
                                                <li><a href="#" title="">trust & safety</a></li>
                                                <li><a href="#" title="">services</a></li>
                                                <li><a href="#" title="">billing</a></li>
                                                <li><a href="#" title="">account</a></li>
                                                <li><a href="#" title="">password</a></li>
                                            </ul>
                                        </aside>
                                    </div>
                                    <div class="col-lg-6 col-sm-8">
                                        <div class="tab-sec">
                                            <div class="dc-toggle">
                                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading" role="tab" id="headingOne">
                                                            <h4 class="panel-title">
                                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                                	<span class="toogle">How do I link to a file or folder?</span>
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse in show" role="tabpanel" aria-labelledby="headingOne">
                                                            <div class="panel-body">
                                                                <p>You're most likely experiencing a syncing issue. Fixes for sync issues can range from easy to complex, but any solution will require you to take some action. This step-by-step article will walk you through troubleshooting steps, and will solve the majority of syncing problems.
</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading" role="tab" id="headingtwo">
                                                            <h4 class="panel-title">
                                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsetwo" aria-expanded="false" aria-controls="collapseOne">
                                                                	<span class="toogle">I deleted a shared folder—will that affect other members of it?</span>
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapsetwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtwo">
                                                            <div class="panel-body">
                                                                <p>You're most likely experiencing a syncing issue. Fixes for sync issues can range from easy to complex, but any solution will require you to take some action. This step-by-step article will walk you through troubleshooting steps, and will solve the majority of syncing problems.
</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading" role="tab" id="headingthree">
                                                            <h4 class="panel-title">
                                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsethree" aria-expanded="false" aria-controls="collapseOne">
                                                                	<span class="toogle">Learn how to troubleshoot and resolve Selective Sync?</span>
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapsethree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingthree">
                                                            <div class="panel-body">
                                                                <p>You're most likely experiencing a syncing issue. Fixes for sync issues can range from easy to complex, but any solution will require you to take some action. This step-by-step article will walk you through troubleshooting steps, and will solve the majority of syncing problems.
</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading" role="tab" id="headingfour">
                                                            <h4 class="panel-title">
                                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="false" aria-controls="collapseOne">
                                                                	<span class="toogle">Adjust billing information?</span>
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapsefour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfour">
                                                            <div class="panel-body">
                                                                <p>You're most likely experiencing a syncing issue. Fixes for sync issues can range from easy to complex, but any solution will require you to take some action. This step-by-step article will walk you through troubleshooting steps, and will solve the majority of syncing problems.
</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading" role="tab" id="headingfive">
                                                            <h4 class="panel-title">
                                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefive" aria-expanded="false" aria-controls="collapseOne">
                                                                	<span class="toogle">You changed the script inside your account?</span>
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapsefive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfive">
                                                            <div class="panel-body">
                                                                <p>You're most likely experiencing a syncing issue. Fixes for sync issues can range from easy to complex, but any solution will require you to take some action. This step-by-step article will walk you through troubleshooting steps, and will solve the majority of syncing problems.
</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading" role="tab" id="headingsix">
                                                            <h4 class="panel-title">
                                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsesix" aria-expanded="false" aria-controls="collapseOne">
                                                                	<span class="toogle">How secured are the payments with Car East?</span>
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapsesix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingsix">
                                                            <div class="panel-body">
                                                                <p>You're most likely experiencing a syncing issue. Fixes for sync issues can range from easy to complex, but any solution will require you to take some action. This step-by-step article will walk you through troubleshooting steps, and will solve the majority of syncing problems.
</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading" role="tab" id="headingseven">
                                                            <h4 class="panel-title">
                                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseseven" aria-expanded="false" aria-controls="collapseOne">
                                                                	<span class="toogle">How do I pay for the rides?</span>
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseseven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingseven">
                                                            <div class="panel-body">
                                                                <p>You're most likely experiencing a syncing issue. Fixes for sync issues can range from easy to complex, but any solution will require you to take some action. This step-by-step article will walk you through troubleshooting steps, and will solve the majority of syncing problems.
</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading" role="tab" id="headingeight">
                                                            <h4 class="panel-title">
                                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseeight" aria-expanded="false" aria-controls="collapseOne">
                                                                	<span class="toogle">Can I pay for a subset of team members?</span>
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseeight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingeight">
                                                            <div class="panel-body">
                                                                <p>You're most likely experiencing a syncing issue. Fixes for sync issues can range from easy to complex, but any solution will require you to take some action. This step-by-step article will walk you through troubleshooting steps, and will solve the majority of syncing problems.
</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- tabs -->
                                    </div><!-- tab sectoin -->
									<div class="col-lg-3 col-sm-12">
										<div class="sidebar right">
											<div class="advertising-box blackish medium-opacity">
												<span>Advertisment</span>
												<div class="bg-image" style="background-image: url(images/banner2.jpg);"></div>
												<div class="baner-meta">
													<span>reffer a friend</span>
													<h5>Earn ₹99</h5>
													<p>now bring a smile on customer's face by giving theme points on each referral</p>
													<a href="#" title="" class="">Earn Now</a>
												</div>
											</div>
										</div>	
									</div>
                                    <div class="col-md-12">
                                    	<div class="space no-bottom">
                                            <div class="cola-notice blackish medium-opacity">
												<div class="bg-image" style="background-image: url(images/resources/notification-bg.jpg)"></div>
												<i class="flaticon-information"></i>
                                                <h4>Didn’t find an answer to your question?</h4>
                                                <span>Call us at (012) 345 - 6789</span>
                                                <a class="theme-btn" href="#" title="">or contact us</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
        			</div>
        		</div>
        	</div>
        </section>

<?php
    include_once('footerTop.php');
    include_once('footer.php');
?>