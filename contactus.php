<!DOCTYPE html>
	<!DOCTYPE html>
<html lang="en" >

<!-- Mirrored from wpkixx.com/html/car-carry/index.php by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 19 Feb 2020 06:54:34 GMT -->

<head>

    <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="" />
    <meta name="keywords" content="" />
	<title>RentHire Vehicles</title>
    <link rel="icon" type="image/png" href="images/favicon.ico">

    <link rel="stylesheet" href="css/apps.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/color.css">
    <link rel="stylesheet" href="css/responsive.css">
	
	<!-- REVOLUTION STYLE SHEETS -->
	<link rel="stylesheet" type="text/css" href="css/revolution/settings.css" />
	<link rel="stylesheet" type="text/css" href="css/revolution/navigation.css" />
	<link rel="stylesheet" type="text/css" href="css/revolution/pe-icon-7-stroke/css/pe-icon-7-stroke.css">

	<style>
		.logoCustomDefault {
		  display: block;
		  text-indent: -9999px;
		  width: 150px;
		  height: 50px;
		  background: url(images/logo-black.svg);
		  background-size: 150px 50px;
		}

		.logoCustomSecondary {
		  display: block;
		  text-indent: -9999px;
		  width: 150px;
		  height: 50px;
		  background: url(images/logo-black-secondary.svg);
		  background-size: 150px 50px;
		}
	</style>
</head>
<body >
	<div class="site-layout">
		
<?php 
	
	if(isset($_COOKIE['userName']))
	{
		include_once('dbclass.php');
		
		$obj = new dbclass();
		$data = $obj -> getCustomerData($_COOKIE['userName']);
//		die($data['fullName']);
		include_once('afterLogin.php');
		
		if(isset($_COOKIE['bookVehicleId']))
		{
			
			if($_COOKIE['redirectOnce'] == 1)
			{
				
				setcookie('redirectOnce','0');
				
				echo "<script>window.location = 'car-detail.php'</script>";
			}
		}
	}
	
	else
	{
		include_once('dbclass.php');
		include_once('beforeLogin.php');
	}
	
	
?>
		
		<div class="topbar" >
        	<div class="container" >
            	<div class="row">
                	<div class="col-md-12">
                        <header>
                            <div class="logo">
                                <a href="index.php" title="index.php" class="logoCustomSecondary">hireRent</a>
                            </div>
                            <nav>
                                <ul class="main-menu">
                                    <li><a href="index.php" title="" >Home</a>
										
									</li>
                                    <li><a href="#" title="" >cars</a>
										<ul>
											<li><a href="cars.php" title="">cars</a></li>
											<li><a href="car-profile.php" title="">car profile</a></li>
											<li><a href="car-detail.php" title="">cars detail</a></li>
											<li><a href="create-route-notification.php" title="">route notification</a></li>
											<li><a href="submit-car-intro.php" title="">Submit car introduction</a></li>
											<li><a href="submit-car-for-rent.php" title="">submit car for rent</a></li>
											<li><a href="submit-route.php" title="">submit car route</a></li>
										</ul>
									</li>
                                    <li><a href="#" title="" >rides</a>
										<ul>
											<li><a href="ride-listing.php" title="">ride listing</a></li>
											<li><a href="ride-request.php" title="">ride request</a></li>
											<li><a href="ride-detail.php" title="">ride detail</a></li>
											<li><a href="ride-requests-list.php" title="">ride request list</a></li>
										</ul>
									</li>
									<li><a href="#" title="" >leasing</a>
										<ul>
											<li><a href="leasing.php" title="">Leasing Page</a></li>
											<li><a href="leasing-car-step1.php" title="">leasing Step 1</a></li>
											<li><a href="leasing-car-step2.php" title="">leasing step 2</a></li>
										</ul>
									</li>
									<li><a href="#" title="" >profiles</a>
										<ul>
											<li><a href="user-profile.php" title="">Profile user</a></li>
											<li><a href="user-profile-v2.php" title="">Profile user v2</a></li>
											<li><a href="my-profile.php" title="">My profile</a></li>
											<li><a href="profile-balance.php" title="">profile balance</a></li>
											<li><a href="profile-earning.php" title="">profile earning</a></li>
											<li><a href="profile-edit.php" title="">profile edit page</a></li>
											<li><a href="profile-invite-friend.php" title="">profile invite friend</a></li>
											<li><a href="profile-points.php" title="">profile points</a></li>
										</ul>
									</li>
									<li><a href="#" title=""><i class="fa fa-ellipsis-v"></i></a>
										<ul>
											<li><a href="register.php" title="">Register Page</a></li>
											<li><a href="how-this-work.php" title="">how it's work</a></li>
											<li><a href="faq.php" title="">faq's</a></li>
											<li><a href="points.php" title="">poinst</a></li>
											<li><a href="coming-soon.php" title="">Coming Soon</a></li>
											<li><a href="404.php" title="">404 Page</a></li>
											
										</ul>
									</li>
                                </ul>
                            </nav>
                            <div class="time-info">
								<a href="https://wa.me/919998896188" target="_blank"><i class="fa fa-whatsapp fa-2x"></i></a>
								<span >Whatsapp now <br>+91-9979274670</span>
							</div>
                        </header>
                    </div>
                </div>
            </div>
        </div><!-- topbar header -->
        
		<div class="responsive-header">
			<div class="res-top">
				<ul>
					<li><a href="#" title="Home"><i class="flaticon-home"></i></a></li>
					<li><a href="#" title="Faq's"><i class="flaticon-info"></i></a></li>
					<li><a href="#" title="Support center"><i class="flaticon-support"></i></a></li>
					<li><a href="#" title="Login"><i class="flaticon-unlocked"></i></a></li>
					<li><a href="#" title="New register"><i class="flaticon-checked"></i></a></li>
					<li class="post-new"><a href="#" title="New post">+Post</a></li>
				</ul>
			</div>
			<div class="logomenu-bar">
				<div class="logo"><a href="index.php" title=""><img src="images/logo-black.png" alt=""></a></div>
				<span class="responsviemenu-btn"><i class="flaticon-menu-1"></i></span> 
			</div>
			<div class="responsive-menu">
				<span class="close-btn"><i class="flaticon-error-1"></i></span>
				<ul>
					<li><a href="index.php" title="">Home</a></li>
					<li class="menu-item-has-children"><a href="#" title="">cars</a>
						<ul class="sub-menu">
							<li><a href="index-after-login.php" title="">page after login</a></li>
							<li><a href="cars.php" title="">cars</a></li>
							<li><a href="car-profile.php" title="">car profile</a></li>
							<li><a href="car-detail.php" title="">cars detail</a></li>
							<li><a href="create-route-notification.php" title="">route notification</a></li>
							<li><a href="submit-car-intro.php" title="">Submit car introduction</a></li>
							<li><a href="submit-car-for-rent.php" title="">submit car for rent</a></li>
							<li><a href="submit-route.php" title="">submit car route</a></li>
						</ul>
					</li>
					<li class="menu-item-has-children"><a href="#" title="">rides</a>
						<ul class="sub-menu">
							<li><a href="ride-listing.php" title="">ride listing</a></li>
							<li><a href="ride-request.php" title="">ride request</a></li>
							<li><a href="ride-detail.php" title="">ride detail</a></li>
							<li><a href="ride-requests-list.php" title="">ride request list</a></li>
						</ul>
					</li>
					<li class="menu-item-has-children"><a href="#" title="">leasing</a>
						<ul class="sub-menu">
							<li><a href="leasing.php" title="">Leasing Page</a></li>
							<li><a href="leasing-car-step1.php" title="">leasing Step 1</a></li>
							<li><a href="leasing-car-step2.php" title="">leasing step 2</a></li>
						</ul>
					</li>
					<li class="menu-item-has-children"><a href="#" title="">profiles</a>
						<ul class="sub-menu">
							<li><a href="user-profile.php" title="">Profile user</a></li>
							<li><a href="user-profile-v2.php" title="">Profile user v2</a></li>
							<li><a href="my-profile.php" title="">My profile</a></li>
							<li><a href="profile-balance.php" title="">profile balance</a></li>
							<li><a href="profile-earning.php" title="">profile earning</a></li>
							<li><a href="profile-edit.php" title="">profile edit page</a></li>
							<li><a href="profile-invite-friend.php" title="">profile invite friend</a></li>
							<li><a href="profile-points.php" title="">profile points</a></li>
						</ul>
					</li>
					<li class="menu-item-has-children"><a href="#" title="">more pages</a>
						<ul class="sub-menu">
							<li><a href="login.php" title="">Login page</a></li>
							<li><a href="register.php" title="">Register Page</a></li>
							<li><a href="how-this-work.php" title="">how it's work</a></li>
							<li><a href="faq.php" title="">faq's</a></li>
							<li><a href="points.php" title="">poinst</a></li>
							<li><a href="coming-soon.php" title="">Coming Soon</a></li>
							<li><a href="404.php" title="">404 Page</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div><!-- Responsive Header -->
	
		
		

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Contact Form HTML/CSS Template - reusable form</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
        <link rel="stylesheet" href="contactus.css" >
		
       <!--own js-->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
		<script type="text/javascript">
		$(document).ready(function() {
			
			$("#contactus_btn").click(function(e) { 
				e.preventDefault();
				var proceed1 = true;
				//simple validation at client's end
				//loop through each field and we simply change border color to red for invalid fields       
				$("#contactus-form input[required=true]").each(function(){			
					$(this).css('border-color',''); 
					if(!$.trim($(this).val())){ //if this field is empty 
						$(this).css('border-color','red'); //change border color to red   
						proceed1 = false; //set do not proceed flag
					}						            			
				});
			   
				if(proceed1) //everything looks good! proceed...
				{	
					//Change button text and colour
					$("#contactus_btn").html("Sending...");
					$("#contactus_btn").css('background-color','orange');
					
					var comment = $.trim($("#comment").val());
					//get input field values data to be sent to server
					post_data1 = {              
						'name'    : $('input[name=name]').val(),				
						'email'    : $('input[name=email]').val(),
						'comment'    : comment
						<!--,
					<!--	'rememberMe'    : $('input[name=rememberMe]').'1'-->
						
					};
					
					console.log(post_data1);
					//Ajax post data to server
					$.post('contactus_js.php', post_data1, function(response){  	
						//console.log(response);
						if(response.type == 'error'){ //load json data from server and output message 
						
						//Change button text and colour
							$("#contactus_btn").html("SEND EMAIL");
							$("#contactus_btn").css('background-color','#3498db');
							
							
							output = '<div class="error" style="background: #FFE8E8;padding: 5px 10px 5px 10px;margin: 0px 0px 5px 0px;border: none;font-weight: bold;color: #FF0000;border-left: 3px solid #FF0000;">'+response.text+'</div>';
							 $("#contactus-form  input[required=true]").val(''); 
						}else{
							output = '<div class="success" style="background: #D8FFC0;padding: 5px 10px 5px 10px;margin: 0px 0px 5px 0px;border: none;font-weight: bold;color: #2E6800;border-left: 3px solid #2E6800;">'+response.text+'</div>';
							//reset values in all input fields
							
							$("#contactus-form #loginsub").slideUp().delay(2000).slideDown(500); //hide form after success
							
								$("#contactus-form  input[required=true]").val(''); 
							window.location.href = 'index.php';
						}
						$("#contactus-form #submitCommentResult").hide().html(output).slideDown();
					}, 'json');
				}
			});
			
			//reset previously set border colors and hide all message on .keyup()
			$("#contactus-form  input[required=true]").keyup(function() { 
				$(this).css('border-color',''); 
				$("#submitCommentResult").slideUp();
			});
		});
		</script>

	<section style="background-image: url('images/resources/contactus.jpg');">
        	<div class="space">
			
        		<div class="container">
        			<div class="row">
        				<div class="col-md-3">
        				</div>
        				<div class="col-md-6">
								<div class="box-authentication" id="contactus-form">
								<div id="submitCommentResult"></div>
								<div id="loginsub">
	                        	<form form action="" id="form-login" method="post" class="montform">
								
			                        <p class="name">
			                            <input name="name" type="text" class="feedback-input" required placeholder="Name" id="name" autocomplete="off"/>
			                        </p>
			                        <p class="email">
			                            <input name="email" type="email" required class="feedback-input" id="email" placeholder="Email" autocomplete="off"/>
			                        </p>
			                        <p class="text">
			                            <textarea name="comment" class="feedback-input" id="comment" placeholder="Message"></textarea>
			                        </p>
			                        <div class="submit">
			                            <button type="submit" id="contactus_btn" name="contactus_btn" class="button-blue">SEND EMAIL</button>
			                            <div class="ease"></div>
			                        </div>
			                    </form>
								</div>
								</div>
                        </div>
        			</div>
        		</div>
        	</div>
			</div>
     </section><!-- contact us -->
	<section>
			<div class="space no-space dark-bg">
				<div class="container">
					<div class="row">
						<div class="col-lg-4 col-sm-4">
							<div class="servize">
								<i class="flaticon-placeholder"></i>
								<div class="serviz-meta">
									<span>E-Space,Vesu,Surat</span>
									<i>India</i>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-sm-4">
							<div class="servize">
								<i class="flaticon-24-hours-1"></i>
								<div class="serviz-meta">
									<span>Tool free number 24/7</span>
									<i>+91-997-927-4670</i>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-sm-4">
							<div class="servize">
								<i class="flaticon-24-hours"></i>
								<div class="serviz-meta">
									<span>Full time services</span>
									<i>24/7</i>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section><!-- footer top -->
        
        <footer class="light">
			<div class="mockup-right">
				<img src="images/resources/footer-mockup.png" alt="">
			</div>
        	<div class="container">
            	<div class="row">
                	<div class="col-lg-4 col-sm-6">
                    	<div class="widget">
                            <a href="index.html" title="" class="logoCustomSecondary"></a>
                            <p>
                            	The trio took this simple idea and built it into the world&#39s leading carpooling platform, connecting millions of people going the same way. 
                            </p>
							<ul class="social">
								<li><a href="https://www.facebook.com/" target="_blank"><img src="images/facebook.png" alt=""></a></li>
                                <li><a href="https://www.instagram.com/" target="_blank"><img src="images/instagram.png" alt=""></a></li>
                                <li><a href="https://www.twitter.com/" target="_blank"><img src="images/twitter.png" alt=""></a></li>
                                <li><a href="https://www.youtube.com/" target="_blank"><img src="images/youtube.png" alt=""></a></li>
								
							</ul>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-6">
                    	<div class="widget">
                        	<div class="widget-title"><h4>useful links</h4></div>
                            <ul class="list-style">
                            	<li><a href="#" title="">leasing</a></li>
                                <li><a href="#" title="">submit route</a></li>
                                <li><a href="#" title="">how does it work?</a></li>
                                <li><a href="#" title="">agent listings</a></li>
                            </ul>

                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <div class="widget">
                            <div class="widget-title"><h4>weekly news letter</h4></div>
                            <div class="news-letter">
                                <p>We may send you information about related events, webinars, products and services which we believe.</p>
                                <form action="dbclass.php" method="post">
                                    <input type="email" name="newsLetterSubscriberEmail" id="newsLetterSubscriberEmail" placeholder="Enter your email address">
                                    <button name="newslettersubscriber_btn" id="newslettersubscriber_btn" type="submit"><i class="fa fa-paper-plane-o"></i></button>
                                </form>
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
			<div class="bottombar">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<span class="copyright">� 2019. All rights reserved.</span>
							<i><img src="images/credit-cards.png" alt=""></i>
						</div>
					</div>
				</div>
			</div>
        </footer><!-- footer -->

	</div>
	<script src="js/apps.min.js"></script>
    <script src="js/script.js"></script>
	
	<!-- REVOLUTION JS FILES -->
		<script type="text/javascript" src="js/revolution/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="js/revolution/jquery.themepunch.revolution.min.js"></script>

		<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  -->	
		<script src="js/revolution/revolution.extension.actions.min.js"></script>
		<script src="js/revolution/revolution.extension.carousel.min.js"></script>
		<script src="js/revolution/revolution.extension.kenburn.min.js"></script>
		<script src="js/revolution/revolution.extension.layeranimation.min.js"></script>
		<script src="js/revolution/revolution.extension.migration.min.js"></script>
		<script src="js/revolution/revolution.extension.navigation.min.js"></script>
		<script src="js/revolution/revolution.extension.parallax.min.js"></script>
		<script src="js/revolution/revolution.extension.slideanims.min.js"></script>
		<script src="js/revolution/revolution.extension.video.min.js"></script>
		<script src="js/revolution/revolution.initialize.js"></script>
        
</body>	

<!-- Mirrored from wpkixx.com/html/car-carry/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 19 Feb 2020 06:55:19 GMT -->
</html>		
	