-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 29, 2020 at 12:06 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rentalservice`
--

-- --------------------------------------------------------

--
-- Table structure for table `booking_history`
--

CREATE TABLE `booking_history` (
  `booking_id` bigint(20) NOT NULL,
  `vehicleId` varchar(50) NOT NULL,
  `vehicleOwnerId` bigint(20) NOT NULL,
  `customerId` bigint(20) NOT NULL,
  `start_date` date NOT NULL,
  `start_time` time NOT NULL,
  `end_date` date NOT NULL,
  `end_time` time NOT NULL,
  `rate` float NOT NULL,
  `startOtp` bigint(20) NOT NULL,
  `rideStart` int(11) NOT NULL DEFAULT 0,
  `start_km` float NOT NULL DEFAULT 0,
  `actual_startTime` timestamp NOT NULL DEFAULT current_timestamp(),
  `actual_endTime` timestamp NOT NULL DEFAULT current_timestamp(),
  `end_km` float NOT NULL,
  `amount_paid` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `booking_history`
--

INSERT INTO `booking_history` (`booking_id`, `vehicleId`, `vehicleOwnerId`, `customerId`, `start_date`, `start_time`, `end_date`, `end_time`, `rate`, `startOtp`, `rideStart`, `start_km`, `actual_startTime`, `actual_endTime`, `end_km`, `amount_paid`) VALUES
(32, '18PN-05-VS-2473', 18, 10, '2020-05-29', '03:00:00', '2020-05-30', '02:00:00', 15, 81217, 1, 1, '2020-05-28 20:16:38', '2020-05-28 20:52:34', 15, 210);

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `cityId` int(11) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`cityId`, `city`, `state_id`) VALUES
(1, 'North and Middle Andaman', 32),
(2, 'South Andaman', 32),
(3, 'Nicobar', 32),
(4, 'Adilabad', 1),
(5, 'Anantapur', 1),
(6, 'Chittoor', 1),
(7, 'East Godavari', 1),
(8, 'Guntur', 1),
(9, 'Hyderabad', 1),
(10, 'Kadapa', 1),
(11, 'Karimnagar', 1),
(12, 'Khammam', 1),
(13, 'Krishna', 1),
(14, 'Kurnool', 1),
(15, 'Mahbubnagar', 1),
(16, 'Medak', 1),
(17, 'Nalgonda', 1),
(18, 'Nellore', 1),
(19, 'Nizamabad', 1),
(20, 'Prakasam', 1),
(21, 'Rangareddi', 1),
(22, 'Srikakulam', 1),
(23, 'Vishakhapatnam', 1),
(24, 'Vizianagaram', 1),
(25, 'Warangal', 1),
(26, 'West Godavari', 1),
(27, 'Anjaw', 3),
(28, 'Changlang', 3),
(29, 'East Kameng', 3),
(30, 'Lohit', 3),
(31, 'Lower Subansiri', 3),
(32, 'Papum Pare', 3),
(33, 'Tirap', 3),
(34, 'Dibang Valley', 3),
(35, 'Upper Subansiri', 3),
(36, 'West Kameng', 3),
(37, 'Barpeta', 2),
(38, 'Bongaigaon', 2),
(39, 'Cachar', 2),
(40, 'Darrang', 2),
(41, 'Dhemaji', 2),
(42, 'Dhubri', 2),
(43, 'Dibrugarh', 2),
(44, 'Goalpara', 2),
(45, 'Golaghat', 2),
(46, 'Hailakandi', 2),
(47, 'Jorhat', 2),
(48, 'Karbi Anglong', 2),
(49, 'Karimganj', 2),
(50, 'Kokrajhar', 2),
(51, 'Lakhimpur', 2),
(52, 'Marigaon', 2),
(53, 'Nagaon', 2),
(54, 'Nalbari', 2),
(55, 'North Cachar Hills', 2),
(56, 'Sibsagar', 2),
(57, 'Sonitpur', 2),
(58, 'Tinsukia', 2),
(59, 'Araria', 4),
(60, 'Aurangabad', 4),
(61, 'Banka', 4),
(62, 'Begusarai', 4),
(63, 'Bhagalpur', 4),
(64, 'Bhojpur', 4),
(65, 'Buxar', 4),
(66, 'Darbhanga', 4),
(67, 'Purba Champaran', 4),
(68, 'Gaya', 4),
(69, 'Gopalganj', 4),
(70, 'Jamui', 4),
(71, 'Jehanabad', 4),
(72, 'Khagaria', 4),
(73, 'Kishanganj', 4),
(74, 'Kaimur', 4),
(75, 'Katihar', 4),
(76, 'Lakhisarai', 4),
(77, 'Madhubani', 4),
(78, 'Munger', 4),
(79, 'Madhepura', 4),
(80, 'Muzaffarpur', 4),
(81, 'Nalanda', 4),
(82, 'Nawada', 4),
(83, 'Patna', 4),
(84, 'Purnia', 4),
(85, 'Rohtas', 4),
(86, 'Saharsa', 4),
(87, 'Samastipur', 4),
(88, 'Sheohar', 4),
(89, 'Sheikhpura', 4),
(90, 'Saran', 4),
(91, 'Sitamarhi', 4),
(92, 'Supaul', 4),
(93, 'Siwan', 4),
(94, 'Vaishali', 4),
(95, 'Pashchim Champaran', 4),
(96, 'Bastar', 36),
(97, 'Bilaspur', 36),
(98, 'Dantewada', 36),
(99, 'Dhamtari', 36),
(100, 'Durg', 36),
(101, 'Jashpur', 36),
(102, 'Janjgir-Champa', 36),
(103, 'Korba', 36),
(104, 'Koriya', 36),
(105, 'Kanker', 36),
(106, 'Kawardha', 36),
(107, 'Mahasamund', 36),
(108, 'Raigarh', 36),
(109, 'Rajnandgaon', 36),
(110, 'Raipur', 36),
(111, 'Surguja', 36),
(112, 'Diu', 29),
(113, 'Daman', 29),
(114, 'Central Delhi', 25),
(115, 'East Delhi', 25),
(116, 'New Delhi', 25),
(117, 'North Delhi', 25),
(118, 'North East Delhi', 25),
(119, 'North West Delhi', 25),
(120, 'South Delhi', 25),
(121, 'South West Delhi', 25),
(122, 'West Delhi', 25),
(123, 'North Goa', 26),
(124, 'South Goa', 26),
(125, 'Ahmedabad', 5),
(126, 'Amreli District', 5),
(127, 'Anand', 5),
(128, 'Banaskantha', 5),
(129, 'Bharuch', 5),
(130, 'Bhavnagar', 5),
(131, 'Dahod', 5),
(132, 'The Dangs', 5),
(133, 'Gandhinagar', 5),
(134, 'Jamnagar', 5),
(135, 'Junagadh', 5),
(136, 'Kutch', 5),
(137, 'Kheda', 5),
(138, 'Mehsana', 5),
(139, 'Narmada', 5),
(140, 'Navsari', 5),
(141, 'Patan', 5),
(142, 'Panchmahal', 5),
(143, 'Porbandar', 5),
(144, 'Rajkot', 5),
(145, 'Sabarkantha', 5),
(146, 'Surendranagar', 5),
(147, 'Surat', 5),
(148, 'Vadodara', 5),
(149, 'Valsad', 5),
(150, 'Ambala', 6),
(151, 'Bhiwani', 6),
(152, 'Faridabad', 6),
(153, 'Fatehabad', 6),
(154, 'Gurgaon', 6),
(155, 'Hissar', 6),
(156, 'Jhajjar', 6),
(157, 'Jind', 6),
(158, 'Karnal', 6),
(159, 'Kaithal', 6),
(160, 'Kurukshetra', 6),
(161, 'Mahendragarh', 6),
(162, 'Mewat', 6),
(163, 'Panchkula', 6),
(164, 'Panipat', 6),
(165, 'Rewari', 6),
(166, 'Rohtak', 6),
(167, 'Sirsa', 6),
(168, 'Sonepat', 6),
(169, 'Yamuna Nagar', 6),
(170, 'Palwal', 6),
(171, 'Bilaspur', 7),
(172, 'Chamba', 7),
(173, 'Hamirpur', 7),
(174, 'Kangra', 7),
(175, 'Kinnaur', 7),
(176, 'Kulu', 7),
(177, 'Lahaul and Spiti', 7),
(178, 'Mandi', 7),
(179, 'Shimla', 7),
(180, 'Sirmaur', 7),
(181, 'Solan', 7),
(182, 'Una', 7),
(183, 'Anantnag', 8),
(184, 'Badgam', 8),
(185, 'Bandipore', 8),
(186, 'Baramula', 8),
(187, 'Doda', 8),
(188, 'Jammu', 8),
(189, 'Kargil', 8),
(190, 'Kathua', 8),
(191, 'Kupwara', 8),
(192, 'Leh', 8),
(193, 'Poonch', 8),
(194, 'Pulwama', 8),
(195, 'Rajauri', 8),
(196, 'Srinagar', 8),
(197, 'Samba', 8),
(198, 'Udhampur', 8),
(199, 'Bokaro', 34),
(200, 'Chatra', 34),
(201, 'Deoghar', 34),
(202, 'Dhanbad', 34),
(203, 'Dumka', 34),
(204, 'Purba Singhbhum', 34),
(205, 'Garhwa', 34),
(206, 'Giridih', 34),
(207, 'Godda', 34),
(208, 'Gumla', 34),
(209, 'Hazaribagh', 34),
(210, 'Koderma', 34),
(211, 'Lohardaga', 34),
(212, 'Pakur', 34),
(213, 'Palamu', 34),
(214, 'Ranchi', 34),
(215, 'Sahibganj', 34),
(216, 'Seraikela and Kharsawan', 34),
(217, 'Pashchim Singhbhum', 34),
(218, 'Ramgarh', 34),
(219, 'Bidar', 9),
(220, 'Belgaum', 9),
(221, 'Bijapur', 9),
(222, 'Bagalkot', 9),
(223, 'Bellary', 9),
(224, 'Bangalore Rural District', 9),
(225, 'Bangalore Urban District', 9),
(226, 'Chamarajnagar', 9),
(227, 'Chikmagalur', 9),
(228, 'Chitradurga', 9),
(229, 'Davanagere', 9),
(230, 'Dharwad', 9),
(231, 'Dakshina Kannada', 9),
(232, 'Gadag', 9),
(233, 'Gulbarga', 9),
(234, 'Hassan', 9),
(235, 'Haveri District', 9),
(236, 'Kodagu', 9),
(237, 'Kolar', 9),
(238, 'Koppal', 9),
(239, 'Mandya', 9),
(240, 'Mysore', 9),
(241, 'Raichur', 9),
(242, 'Shimoga', 9),
(243, 'Tumkur', 9),
(244, 'Udupi', 9),
(245, 'Uttara Kannada', 9),
(246, 'Ramanagara', 9),
(247, 'Chikballapur', 9),
(248, 'Yadagiri', 9),
(249, 'Alappuzha', 10),
(250, 'Ernakulam', 10),
(251, 'Idukki', 10),
(252, 'Kollam', 10),
(253, 'Kannur', 10),
(254, 'Kasaragod', 10),
(255, 'Kottayam', 10),
(256, 'Kozhikode', 10),
(257, 'Malappuram', 10),
(258, 'Palakkad', 10),
(259, 'Pathanamthitta', 10),
(260, 'Thrissur', 10),
(261, 'Thiruvananthapuram', 10),
(262, 'Wayanad', 10),
(263, 'Alirajpur', 11),
(264, 'Anuppur', 11),
(265, 'Ashok Nagar', 11),
(266, 'Balaghat', 11),
(267, 'Barwani', 11),
(268, 'Betul', 11),
(269, 'Bhind', 11),
(270, 'Bhopal', 11),
(271, 'Burhanpur', 11),
(272, 'Chhatarpur', 11),
(273, 'Chhindwara', 11),
(274, 'Damoh', 11),
(275, 'Datia', 11),
(276, 'Dewas', 11),
(277, 'Dhar', 11),
(278, 'Dindori', 11),
(279, 'Guna', 11),
(280, 'Gwalior', 11),
(281, 'Harda', 11),
(282, 'Hoshangabad', 11),
(283, 'Indore', 11),
(284, 'Jabalpur', 11),
(285, 'Jhabua', 11),
(286, 'Katni', 11),
(287, 'Khandwa', 11),
(288, 'Khargone', 11),
(289, 'Mandla', 11),
(290, 'Mandsaur', 11),
(291, 'Morena', 11),
(292, 'Narsinghpur', 11),
(293, 'Neemuch', 11),
(294, 'Panna', 11),
(295, 'Rewa', 11),
(296, 'Rajgarh', 11),
(297, 'Ratlam', 11),
(298, 'Raisen', 11),
(299, 'Sagar', 11),
(300, 'Satna', 11),
(301, 'Sehore', 11),
(302, 'Seoni', 11),
(303, 'Shahdol', 11),
(304, 'Shajapur', 11),
(305, 'Sheopur', 11),
(306, 'Shivpuri', 11),
(307, 'Sidhi', 11),
(308, 'Singrauli', 11),
(309, 'Tikamgarh', 11),
(310, 'Ujjain', 11),
(311, 'Umaria', 11),
(312, 'Vidisha', 11),
(313, 'Ahmednagar', 12),
(314, 'Akola', 12),
(315, 'Amrawati', 12),
(316, 'Aurangabad', 12),
(317, 'Bhandara', 12),
(318, 'Beed', 12),
(319, 'Buldhana', 12),
(320, 'Chandrapur', 12),
(321, 'Dhule', 12),
(322, 'Gadchiroli', 12),
(323, 'Gondiya', 12),
(324, 'Hingoli', 12),
(325, 'Jalgaon', 12),
(326, 'Jalna', 12),
(327, 'Kolhapur', 12),
(328, 'Latur', 12),
(329, 'Mumbai City', 12),
(330, 'Mumbai suburban', 12),
(331, 'Nandurbar', 12),
(332, 'Nanded', 12),
(333, 'Nagpur', 12),
(334, 'Nashik', 12),
(335, 'Osmanabad', 12),
(336, 'Parbhani', 12),
(337, 'Pune', 12),
(338, 'Raigad', 12),
(339, 'Ratnagiri', 12),
(340, 'Sindhudurg', 12),
(341, 'Sangli', 12),
(342, 'Solapur', 12),
(343, 'Satara', 12),
(344, 'Thane', 12),
(345, 'Wardha', 12),
(346, 'Washim', 12),
(347, 'Yavatmal', 12),
(348, 'Bishnupur', 13),
(349, 'Churachandpur', 13),
(350, 'Chandel', 13),
(351, 'Imphal East', 13),
(352, 'Senapati', 13),
(353, 'Tamenglong', 13),
(354, 'Thoubal', 13),
(355, 'Ukhrul', 13),
(356, 'Imphal West', 13),
(357, 'East Garo Hills', 14),
(358, 'East Khasi Hills', 14),
(359, 'Jaintia Hills', 14),
(360, 'Ri-Bhoi', 14),
(361, 'South Garo Hills', 14),
(362, 'West Garo Hills', 14),
(363, 'West Khasi Hills', 14),
(364, 'Aizawl', 15),
(365, 'Champhai', 15),
(366, 'Kolasib', 15),
(367, 'Lawngtlai', 15),
(368, 'Lunglei', 15),
(369, 'Mamit', 15),
(370, 'Saiha', 15),
(371, 'Serchhip', 15),
(372, 'Dimapur', 16),
(373, 'Kohima', 16),
(374, 'Mokokchung', 16),
(375, 'Mon', 16),
(376, 'Phek', 16),
(377, 'Tuensang', 16),
(378, 'Wokha', 16),
(379, 'Zunheboto', 16),
(380, 'Angul', 17),
(381, 'Boudh', 17),
(382, 'Bhadrak', 17),
(383, 'Bolangir', 17),
(384, 'Bargarh', 17),
(385, 'Baleswar', 17),
(386, 'Cuttack', 17),
(387, 'Debagarh', 17),
(388, 'Dhenkanal', 17),
(389, 'Ganjam', 17),
(390, 'Gajapati', 17),
(391, 'Jharsuguda', 17),
(392, 'Jajapur', 17),
(393, 'Jagatsinghpur', 17),
(394, 'Khordha', 17),
(395, 'Kendujhar', 17),
(396, 'Kalahandi', 17),
(397, 'Kandhamal', 17),
(398, 'Koraput', 17),
(399, 'Kendrapara', 17),
(400, 'Malkangiri', 17),
(401, 'Mayurbhanj', 17),
(402, 'Nabarangpur', 17),
(403, 'Nuapada', 17),
(404, 'Nayagarh', 17),
(405, 'Puri', 17),
(406, 'Rayagada', 17),
(407, 'Sambalpur', 17),
(408, 'Subarnapur', 17),
(409, 'Sundargarh', 17),
(410, 'Karaikal', 27),
(411, 'Mahe', 27),
(412, 'Puducherry', 27),
(413, 'Yanam', 27),
(414, 'Amritsar', 18),
(415, 'Bathinda', 18),
(416, 'Firozpur', 18),
(417, 'Faridkot', 18),
(418, 'Fatehgarh Sahib', 18),
(419, 'Gurdaspur', 18),
(420, 'Hoshiarpur', 18),
(421, 'Jalandhar', 18),
(422, 'Kapurthala', 18),
(423, 'Ludhiana', 18),
(424, 'Mansa', 18),
(425, 'Moga', 18),
(426, 'Mukatsar', 18),
(427, 'Nawan Shehar', 18),
(428, 'Patiala', 18),
(429, 'Rupnagar', 18),
(430, 'Sangrur', 18),
(431, 'Ajmer', 19),
(432, 'Alwar', 19),
(433, 'Bikaner', 19),
(434, 'Barmer', 19),
(435, 'Banswara', 19),
(436, 'Bharatpur', 19),
(437, 'Baran', 19),
(438, 'Bundi', 19),
(439, 'Bhilwara', 19),
(440, 'Churu', 19),
(441, 'Chittorgarh', 19),
(442, 'Dausa', 19),
(443, 'Dholpur', 19),
(444, 'Dungapur', 19),
(445, 'Ganganagar', 19),
(446, 'Hanumangarh', 19),
(447, 'Juhnjhunun', 19),
(448, 'Jalore', 19),
(449, 'Jodhpur', 19),
(450, 'Jaipur', 19),
(451, 'Jaisalmer', 19),
(452, 'Jhalawar', 19),
(453, 'Karauli', 19),
(454, 'Kota', 19),
(455, 'Nagaur', 19),
(456, 'Pali', 19),
(457, 'Pratapgarh', 19),
(458, 'Rajsamand', 19),
(459, 'Sikar', 19),
(460, 'Sawai Madhopur', 19),
(461, 'Sirohi', 19),
(462, 'Tonk', 19),
(463, 'Udaipur', 19),
(464, 'East Sikkim', 20),
(465, 'North Sikkim', 20),
(466, 'South Sikkim', 20),
(467, 'West Sikkim', 20),
(468, 'Ariyalur', 21),
(469, 'Chennai', 21),
(470, 'Coimbatore', 21),
(471, 'Cuddalore', 21),
(472, 'Dharmapuri', 21),
(473, 'Dindigul', 21),
(474, 'Erode', 21),
(475, 'Kanchipuram', 21),
(476, 'Kanyakumari', 21),
(477, 'Karur', 21),
(478, 'Madurai', 21),
(479, 'Nagapattinam', 21),
(480, 'The Nilgiris', 21),
(481, 'Namakkal', 21),
(482, 'Perambalur', 21),
(483, 'Pudukkottai', 21),
(484, 'Ramanathapuram', 21),
(485, 'Salem', 21),
(486, 'Sivagangai', 21),
(487, 'Tiruppur', 21),
(488, 'Tiruchirappalli', 21),
(489, 'Theni', 21),
(490, 'Tirunelveli', 21),
(491, 'Thanjavur', 21),
(492, 'Thoothukudi', 21),
(493, 'Thiruvallur', 21),
(494, 'Thiruvarur', 21),
(495, 'Tiruvannamalai', 21),
(496, 'Vellore', 21),
(497, 'Villupuram', 21),
(498, 'Dhalai', 22),
(499, 'North Tripura', 22),
(500, 'South Tripura', 22),
(501, 'West Tripura', 22),
(502, 'Almora', 33),
(503, 'Bageshwar', 33),
(504, 'Chamoli', 33),
(505, 'Champawat', 33),
(506, 'Dehradun', 33),
(507, 'Haridwar', 33),
(508, 'Nainital', 33),
(509, 'Pauri Garhwal', 33),
(510, 'Pithoragharh', 33),
(511, 'Rudraprayag', 33),
(512, 'Tehri Garhwal', 33),
(513, 'Udham Singh Nagar', 33),
(514, 'Uttarkashi', 33),
(515, 'Agra', 23),
(516, 'Allahabad', 23),
(517, 'Aligarh', 23),
(518, 'Ambedkar Nagar', 23),
(519, 'Auraiya', 23),
(520, 'Azamgarh', 23),
(521, 'Barabanki', 23),
(522, 'Badaun', 23),
(523, 'Bagpat', 23),
(524, 'Bahraich', 23),
(525, 'Bijnor', 23),
(526, 'Ballia', 23),
(527, 'Banda', 23),
(528, 'Balrampur', 23),
(529, 'Bareilly', 23),
(530, 'Basti', 23),
(531, 'Bulandshahr', 23),
(532, 'Chandauli', 23),
(533, 'Chitrakoot', 23),
(534, 'Deoria', 23),
(535, 'Etah', 23),
(536, 'Kanshiram Nagar', 23),
(537, 'Etawah', 23),
(538, 'Firozabad', 23),
(539, 'Farrukhabad', 23),
(540, 'Fatehpur', 23),
(541, 'Faizabad', 23),
(542, 'Gautam Buddha Nagar', 23),
(543, 'Gonda', 23),
(544, 'Ghazipur', 23),
(545, 'Gorkakhpur', 23),
(546, 'Ghaziabad', 23),
(547, 'Hamirpur', 23),
(548, 'Hardoi', 23),
(549, 'Mahamaya Nagar', 23),
(550, 'Jhansi', 23),
(551, 'Jalaun', 23),
(552, 'Jyotiba Phule Nagar', 23),
(553, 'Jaunpur District', 23),
(554, 'Kanpur Dehat', 23),
(555, 'Kannauj', 23),
(556, 'Kanpur Nagar', 23),
(557, 'Kaushambi', 23),
(558, 'Kushinagar', 23),
(559, 'Lalitpur', 23),
(560, 'Lakhimpur Kheri', 23),
(561, 'Lucknow', 23),
(562, 'Mau', 23),
(563, 'Meerut', 23),
(564, 'Maharajganj', 23),
(565, 'Mahoba', 23),
(566, 'Mirzapur', 23),
(567, 'Moradabad', 23),
(568, 'Mainpuri', 23),
(569, 'Mathura', 23),
(570, 'Muzaffarnagar', 23),
(571, 'Pilibhit', 23),
(572, 'Pratapgarh', 23),
(573, 'Rampur', 23),
(574, 'Rae Bareli', 23),
(575, 'Saharanpur', 23),
(576, 'Sitapur', 23),
(577, 'Shahjahanpur', 23),
(578, 'Sant Kabir Nagar', 23),
(579, 'Siddharthnagar', 23),
(580, 'Sonbhadra', 23),
(581, 'Sant Ravidas Nagar', 23),
(582, 'Sultanpur', 23),
(583, 'Shravasti', 23),
(584, 'Unnao', 23),
(585, 'Varanasi', 23),
(586, 'Birbhum', 24),
(587, 'Bankura', 24),
(588, 'Bardhaman', 24),
(589, 'Darjeeling', 24),
(590, 'Dakshin Dinajpur', 24),
(591, 'Hooghly', 24),
(592, 'Howrah', 24),
(593, 'Jalpaiguri', 24),
(594, 'Cooch Behar', 24),
(595, 'Kolkata', 24),
(596, 'Malda', 24),
(597, 'Midnapore', 24),
(598, 'Murshidabad', 24),
(599, 'Nadia', 24),
(600, 'North 24 Parganas', 24),
(601, 'South 24 Parganas', 24),
(602, 'Purulia', 24),
(603, 'Uttar Dinajpur', 24);

-- --------------------------------------------------------

--
-- Table structure for table `contactus_tbl`
--

CREATE TABLE `contactus_tbl` (
  `contactus_id` bigint(20) NOT NULL,
  `fullName` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `comment` varchar(200) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `contactus_tbl`
--

INSERT INTO `contactus_tbl` (`contactus_id`, `fullName`, `email`, `comment`, `status`) VALUES
(1, 'keyur', 'keyurmacwana@gmail.com', 'test1', 0),
(2, 'keyur', 'keyurmacwana@gmail.com', 'test1', 0),
(3, 'keyur', 'keyurmacwana@gmail.com', 'test1', 0),
(4, 'asdd', 'keyurmacwana@gmail.com', 'awd', 0),
(5, 'asdd', 'keyurmacwana@gmail.com', 'awdawd', 0),
(6, 'asdd', 'keyurmacwana@gmail.com', 'awdawd', 0),
(7, 'zsczs', 'keyurmacwana@gmail.com', 'awdawdw', 0),
(8, 'nitntweghjikvbew', 'efjnesakf@eafaf.com', 'akfjnwajikfwna', 0),
(9, 'nitntweghjikvbew', 'efjnesakf@eafaf.com', 'akfjnwajikfwna', 0),
(10, 'esf', 'sefs@seg.com', 'sefgesg', 0),
(11, 'awdawd', 'adwd@srhs.sfes', 'afvwf', 0),
(12, 'awdwad', 'awdawd@rhrd.drgdr', 'aefw', 0),
(13, 'adawd', 'segesf@htd.sef', 'efddas', 0),
(14, 'srgse', 'esgfe@sgr.fe', 'eaf', 0),
(15, 'adf', 'awd@gr.sef', 'af', 0),
(16, 'awd', 'awd@grs.drg', 'ef', 0),
(17, 'sefs', 'sef@esg.ef', 'ef', 0),
(18, 'fhgg', 'df@gg.cu', 'gdfgdh', 0);

-- --------------------------------------------------------

--
-- Table structure for table `customer_tbl`
--

CREATE TABLE `customer_tbl` (
  `customerId` bigint(20) NOT NULL,
  `fullName` varchar(50) NOT NULL,
  `mobile` bigint(10) NOT NULL,
  `street` varchar(150) NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `licenseLink` varchar(200) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `walletId` bigint(20) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT 1,
  `dateOfBirth` date NOT NULL,
  `pinCode` bigint(10) NOT NULL,
  `licenseNumber` varchar(20) NOT NULL,
  `issueDate` date NOT NULL,
  `expirationDate` date NOT NULL,
  `vehiclePrefrence` varchar(100) NOT NULL,
  `profile` varchar(200) NOT NULL,
  `security_deposite` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customer_tbl`
--

INSERT INTO `customer_tbl` (`customerId`, `fullName`, `mobile`, `street`, `city`, `state`, `licenseLink`, `password`, `email`, `walletId`, `is_active`, `dateOfBirth`, `pinCode`, `licenseNumber`, `issueDate`, `expirationDate`, `vehiclePrefrence`, `profile`, `security_deposite`) VALUES
(10, 'keyur macwana', 9537371017, 'samarth enclave,vip road', 'Surat', '', 'drivingLicense9537371017.png', 'U2JKSm8yK1I2ZUhzaG5GbkgzT09HUT09', 'keyurmacwana@gmail.com', 0, 1, '1997-06-07', 0, '', '1995-04-03', '1995-01-03', 'TWO WHEELER,FOUR WHEELER', '', 4);

-- --------------------------------------------------------

--
-- Table structure for table `deleted_booking`
--

CREATE TABLE `deleted_booking` (
  `booking_id` bigint(20) NOT NULL,
  `vehicleId` varchar(50) NOT NULL,
  `vehicleOwnerId` bigint(20) NOT NULL,
  `customerId` bigint(20) NOT NULL,
  `start_date` date NOT NULL,
  `start_time` time NOT NULL,
  `end_date` date NOT NULL,
  `end_time` time NOT NULL,
  `rate` float NOT NULL,
  `startOtp` bigint(20) NOT NULL,
  `rideStart` int(11) NOT NULL DEFAULT 0,
  `start_km` float NOT NULL DEFAULT 0,
  `actual_startTime` timestamp NOT NULL DEFAULT current_timestamp(),
  `actual_endTime` timestamp NOT NULL DEFAULT current_timestamp(),
  `end_km` float NOT NULL,
  `amount_paid` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `deleted_newbooking`
--

CREATE TABLE `deleted_newbooking` (
  `booking_id` bigint(20) NOT NULL,
  `vehicleId` varchar(50) NOT NULL,
  `vehicleOwnerId` bigint(20) NOT NULL,
  `customerId` bigint(20) NOT NULL,
  `start_date` date NOT NULL,
  `start_time` time NOT NULL,
  `end_date` date NOT NULL,
  `end_time` time NOT NULL,
  `rate` float NOT NULL,
  `startOtp` bigint(20) NOT NULL,
  `rideStart` int(11) NOT NULL DEFAULT 0,
  `start_km` float NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `fueltype_tbl`
--

CREATE TABLE `fueltype_tbl` (
  `fuelType_id` bigint(20) NOT NULL,
  `fuelType_Name` varchar(50) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT 0,
  `added_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `delete_status` int(11) NOT NULL DEFAULT 0,
  `adminSignature` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fueltype_tbl`
--

INSERT INTO `fueltype_tbl` (`fuelType_id`, `fuelType_Name`, `is_active`, `added_date`, `delete_status`, `adminSignature`) VALUES
(1, 'Petrol', 1, '2020-03-17 09:01:34', 0, 0),
(2, 'Diesel', 1, '2020-03-17 09:01:34', 0, 0),
(3, 'Electric', 1, '2020-03-17 09:01:34', 0, 0),
(4, 'CNG', 1, '2020-03-17 09:01:34', 0, 0),
(5, 'Hydrogen', 1, '2020-03-17 09:01:34', 0, 0),
(6, 'kerose ', 0, '2020-03-17 09:16:28', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `home_page_banner`
--

CREATE TABLE `home_page_banner` (
  `bannerId` bigint(20) NOT NULL,
  `mainText1` varchar(50) NOT NULL,
  `subText` varchar(100) NOT NULL,
  `hrefLink` varchar(200) NOT NULL,
  `hrefText` varchar(50) NOT NULL,
  `bannerImageLink` varchar(200) NOT NULL,
  `mainText2` varchar(50) NOT NULL,
  `mainText3` varchar(50) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT 1,
  `delete_status` int(11) NOT NULL DEFAULT 0,
  `adminSignature` bigint(20) NOT NULL,
  `added_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `home_page_banner`
--

INSERT INTO `home_page_banner` (`bannerId`, `mainText1`, `subText`, `hrefLink`, `hrefText`, `bannerImageLink`, `mainText2`, `mainText3`, `is_active`, `delete_status`, `adminSignature`, `added_date`) VALUES
(4, 'Expreience', 'Dont just take our word for it. We are rated at 9.7/10 on independent, review website TrustPilot, fr', 'http://localhost/rentalservice/customer/index.php', 'REGISTER NOW', 'Array', 'Cars', 'Leasing', 1, 0, 1, '2020-02-29 12:55:01');

-- --------------------------------------------------------

--
-- Table structure for table `newbooking`
--

CREATE TABLE `newbooking` (
  `booking_id` bigint(20) NOT NULL,
  `vehicleId` varchar(50) NOT NULL,
  `vehicleOwnerId` bigint(20) NOT NULL,
  `customerId` bigint(20) NOT NULL,
  `start_date` date NOT NULL,
  `start_time` time NOT NULL,
  `end_date` date NOT NULL,
  `end_time` time NOT NULL,
  `rate` float NOT NULL,
  `startOtp` bigint(20) NOT NULL,
  `rideStart` int(11) NOT NULL DEFAULT 0,
  `start_km` float NOT NULL DEFAULT 0,
  `actual_startTime` timestamp NOT NULL DEFAULT current_timestamp(),
  `actual_endTime` timestamp NOT NULL DEFAULT current_timestamp(),
  `end_km` float NOT NULL,
  `amount_paid` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `newslettersubscriber`
--

CREATE TABLE `newslettersubscriber` (
  `newsLetterSubscriberId` bigint(20) NOT NULL,
  `newsLetterSubscriberEmail` varchar(50) NOT NULL,
  `delete_status` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `newslettersubscriber`
--

INSERT INTO `newslettersubscriber` (`newsLetterSubscriberId`, `newsLetterSubscriberEmail`, `delete_status`) VALUES
(1, 'keyurmacwana@gmail.com', 0);

-- --------------------------------------------------------

--
-- Table structure for table `owner_tbl`
--

CREATE TABLE `owner_tbl` (
  `vehicleOwnerId` bigint(20) NOT NULL,
  `userName` varchar(25) NOT NULL,
  `password` varchar(25) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 0,
  `mobileNumber` bigint(10) NOT NULL,
  `firstName` varchar(50) NOT NULL,
  `lastName` varchar(50) NOT NULL,
  `street` int(100) NOT NULL,
  `pincode` bigint(20) NOT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT current_timestamp(),
  `adminSignature` bigint(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `city` varchar(30) NOT NULL,
  `state` varchar(30) NOT NULL,
  `profile` varchar(200) NOT NULL,
  `idProofType` varchar(50) NOT NULL,
  `idProofLink` varchar(200) NOT NULL,
  `delete_status` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `owner_tbl`
--

INSERT INTO `owner_tbl` (`vehicleOwnerId`, `userName`, `password`, `is_active`, `mobileNumber`, `firstName`, `lastName`, `street`, `pincode`, `dateCreated`, `adminSignature`, `email`, `city`, `state`, `profile`, `idProofType`, `idProofLink`, `delete_status`) VALUES
(18, 'keyur', '123', 1, 9537371017, 'Keyur', 'Macwana', 0, 0, '2020-02-15 09:00:58', 0, 'keyurmacwana@gmail.com', 'Surat1', 'Gujarat', 'profilePhoto.jpeg', '', '', 0),
(27, 'ankit', '123', 1, 9998896188, 'ankit', 'kalal', 0, 0, '2020-03-11 07:33:00', 0, 'ankitkalal1502@gmail.com', '', '', '5397629220pexels-photo-10434719070683907.jpeg', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `register_tbl`
--

CREATE TABLE `register_tbl` (
  `register_id` bigint(20) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile` bigint(10) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(25) NOT NULL,
  `profile` varchar(2000) NOT NULL,
  `added_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `register_tbl`
--

INSERT INTO `register_tbl` (`register_id`, `full_name`, `email`, `mobile`, `username`, `password`, `profile`, `added_date`) VALUES
(1, 'Keyur Macwana', 'keyurmacwana@gmail.com', 9537371017, 'admin', '1234', '2475796213pexels-photo-2204535271823570.jpeg', '2020-01-13 05:37:39');

-- --------------------------------------------------------

--
-- Table structure for table `ridecurrent`
--

CREATE TABLE `ridecurrent` (
  `currentRideId` bigint(20) NOT NULL,
  `vehicleOwnerId` bigint(20) NOT NULL,
  `customerId` bigint(20) NOT NULL,
  `vehicleId` bigint(20) NOT NULL,
  `startTime` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `initialReading` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `stateId` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `country_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`stateId`, `name`, `country_id`) VALUES
(1, 'ANDHRA PRADESH', 105),
(2, 'ASSAM', 105),
(3, 'ARUNACHAL PRADESH', 105),
(4, 'BIHAR', 105),
(5, 'GUJRAT', 105),
(6, 'HARYANA', 105),
(7, 'HIMACHAL PRADESH', 105),
(8, 'JAMMU & KASHMIR', 105),
(9, 'KARNATAKA', 105),
(10, 'KERALA', 105),
(11, 'MADHYA PRADESH', 105),
(12, 'MAHARASHTRA', 105),
(13, 'MANIPUR', 105),
(14, 'MEGHALAYA', 105),
(15, 'MIZORAM', 105),
(16, 'NAGALAND', 105),
(17, 'ORISSA', 105),
(18, 'PUNJAB', 105),
(19, 'RAJASTHAN', 105),
(20, 'SIKKIM', 105),
(21, 'TAMIL NADU', 105),
(22, 'TRIPURA', 105),
(23, 'UTTAR PRADESH', 105),
(24, 'WEST BENGAL', 105),
(25, 'DELHI', 105),
(26, 'GOA', 105),
(27, 'PONDICHERY', 105),
(28, 'LAKSHDWEEP', 105),
(29, 'DAMAN & DIU', 105),
(30, 'DADRA & NAGAR', 105),
(31, 'CHANDIGARH', 105),
(32, 'ANDAMAN & NICOBAR', 105),
(33, 'UTTARANCHAL', 105),
(34, 'JHARKHAND', 105),
(35, 'CHATTISGARH', 105);

-- --------------------------------------------------------

--
-- Table structure for table `termsandconditions_category`
--

CREATE TABLE `termsandconditions_category` (
  `termCategoryId` bigint(20) NOT NULL,
  `termCategory` varchar(50) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT 1,
  `delete_status` int(11) NOT NULL DEFAULT 0,
  `dateCreated` timestamp NOT NULL DEFAULT current_timestamp(),
  `adminSignature` bigint(20) NOT NULL,
  `is_taken` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `termsandconditions_category`
--

INSERT INTO `termsandconditions_category` (`termCategoryId`, `termCategory`, `is_active`, `delete_status`, `dateCreated`, `adminSignature`, `is_taken`) VALUES
(1, 'Booking', 0, 0, '2020-03-05 09:55:47', 1, 0),
(9, 'Cancelation', 1, 0, '2020-03-05 10:47:02', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `termsandconditions_tbl`
--

CREATE TABLE `termsandconditions_tbl` (
  `termId` bigint(20) NOT NULL,
  `termFor` int(11) NOT NULL,
  `termCategoryId` bigint(20) NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  `termDetail` text NOT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `adminSignature` bigint(20) NOT NULL,
  `is_taken` int(11) NOT NULL DEFAULT 0,
  `delete_status` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `termsandconditions_tbl`
--

INSERT INTO `termsandconditions_tbl` (`termId`, `termFor`, `termCategoryId`, `is_active`, `termDetail`, `dateCreated`, `adminSignature`, `is_taken`, `delete_status`) VALUES
(24, 0, 1, 1, '5', '2020-03-05 10:58:42', 1, 0, 1),
(26, 0, 9, 1, '1', '2020-03-05 10:58:39', 1, 0, 1),
(27, 0, 1, 0, '1', '2020-03-05 10:59:01', 1, 0, 1),
(28, 0, 1, 0, '1', '2020-03-05 10:59:00', 1, 0, 1),
(29, 0, 1, 0, '1', '2020-03-05 11:02:33', 1, 0, 1),
(30, 0, 9, 0, '1', '2020-03-05 11:03:20', 1, 0, 1),
(31, 0, 1, 0, '1', '2020-03-05 11:04:12', 1, 0, 1),
(32, 0, 9, 0, '1', '2020-03-05 11:04:41', 1, 0, 1),
(33, 0, 1, 0, '1', '2020-03-05 11:05:23', 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `vehiclecatagorycar`
--

CREATE TABLE `vehiclecatagorycar` (
  `id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `rate` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vehiclecatagorycar`
--

INSERT INTO `vehiclecatagorycar` (`id`, `type`, `rate`) VALUES
(1, 'hatchback', 0),
(2, 'sedan', 0),
(3, 'SUV', 0),
(4, 'MUV', 0),
(5, 'convertibles', 0),
(6, 'station wagon', 0),
(7, 'coupe', 0),
(8, 'pick-up', 0);

-- --------------------------------------------------------

--
-- Table structure for table `vehiclecatagorymopad`
--

CREATE TABLE `vehiclecatagorymopad` (
  `id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `rate` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vehiclecatagorymopad`
--

INSERT INTO `vehiclecatagorymopad` (`id`, `type`, `rate`) VALUES
(1, 'economical', 0),
(2, 'cruiser', 0),
(3, 'sport', 0),
(4, 'touring', 0),
(5, 'sport touring', 0),
(6, 'dual purpose', 0),
(7, 'scooters', 0);

-- --------------------------------------------------------

--
-- Table structure for table `vehicleholiday_tbl`
--

CREATE TABLE `vehicleholiday_tbl` (
  `vehicleHolidayId` bigint(20) NOT NULL,
  `vehicleId` varchar(50) NOT NULL,
  `startDate` date NOT NULL,
  `endDate` date NOT NULL,
  `totalDays` int(11) NOT NULL,
  `reason` varchar(200) NOT NULL,
  `delete_status` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `vehicleownersignuprequest_tbl`
--

CREATE TABLE `vehicleownersignuprequest_tbl` (
  `vehicleOwnerRequestId` bigint(20) NOT NULL,
  `status` int(2) NOT NULL DEFAULT 0,
  `userName` varchar(25) NOT NULL,
  `firstName` varchar(50) NOT NULL,
  `lastName` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `date_requested` timestamp NOT NULL DEFAULT current_timestamp(),
  `mobileNumber` bigint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vehicleownersignuprequest_tbl`
--

INSERT INTO `vehicleownersignuprequest_tbl` (`vehicleOwnerRequestId`, `status`, `userName`, `firstName`, `lastName`, `email`, `password`, `date_requested`, `mobileNumber`) VALUES
(6, 1, 'ankit', 'ankit', 'kalal', 'ankitkalal0004@gmail.com', '123', '2020-03-11 07:25:36', 0);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_category_tbl`
--

CREATE TABLE `vehicle_category_tbl` (
  `vehicle_category_id` bigint(20) NOT NULL,
  `vehicle_type_id` bigint(20) NOT NULL,
  `vehicle_category` varchar(100) NOT NULL,
  `added_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_active` tinyint(4) NOT NULL DEFAULT 1,
  `delete_status` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vehicle_category_tbl`
--

INSERT INTO `vehicle_category_tbl` (`vehicle_category_id`, `vehicle_type_id`, `vehicle_category`, `added_date`, `is_active`, `delete_status`) VALUES
(33, 1, 'Economical', '2020-03-11 06:21:12', 1, 0),
(34, 1, 'Cruiser', '2020-03-11 06:22:52', 1, 0),
(35, 1, 'Sports', '2020-03-11 06:23:33', 1, 0),
(36, 1, 'Touring', '2020-03-11 06:25:08', 1, 0),
(37, 1, 'Sport Touring', '2020-03-11 06:24:27', 1, 0),
(38, 1, 'Mopad', '2020-03-11 06:25:19', 1, 0),
(39, 2, 'Hatchback', '2020-03-11 06:29:07', 1, 0),
(40, 2, 'Sedan', '2020-03-11 06:29:07', 1, 0),
(41, 2, 'SUV', '2020-03-11 06:29:07', 1, 0),
(42, 2, 'MUV', '2020-03-11 06:29:07', 1, 0),
(43, 2, 'Convertibles', '2020-03-11 06:29:07', 1, 0),
(44, 2, 'Station Wagon', '2020-03-11 06:29:07', 1, 0),
(45, 2, 'Coupe', '2020-03-11 06:29:07', 1, 0),
(46, 2, 'Pick-up', '2020-03-11 06:29:07', 1, 0),
(47, 1, 'Sports', '2020-03-13 11:02:36', 1, 1),
(48, 2, '', '2020-03-13 11:07:27', 1, 1),
(49, 1, 'monster1', '2020-03-17 08:44:58', 0, 1),
(50, 2, 'Electirc', '2020-03-18 10:39:38', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_tbl`
--

CREATE TABLE `vehicle_tbl` (
  `vehicleId` varchar(50) NOT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `owner_id` bigint(20) NOT NULL,
  `vehicle_type_id` bigint(25) NOT NULL,
  `vehicle_category_id` bigint(25) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT 1,
  `delete_status` int(11) NOT NULL DEFAULT 0,
  `licenseNumberPlate` varchar(14) NOT NULL,
  `views` int(11) NOT NULL,
  `rides` int(11) NOT NULL DEFAULT 0,
  `rate` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vehicle_tbl`
--

INSERT INTO `vehicle_tbl` (`vehicleId`, `dateCreated`, `owner_id`, `vehicle_type_id`, `vehicle_category_id`, `is_active`, `delete_status`, `licenseNumberPlate`, `views`, `rides`, `rate`) VALUES
('18GJ-05-LN-2473', '2020-03-20 09:40:49', 18, 2, 43, 1, 0, 'GJ-05-LN-2473', 6, 0, 20),
('18GJ-05-PV-2473', '2020-05-28 18:29:42', 18, 2, 46, 1, 0, 'GJ-05-PV-2473', 6, 0, 10),
('18PN-05-VS-2473', '2020-05-28 19:26:42', 18, 2, 41, 1, 0, 'PN-05-VS-2473', 11, 0, 15),
('27GJ-05-LN-2423', '2020-04-06 09:50:00', 27, 1, 36, 1, 0, 'GJ-05-LN-2423', 2, 0, 11),
('27GJ-05-LN-2473', '2020-05-28 18:50:51', 27, 1, 35, 1, 0, 'GJ-05-LN-2473', 12, 0, 12),
('27RJ-05-QJ-2473', '2020-05-28 16:01:22', 27, 1, 34, 1, 0, 'RJ-05-QJ-2473', 6, 0, 9);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_tbl_address`
--

CREATE TABLE `vehicle_tbl_address` (
  `vehicle_tbl_address_id` bigint(20) NOT NULL,
  `vehicleId` varchar(50) NOT NULL,
  `latitude` varchar(100) NOT NULL,
  `longitude` varchar(100) NOT NULL,
  `street` varchar(100) NOT NULL,
  `city` varchar(30) NOT NULL,
  `state` varchar(30) NOT NULL,
  `door_number` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vehicle_tbl_address`
--

INSERT INTO `vehicle_tbl_address` (`vehicle_tbl_address_id`, `vehicleId`, `latitude`, `longitude`, `street`, `city`, `state`, `door_number`) VALUES
(6, '18GJ-05-LN-2473', '', '', 'Samarth enclave,vip road', 'Surat', 'Gujarat', 'A1-1004'),
(7, '18GJ-05-PV-2473', '', '', 'samarth enclave,vip road', 'Surat', 'Gujarat', 'A1-1004'),
(8, '18PN-05-VS-2473', '', '', 'Samarth enclave,vip road', 'Surat', 'Gujarat', 'A1-1004'),
(9, '27RJ-05-QJ-2473', '', '', 'Radha Nagar Society', 'Surat', 'Gujarat', 'B231'),
(10, '27GJ-05-LN-2473', '', '', 'Radha Nagar Society', 'Surat', 'Gujarat', 'B231'),
(11, '27GJ-05-LN-2423', '', '', 'Radha Nagar Society', 'Surat', 'Gujarat', 'B231');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_tbl_documents`
--

CREATE TABLE `vehicle_tbl_documents` (
  `vehicle_tbl_documents_id` bigint(20) NOT NULL,
  `vehicleId` varchar(50) NOT NULL,
  `photo1_link` varchar(200) NOT NULL,
  `photo2_link` varchar(200) NOT NULL,
  `photo3_link` varchar(200) NOT NULL,
  `rcBook_link` varchar(200) NOT NULL,
  `puc_link` varchar(200) NOT NULL,
  `insurance_link` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vehicle_tbl_documents`
--

INSERT INTO `vehicle_tbl_documents` (`vehicle_tbl_documents_id`, `vehicleId`, `photo1_link`, `photo2_link`, `photo3_link`, `rcBook_link`, `puc_link`, `insurance_link`) VALUES
(0, '18GJ-05-LN-2473', '8660899900miniCouperFront2118818847.jpg', '4298965283miniCouperBack8921140410.jpg', '4281879046miniCouperInterior1270573783.jpg', '9381876762rcBookSample16718434499.jpg', '7511783699pucSample19725992205.jpg', '5186485044insuranceSample18250104178.jpg'),
(0, '18GJ-05-PV-2473', '3672058847hondaRidgelineFront1500567814.jpg', '2006046055hondaRidgelineBack9952730704.jpg', '6274870549hondaRidgelineSide1727013396.jpg', '4340252134rcBookSample13110452607.jpg', '1473316117pucSample11982890246.jpg', '7533630096insuranceSample14215192413.jpg'),
(0, '18PN-05-VS-2473', '1188002547renaultTriberFront4793315820.jpg', '2958175277renaultTriberReal1259857368.jpg', '9117291510renaultTriberTop7251916745.jpg', '7306898042rcBookSample13442431907.jpg', '2981479170pucSample12043785728.jpg', '1938714996insuranceSample19362489346.jpg'),
(0, '27RJ-05-QJ-2473', '4719864993bajajAvengerSide7320041173.jpg', '', '', '8174612739rcBookSample14020887292.jpg', '4888796899pucSample11925261061.jpg', '1217673739insuranceSample14786441313.jpg'),
(0, '27GJ-05-LN-2473', '2544108834benelli1306031132.webp', '', '', '6423318979rcBookSample11581670671.jpg', '1721486442pucSample18304564444.jpg', '4085591745insuranceSample17654143874.jpg'),
(0, '27GJ-05-LN-2423', '6557794629royalEnfieldHimalayan8845502336.jpg', '', '', '5546011657rcBookSample13271187743.jpg', '1431285577pucSample12141507462.jpg', '4824316490insuranceSample11538419469.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_tbl_features`
--

CREATE TABLE `vehicle_tbl_features` (
  `vehicle_tbl_features_id` bigint(20) NOT NULL,
  `vehicleId` varchar(50) NOT NULL,
  `fuelType_id` bigint(20) NOT NULL,
  `approxKM` int(11) NOT NULL,
  `seats` int(11) NOT NULL,
  `transmission` int(11) NOT NULL,
  `abs` int(11) NOT NULL,
  `overheadCarrier` int(11) NOT NULL,
  `self_driving` int(11) NOT NULL,
  `luggageCapacity` int(11) NOT NULL,
  `cruise_control` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vehicle_tbl_features`
--

INSERT INTO `vehicle_tbl_features` (`vehicle_tbl_features_id`, `vehicleId`, `fuelType_id`, `approxKM`, `seats`, `transmission`, `abs`, `overheadCarrier`, `self_driving`, `luggageCapacity`, `cruise_control`) VALUES
(6, '18GJ-05-LN-2473', 2, 0, 5, 1, 1, 0, 0, 3, 1),
(7, '18GJ-05-PV-2473', 2, 0, 2, 0, 0, 0, 0, 5, 0),
(8, '18PN-05-VS-2473', 5, 0, 5, 1, 1, 0, 0, 3, 1),
(9, '27RJ-05-QJ-2473', 4, 0, 2, 0, 1, 0, 0, 0, 0),
(10, '27GJ-05-LN-2473', 1, 0, 2, 0, 1, 0, 0, 0, 0),
(11, '27GJ-05-LN-2423', 5, 0, 2, 0, 1, 0, 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_tbl_model`
--

CREATE TABLE `vehicle_tbl_model` (
  `vehicle_tbl_model_id` bigint(20) NOT NULL,
  `vehicleId` varchar(50) NOT NULL,
  `companyName` varchar(25) NOT NULL,
  `modelName` varchar(25) NOT NULL,
  `modelNumber` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vehicle_tbl_model`
--

INSERT INTO `vehicle_tbl_model` (`vehicle_tbl_model_id`, `vehicleId`, `companyName`, `modelName`, `modelNumber`) VALUES
(6, '18GJ-05-LN-2473', 'Mini', 'Cooper', '2018'),
(7, '18GJ-05-PV-2473', 'Honda', 'Ridgliner', 'sf205'),
(8, '18PN-05-VS-2473', 'Renault', 'Tribler', 'f225'),
(9, '27RJ-05-QJ-2473', 'Bajaj', 'Avenger', '2019'),
(10, '27GJ-05-LN-2473', 'Benelli', 'TNT', '23'),
(11, '27GJ-05-LN-2423', 'RoyalEnfield', 'Himalayan', 'sf205');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_tbl_schedule`
--

CREATE TABLE `vehicle_tbl_schedule` (
  `vehicle_tbl_schedule_id` bigint(20) NOT NULL,
  `vehicleId` varchar(50) NOT NULL,
  `weekdayStart` time NOT NULL,
  `weekdayEnd` time NOT NULL,
  `weekendStart` time NOT NULL,
  `weekendEnd` time NOT NULL,
  `multiDayRenting` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vehicle_tbl_schedule`
--

INSERT INTO `vehicle_tbl_schedule` (`vehicle_tbl_schedule_id`, `vehicleId`, `weekdayStart`, `weekdayEnd`, `weekendStart`, `weekendEnd`, `multiDayRenting`) VALUES
(6, '18GJ-05-LN-2473', '07:00:00', '19:00:00', '07:00:00', '00:00:00', 4),
(7, '18GJ-05-PV-2473', '07:00:00', '19:00:00', '07:00:00', '00:00:00', 5),
(8, '18PN-05-VS-2473', '07:00:00', '19:00:00', '07:00:00', '00:00:00', 1),
(9, '27RJ-05-QJ-2473', '07:00:00', '19:00:00', '07:00:00', '00:00:00', 6),
(10, '27GJ-05-LN-2473', '07:00:00', '19:00:00', '07:00:00', '00:00:00', 1),
(11, '27GJ-05-LN-2423', '07:00:00', '19:00:00', '07:00:00', '00:00:00', 7);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_type_tbl`
--

CREATE TABLE `vehicle_type_tbl` (
  `vehicle_type_id` bigint(20) NOT NULL,
  `vehicle_type` varchar(100) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT 0,
  `added_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `active_date` timestamp NULL DEFAULT NULL,
  `delete_status` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vehicle_type_tbl`
--

INSERT INTO `vehicle_type_tbl` (`vehicle_type_id`, `vehicle_type`, `is_active`, `added_date`, `active_date`, `delete_status`) VALUES
(1, 'TWO WHEELER', 1, '2020-01-25 07:48:33', '0000-00-00 00:00:00', 0),
(2, 'FOUR WHEELER', 1, '2020-01-25 08:09:48', '0000-00-00 00:00:00', 0),
(21, 'TRUCK', 1, '2020-02-25 09:33:28', '0000-00-00 00:00:00', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contactus_tbl`
--
ALTER TABLE `contactus_tbl`
  ADD PRIMARY KEY (`contactus_id`);

--
-- Indexes for table `customer_tbl`
--
ALTER TABLE `customer_tbl`
  ADD PRIMARY KEY (`customerId`),
  ADD UNIQUE KEY `mobile` (`mobile`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `fueltype_tbl`
--
ALTER TABLE `fueltype_tbl`
  ADD PRIMARY KEY (`fuelType_id`);

--
-- Indexes for table `home_page_banner`
--
ALTER TABLE `home_page_banner`
  ADD PRIMARY KEY (`bannerId`);

--
-- Indexes for table `newbooking`
--
ALTER TABLE `newbooking`
  ADD PRIMARY KEY (`booking_id`);

--
-- Indexes for table `newslettersubscriber`
--
ALTER TABLE `newslettersubscriber`
  ADD PRIMARY KEY (`newsLetterSubscriberId`);

--
-- Indexes for table `owner_tbl`
--
ALTER TABLE `owner_tbl`
  ADD PRIMARY KEY (`vehicleOwnerId`);

--
-- Indexes for table `register_tbl`
--
ALTER TABLE `register_tbl`
  ADD PRIMARY KEY (`register_id`);

--
-- Indexes for table `ridecurrent`
--
ALTER TABLE `ridecurrent`
  ADD PRIMARY KEY (`currentRideId`);

--
-- Indexes for table `termsandconditions_category`
--
ALTER TABLE `termsandconditions_category`
  ADD PRIMARY KEY (`termCategoryId`);

--
-- Indexes for table `termsandconditions_tbl`
--
ALTER TABLE `termsandconditions_tbl`
  ADD PRIMARY KEY (`termId`);

--
-- Indexes for table `vehiclecatagorycar`
--
ALTER TABLE `vehiclecatagorycar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehiclecatagorymopad`
--
ALTER TABLE `vehiclecatagorymopad`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicleholiday_tbl`
--
ALTER TABLE `vehicleholiday_tbl`
  ADD PRIMARY KEY (`vehicleHolidayId`),
  ADD KEY `vehicleholiday_tbl_ibfk_1` (`vehicleId`);

--
-- Indexes for table `vehicleownersignuprequest_tbl`
--
ALTER TABLE `vehicleownersignuprequest_tbl`
  ADD PRIMARY KEY (`vehicleOwnerRequestId`);

--
-- Indexes for table `vehicle_category_tbl`
--
ALTER TABLE `vehicle_category_tbl`
  ADD PRIMARY KEY (`vehicle_category_id`),
  ADD KEY `vehicle_type_id` (`vehicle_type_id`);

--
-- Indexes for table `vehicle_tbl`
--
ALTER TABLE `vehicle_tbl`
  ADD PRIMARY KEY (`vehicleId`),
  ADD UNIQUE KEY `vehicleId` (`vehicleId`);

--
-- Indexes for table `vehicle_tbl_address`
--
ALTER TABLE `vehicle_tbl_address`
  ADD PRIMARY KEY (`vehicle_tbl_address_id`),
  ADD KEY `vehicle_tbl_address_ibfk_1` (`vehicleId`);

--
-- Indexes for table `vehicle_tbl_documents`
--
ALTER TABLE `vehicle_tbl_documents`
  ADD KEY `vehicle_tbl_documents_ibfk_1` (`vehicleId`);

--
-- Indexes for table `vehicle_tbl_features`
--
ALTER TABLE `vehicle_tbl_features`
  ADD PRIMARY KEY (`vehicle_tbl_features_id`),
  ADD KEY `vehicle_tbl_features_ibfk_1` (`vehicleId`);

--
-- Indexes for table `vehicle_tbl_model`
--
ALTER TABLE `vehicle_tbl_model`
  ADD PRIMARY KEY (`vehicle_tbl_model_id`),
  ADD KEY `vehicle_tbl_model_ibfk_1` (`vehicleId`);

--
-- Indexes for table `vehicle_tbl_schedule`
--
ALTER TABLE `vehicle_tbl_schedule`
  ADD PRIMARY KEY (`vehicle_tbl_schedule_id`),
  ADD KEY `vehicle_tbl_schedule_ibfk_1` (`vehicleId`);

--
-- Indexes for table `vehicle_type_tbl`
--
ALTER TABLE `vehicle_type_tbl`
  ADD PRIMARY KEY (`vehicle_type_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contactus_tbl`
--
ALTER TABLE `contactus_tbl`
  MODIFY `contactus_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `customer_tbl`
--
ALTER TABLE `customer_tbl`
  MODIFY `customerId` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `fueltype_tbl`
--
ALTER TABLE `fueltype_tbl`
  MODIFY `fuelType_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `home_page_banner`
--
ALTER TABLE `home_page_banner`
  MODIFY `bannerId` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `newbooking`
--
ALTER TABLE `newbooking`
  MODIFY `booking_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `newslettersubscriber`
--
ALTER TABLE `newslettersubscriber`
  MODIFY `newsLetterSubscriberId` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `owner_tbl`
--
ALTER TABLE `owner_tbl`
  MODIFY `vehicleOwnerId` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `register_tbl`
--
ALTER TABLE `register_tbl`
  MODIFY `register_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ridecurrent`
--
ALTER TABLE `ridecurrent`
  MODIFY `currentRideId` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `termsandconditions_category`
--
ALTER TABLE `termsandconditions_category`
  MODIFY `termCategoryId` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `termsandconditions_tbl`
--
ALTER TABLE `termsandconditions_tbl`
  MODIFY `termId` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `vehiclecatagorycar`
--
ALTER TABLE `vehiclecatagorycar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `vehiclecatagorymopad`
--
ALTER TABLE `vehiclecatagorymopad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `vehicleholiday_tbl`
--
ALTER TABLE `vehicleholiday_tbl`
  MODIFY `vehicleHolidayId` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `vehicleownersignuprequest_tbl`
--
ALTER TABLE `vehicleownersignuprequest_tbl`
  MODIFY `vehicleOwnerRequestId` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `vehicle_category_tbl`
--
ALTER TABLE `vehicle_category_tbl`
  MODIFY `vehicle_category_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `vehicle_tbl_address`
--
ALTER TABLE `vehicle_tbl_address`
  MODIFY `vehicle_tbl_address_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `vehicle_tbl_features`
--
ALTER TABLE `vehicle_tbl_features`
  MODIFY `vehicle_tbl_features_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `vehicle_tbl_model`
--
ALTER TABLE `vehicle_tbl_model`
  MODIFY `vehicle_tbl_model_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `vehicle_tbl_schedule`
--
ALTER TABLE `vehicle_tbl_schedule`
  MODIFY `vehicle_tbl_schedule_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `vehicle_type_tbl`
--
ALTER TABLE `vehicle_type_tbl`
  MODIFY `vehicle_type_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `vehicleholiday_tbl`
--
ALTER TABLE `vehicleholiday_tbl`
  ADD CONSTRAINT `vehicleholiday_tbl_ibfk_1` FOREIGN KEY (`vehicleId`) REFERENCES `vehicle_tbl` (`vehicleId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `vehicle_tbl_address`
--
ALTER TABLE `vehicle_tbl_address`
  ADD CONSTRAINT `vehicle_tbl_address_ibfk_1` FOREIGN KEY (`vehicleId`) REFERENCES `vehicle_tbl` (`vehicleId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `vehicle_tbl_documents`
--
ALTER TABLE `vehicle_tbl_documents`
  ADD CONSTRAINT `vehicle_tbl_documents_ibfk_1` FOREIGN KEY (`vehicleId`) REFERENCES `vehicle_tbl` (`vehicleId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `vehicle_tbl_features`
--
ALTER TABLE `vehicle_tbl_features`
  ADD CONSTRAINT `vehicle_tbl_features_ibfk_1` FOREIGN KEY (`vehicleId`) REFERENCES `vehicle_tbl` (`vehicleId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `vehicle_tbl_model`
--
ALTER TABLE `vehicle_tbl_model`
  ADD CONSTRAINT `vehicle_tbl_model_ibfk_1` FOREIGN KEY (`vehicleId`) REFERENCES `vehicle_tbl` (`vehicleId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `vehicle_tbl_schedule`
--
ALTER TABLE `vehicle_tbl_schedule`
  ADD CONSTRAINT `vehicle_tbl_schedule_ibfk_1` FOREIGN KEY (`vehicleId`) REFERENCES `vehicle_tbl` (`vehicleId`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
