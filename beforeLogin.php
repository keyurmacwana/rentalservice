<div class="header-top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="top-links">
                            <li><i class="flaticon-support"></i><a href="contactus.php" title="">Contact Us</a></li>
                            <li><i class="flaticon-info"></i><a href="faq.php" title="">Faq's</a></li>
                            <li><i class="fa fa-car"></i><a href="./vehicleOwner/vehicleOwnerSignup.php" title="">Become an Owner</a></li>
                        </ul>
                        <div class="login-area">
                            <ul>
                                <li class="log-in"><a href="login.php" title="Login"><i class="flaticon-unlocked"></i> login</a></li>
                                <li class="Register"><a href="register.php" title="Register"><i class="flaticon-checked"></i> register</a></li>
                               
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- topbar -->