<?php 
	include_once('header.php');
	$Prefrence = explode(',',$data['vehiclePrefrence']);


      //date of birth date to string
      $date = $data['dateOfBirth'];

      //echo "$date"; echo "<br>";
      $dateToPrint = date('j m Y', strtotime($date) ) ;
     // echo "$dateToPrint"; echo "<br>";
      $day = substr($dateToPrint, 0, 2);
      $month = substr($dateToPrint, 2,3);
      $year = substr($dateToPrint, 5, 10);


      $date1 = $data['issueDate'];
     //echo "$date1"; echo "<br>";
      $dateToPrint1 = date('j m Y', strtotime($date1) ) ;
     // echo "$dateToPrint1"; echo "<br>";
      $day1 = substr($dateToPrint1, 0, 2);
      $month1 = substr($dateToPrint1, 2,3);
      $year1 = substr($dateToPrint1, 5, 10);

      
      $date2 = $data['expirationDate'];
    //  echo "$date2"; echo "<br>";
      $dateToPrint2 = date('j m Y', strtotime($date2) ) ;
    //  echo "$dateToPrint2"; echo "<br>";
      $day2 = substr($dateToPrint2, 0, 2);
      $month2 = substr($dateToPrint2, 2,3);
      $year2 = substr($dateToPrint2, 5, 10);
      
     
   


?>

<div class="sub-head blackish ext-low-opacity">
			<div class="bg-image" style="background-image: url(images/resources/subhead-bg.jpg)"></div>
        	<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="cola-head">
							<div class="head-meta">
								<h4><?php echo $data['fullName'];?></h4>
								
							</div>
							<div class="info-links">
								<a href="how-this-work.php" style="color:#FF0000;"title="">how does this work</a>
								<a href="../vehicleOwner/vehicleOwnerSignup.php" title="">submit your car</a>
								<span><i class="fa fa-phone"></i>call <?php echo $data['mobile'];?></span>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div><!-- subhead -->
        
        <section>
        	<div class="space">
        		<div class="container">
        			<div class="row">
                    	<div class="col-md-12">
                        	<div class="profile-edit">
                            	<div class="row">
                                	<div class="col-lg-3">
                                    	<div class="profile-dp">
                                        	<figure><img src="images/resources/addImage.png" alt=""></figure>
                                            <div class="profile-info">
												<span class="profile-name"><?php echo $data['fullName'];?></span>
												
											</div>
<!--                                            <ul class="verification">
                                            	<li><i class="flaticon-world"></i>Online on 4 Oct, 2017 at 08:36</li>
                                                <li><i class="flaticon-tick"></i>Account connected on Facebook</li>
                                                <li><i class="flaticon-tick"></i>Phone number verified</li>
                                            </ul>
-->
                                           
                                        </div>
                                    </div>
                                    <div class="col-lg-9 col-md-12">
                                    	<div class="edit-profile">
                                        	<form action="dbclass.php" method="post" class="cola-form" enctype="multipart/form-data">
                                            	<div class="row">
                                                	<div class="col-md-8">
                                                    	<label>Full Name ( as on driving license):</label>
                                                		<input type="text" name="fullname" id="fullname"placeholder="<?php echo $data['fullName'];?>">
                                                    </div>
                                                   <div class="col-lg-4 col-md-12">
                                                    	<label>DATE OF BIRTH:</label>
                                                        <div class="row">
                                                    	<div class="col-md-4">
                                                            <select name="DOB_D">
                                                                <option data-display="D">D</option>
                                                               <?php
																	$cont = 1;
																	while($cont < 32)
																	{
																		
                                                                        if($day == $cont)
                                                                        {
                                                                            echo "<option 
                                                                                  value='".$cont."' selected> 
                                                                                $cont ";
                                                                        }

                                                                        else
                                                                        {
                                                                            echo "<option> $cont ";
                                                                        }

                                                                        
                                                                            echo "</option>";
                                                                        
																		$cont++;
																	}
																?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <select name="DOB_M">
                                                                <option data-display="M">M</option>
                                                               <?php
																	$cont = 1;
																	while($cont < 13)
																	{
																		if($month == $cont)
                                                                        {
                                                                            echo "<option 
                                                                                  value='".$cont."' selected> 
                                                                                $cont ";
                                                                        }

                                                                        else
                                                                        {
                                                                            echo "<option> $cont ";
                                                                        }

                                                                        
                                                                            echo "</option>";
                                                                        
                                                                        $cont++;
																	}
																?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <select name="DOB_Y">
                                                                <option data-display="Y">Y</option>
                                                               <?php
																	$cont = 1992;
																	while($cont < 2003)
																	{
																		if($year == $cont)
                                                                        {
                                                                            echo "<option 
                                                                                  value='".$cont."' selected> 
                                                                                $cont ";
                                                                        }

                                                                        else
                                                                        {
                                                                            echo "<option> $cont ";
                                                                        }

                                                                        
                                                                            echo "</option>";
                                                                        
                                                                        $cont++;
																	}
																?>
                                                            </select>
                                                        </div>
                                                        </div>
                                                    </div>
												</div>
												<div class="row">
                                                   
                                                    <div class="col-md-8">
                                                    	<label>Street & number:</label>
                                                		<input type="text" name="street" id="street" 
														  value="<?php echo $data['street'];?>">
                                                    </div>
                                                    <div class="col-md-2">
                                                    	<label>city:</label>
                                                        <input type="text" name="c_city" id="c_city" 
														 value="<?php echo $data['city'];?>">
                                                    </div>
													<div class="col-md-2">
                                                    	<label>Postal Code:</label>
                                                        <input type="text" name="c_postalCode" id="c_postalCode" 
														  value="<?php echo $data['pinCode'];?>">                                                    </div>
												</div>
												<div class="row">
                                                    <div class="col-md-4">
                                                    	<label>Mobile: <a href="#" title="" class="update-nmbr">update</a></label>
                                                		<input type="text" name="mobile" id="mobile"
                                                         value="<?php echo $data['mobile'];?>" >
                                                    </div>
                                                    <div class="col-md-4">
                                                    	<label>Email:<a href="#" title="" class="update-nmbr">update</a></label>
                                                		<input type="text" name="email" id="email" value="<?php echo $data['email'];?>" >
                                                    </div>
                                                    <div class="col-md-4">
                                                    	<label>Number driving license: <i class="fa fa-info-circle"></i></label>
                                                		<input type="text" name="c_licenseNo" id="c_licenseNo" 
														  value="<?php echo $data['licenseNumber'];?>">

                                                    </div>
												</div>
												<div class="row">
                                                    <div class="col-md-4">
                                                    	<div class="dp-edit">
                                                        	<span>Upload Driving License:</span>
                                       
                                   
                                        
                                      	<?php
											if($data['licenseLink'] == "")
											{
										?>
												<img src="images/resources/drivingLicnseStock.png" id="realimage" onclick="triggerclick()" >
										<?php
											}
											
											else
											{
										?>
												<img src="./customerImage/<?php echo $data['licenseLink'];?>" id="realimage" height="100px"
										  width="100px" onclick="triggerclick()" align="center">
										<?php
											
											}
										?>
										
										
                                        <input type="file" name="imgUpload" id="imgUpload" onchange="displayimage(this)" style="display: none">
                                          </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-12">
                                                    	<label>driver's license issuance date:</label>
                                                        <div class="row">
                                                    	<div class="col-md-4">
                                                            <select name="ISSUE_D">
                                                                <option data-display="D">D</option>
                                                               <?php
																	$cont = 1;
																	while($cont < 32)
																	{
																		  if($day1 == $cont)
                                                                        {
                                                                            echo "<option 
                                                                                  value='".$cont."' selected> 
                                                                                $cont ";
                                                                        }

                                                                        else
                                                                        {
                                                                            echo "<option> $cont ";
                                                                        }

                                                                        
                                                                            echo "</option>";
                                                                        
                                                                        $cont++;
																	}
																?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <select name="ISSUE_M">
                                                                <option data-display="M">M</option>
                                                               <?php
																	$cont = 1;
																	while($cont < 13)
																	{
																		  if($month1 == $cont)
                                                                        {
                                                                            echo "<option 
                                                                                  value='".$cont."' selected> 
                                                                                $cont ";
                                                                        }

                                                                        else
                                                                        {
                                                                            echo "<option> $cont ";
                                                                        }

                                                                        
                                                                            echo "</option>";
                                                                        
                                                                        $cont++;
																	}
																?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <select name="ISSUE_Y">
                                                                <option data-display="Y">Y</option>
                                                               <?php
																	$cont = 1992;
																	while($cont < 2003)
																	{
																		  if($year1 == $cont)
                                                                        {
                                                                            echo "<option 
                                                                                  value='".$cont."' selected> 
                                                                                $cont ";
                                                                        }

                                                                        else
                                                                        {
                                                                            echo "<option> $cont ";
                                                                        }

                                                                        
                                                                            echo "</option>";
                                                                        
                                                                        $cont++;
																	}
																?>
                                                            </select>
                                                        </div>
                                                        </div>
                                                    </div>
													 
                                                    <div class="col-lg-4 col-md-12">
                                                    	<label>driver's license Expiry date</label>
                                                        <div class="row">
                                                    	<div class="col-md-4">
                                                            <select name="Expiry_D">
                                                            	<option data-display="D" >D</option>
                                                               
																<?php
																	$cont = 1;
																	while($cont < 32)
																	{
																		  if($day2 == $cont)
                                                                        {
                                                                            echo "<option 
                                                                                  value='".$cont."' selected> 
                                                                                $cont ";
                                                                        }

                                                                        else
                                                                        {
                                                                            echo "<option> $cont ";
                                                                        }

                                                                        
                                                                            echo "</option>";
                                                                        
                                                                        $cont++;
																	}
																?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <select name="Expiry_M">
                                                                <option data-display="M">M</option>
                                                                <?php
																	$cont = 1;
																	while($cont < 13)
																	{
																		  if($month2 == $cont)
                                                                        {
                                                                            echo "<option 
                                                                                  value='".$cont."' selected> 
                                                                                $cont ";
                                                                        }

                                                                        else
                                                                        {
                                                                            echo "<option> $cont ";
                                                                        }

                                                                        
                                                                            echo "</option>";
                                                                        
                                                                        $cont++;
																	}
																?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <select name="Expiry_Y">
                                                                <option data-display="Y">Y</option>
                                                               <?php
																	$cont = 1992;
																	while($cont < 2003)
																	{
																		  if($year2 == $cont)
                                                                        {
                                                                            echo "<option 
                                                                                  value='".$cont."' selected> 
                                                                                $cont ";
                                                                        }

                                                                        else
                                                                        {
                                                                            echo "<option> $cont ";
                                                                        }

                                                                        
                                                                            echo "</option>";
                                                                        
                                                                        $cont++;
																	}
																?>
                                                            </select>
                                                        </div>
                                                        </div>
                                                    </div>
													<div class="col-md-4">
                                                    		<div class="notification-rec">
                                                        	<span>Vehicle Prefrence:</span>
                                                            	<?php
																	$sql = "SELECT * FROM vehicle_type_tbl WHERE is_active = '1' AND delete_status = '0'";
																	$ans = mysqli_query($obj->con,$sql);
																	
																	while($row = mysqli_fetch_array($ans))
																	{
																?>
                                                            	<p>
                                                               <input type="checkbox" name="Prefrence[]" 
															   value="<?php echo $row['vehicle_type'];?>"
															   <?php
															   foreach($Prefrence as $value){
																	if($row['vehicle_type'] == $value)
																	{
																		echo "checked";
																	}
																}
															   ?>
															   >
                                                                    <label></label>
                                                                    <?php echo $row['vehicle_type'];?>
                                                                </p>
                                                                <?php
																	}
																?>
                                                                
                                                        </div>
													
                                                    </div>
												</div>
                                                <div class="row">   
                                                    <button type="submit" name="save_change" id="save_change"
													              class="theme-btn long">save change</button>
												     <input type="hidden" id="customerId" name="customerId" value="<?php echo $data['customerId'];?>"> 		  
                                                </div>
                                            </form>
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
        		</div>
        	</div>
        </section><!-- points section -->
		
		 <script type="text/javascript">
        function triggerclick(){
            document.querySelector('#imgUpload').click();
        }
        function displayimage(e){
            if(e.files[0]){
                var reader = new FileReader();
                reader.onload = function(e){
                    document.querySelector('#realimage').setAttribute('src', e.target.result);
                }
                reader.readAsDataURL(e.files[0]);
            }
        }
        </script>

        <script type="text/javascript">
        function triggerclickmulti(){
            document.querySelector('#gallery-photo-add').click();
        }
        </script>
        <script type="text/javascript">
            function  change_subcategory(){
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.open("GET","ajax.php?subcat=" + document.getElementById("category").value,false);
                xmlhttp.send(null);
                document.getElementById("psize").innerHTML=xmlhttp.responseText;
            }
        </script>
<?php
	include_once('footer.php');
?>