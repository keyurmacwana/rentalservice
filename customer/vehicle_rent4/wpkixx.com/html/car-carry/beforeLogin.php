<div class="header-top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="top-links">
                            <li><i class="flaticon-support"></i><a href="support.html" title="">support center</a></li>
                            <li><i class="flaticon-info"></i><a href="faq.html" title="">Faq's</a></li>
                        </ul>
                        <div class="login-area">
                            <ul>
                                <li class="log-in"><a href="login.php" title="Login"><i class="flaticon-unlocked"></i> login</a></li>
                                <li class="Register"><a href="register.html" title="Register"><i class="flaticon-checked"></i> register</a></li>
                                <li class="post"><a href="#" title="">+post</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- topbar -->