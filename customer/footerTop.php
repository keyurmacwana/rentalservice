<section>
			<div class="space no-space dark-bg">
				<div class="container">
					<div class="row">
						<div class="col-lg-4 col-sm-4">
							<div class="servize">
								<i class="flaticon-placeholder"></i>
								<div class="serviz-meta">
									<span>E-Space,Vesu,Surat</span>
									<i>India</i>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-sm-4">
							<div class="servize">
								<i class="flaticon-24-hours-1"></i>
								<div class="serviz-meta">
									<span>Tool free number 24/7</span>
									<i>+91-997-927-4670</i>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-sm-4">
							<div class="servize">
								<i class="flaticon-24-hours"></i>
								<div class="serviz-meta">
									<span>Full time services</span>
									<i>24/7</i>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section><!-- footer top -->