<?php
	include_once('header.php');
?>

<div class="sub-head blackish ext-low-opacity">
			<div class="bg-image" style="background-image: url(images/resources/subhead-bg.jpg)"></div>
        	<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="cola-head">
							<div class="head-meta">
								<h4>Register</h4>
								<p>( Create an account today. )</p>
							</div>
							
						</div>
					</div>
				</div>
			</div>
        </div><!-- subhead -->
        
        <section>
        	<div class="space">
        		<div class="container">
        			<div class="row">
        				<div class="col-md-12">
                        	<div class="login-sec">
                            	<div class="row">
                                	<div class="col-md-4">
                                    	<div class="log-with-fb">
                                        	<h4>Login in seconds:</h4>
                                            <a title="" href="#" class="facebook"><i class="fa fa-facebook"></i>facebook</a>
											<a title="" href="#" class="google"><i class="fa fa-google-plus"></i>Google</a>
											<a title="" href="#" class="twitter"><i class="fa fa-twitter"></i>Twitter</a>
                                        </div>
                                    </div>
                                    <div class="offset-lg-1 col-lg-7 col-md-8">
                                    	<div class="cola-form">
                                        	<h4><i class="flaticon-sign-in"></i>Register:</h4>
                                            <form method="post" action="dbclass.php">
                                            	<label>User Name:</label>
                                                <input type="text" id="userName" name="userName">
                                            	<label>first Name:</label>
                                                <input type="text" id="firstName" name="firstName">
                                                <label>last name:</label>
                                                <input type="text" id="lastName" name="lastName">
                                                <label>email:</label>
                                                <input type="text" id="email" name="email">
                                                <label>Mobile:</label>
                                                <input type="text" id="mobile" name="mobile">
                                                <label>password:</label>
                                                <input type="password" id="password" name="password" >
                                                <label>confirm password:</label>
                                                <input type="password" id="cndPwd" name="cnfPwd" >
                                                <input type="checkbox" id="collabors" name="collabors" value="1">
                                                <label for="checkbox">I accept Collabors’s <a href="#" title="">Terms & Conditions</a></label>
                                                <button type="submit" class="theme-btn" id="register_btn" name="register_btn">register</button>
                                            </form>
                                            <span>Are you a member already? <a href="login.php" title="">Login here</a></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
        			</div>
        		</div>
        	</div>
        </section><!-- form section -->
        
        

<?php
	include_once('footer.php');
?>