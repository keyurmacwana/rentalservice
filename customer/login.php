<?php
	include_once('header.php');
?>
        
		<div class="sub-head blackish ext-low-opacity">
			<div class="bg-image" style="background-image: url(images/resources/subhead-bg.jpg)"></div>
        	<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="cola-head">
							<div class="head-meta">
								<h4>Login</h4>
								<p>( Login to your account. )</p>
							</div>
							
							<div class="info-links">
								<a href="#" title="">how does this work</a>
								<a href="#" title="">submit your car</a>
								<span><i class="fa fa-phone"></i>call (012) 345 - 6789</span>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div><!-- subhead -->
        
        <section>
        	<div class="space">
        		<div class="container">
        			<div class="row">
        				<div class="col-md-12">
                        	<div class="login-sec">
                            	<div class="row">
                                	<div class="col-md-4">
                                    	<div class="log-with-fb">
                                        	<h4>Login in seconds:</h4>
                                            <a class="facebook" href="#" title=""><i class="fa fa-facebook"></i>facebook</a>
											<a class="google" href="#" title=""><i class="fa fa-google-plus"></i>Google</a>
											<a class="twitter" href="#" title=""><i class="fa fa-twitter"></i>Twitter</a>
                                        </div>
                                    </div>
                                    <div class="offset-md-1 col-md-7">
                                    	<div class="cola-form">
                                        	<h4><i class="flaticon-key"></i>Login:</h4>
                                            <form method="post">
                                            	<label>Email</label>
                                                <input type="text" placeholder="Your email address">
                                                <label>password</label>
                                                <input type="password" placeholder="Password">
                                                <input type="checkbox">
                                                <label for="checkbox">Remember me</label>
                                                <button type="submit" id="checkbox" class="theme-btn">login</button>
                                            </form>
                                            <span>Aren’t you a member yet? <a href="register.php" title="">Register here</a></span>
                                            <span>Forgot your password? <a href="#" title="">Click here</a></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
        			</div>
        		</div>
        	</div>
        </section><!-- form section -->
        
        <section>
			<div class="space no-space dark-bg">
				<div class="container">
					<div class="row">
						<div class="col-lg-4 col-sm-4">
							<div class="servize">
								<i class="flaticon-placeholder"></i>
								<div class="serviz-meta">
									<span>Tucson, AZ 80210: 501 Lane</span>
									<i>Canada</i>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-sm-4">
							<div class="servize">
								<i class="flaticon-24-hours-1"></i>
								<div class="serviz-meta">
									<span>Tool free number 24/7</span>
									<i>+1-111-222-333</i>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-sm-4">
							<div class="servize">
								<i class="flaticon-24-hours"></i>
								<div class="serviz-meta">
									<span>Full time services</span>
									<i>24/7</i>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section><!-- footer top -->
        
<?php
	include_once('footer.php');
?>