<?php 
	
	include_once('header.php');
?>
<style>
	.myButton{
	  background-color: #4CAF50; /* Green */
	  border: 2px solid #4CAF50;;
	  color: white;
	  padding: 16px 32px;
	  text-align: center;
	  text-decoration: none;
	  display: inline-block;
	  font-size: 16px;
	  margin: 4px 2px;
	  transition-duration: 0.4s;
	  cursor: pointer;		
	  border-radius: 8px;										
	}
	
	.myButton:hover {
	  background-color: white; /* Green */
	  border:  2px solid #4CAF50;
	  color: black;
	  padding: 16px 32px;
	  text-align: center;
	  text-decoration: none;
	  display: inline-block;
	  font-size: 16px;
	  margin: 4px 2px;
	  transition-duration: 0.4s;
	  cursor: pointer;		
	  border-radius: 8px;
	}
</style>

<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>


<?php
//BEGIN FETCH index page random vehicles ORDER BY RAND()
	if(!isset($_GET['details']))
	{
	
		$sql = "SELECT vt.*,vct.*,vtb.*,vtd.*,vtm.*,vta.*,vts.* ,vtf.*
															
			FROM  vehicle_tbl vt 
			
			LEFT JOIN vehicle_category_tbl vct
			ON vt.vehicle_category_id = vct.vehicle_category_id 
			
			LEFT JOIN vehicle_type_tbl vtb
			ON vt.vehicle_type_id = vtb.vehicle_type_id 
			
			LEFT JOIN vehicle_tbl_documents vtd
			ON vt.vehicleId = vtd.vehicleId

			LEFT JOIN vehicle_tbl_model vtm 
			ON vt.vehicleId = vtm.vehicleId

			LEFT JOIN vehicle_tbl_schedule vts
			ON vt.vehicleId = vts.vehicleId 
			
			LEFT JOIN vehicle_tbl_features vtf
			ON vt.vehicleId = vtf.vehicleId 
			
			LEFT JOIN vehicle_tbl_address vta
			ON vt.vehicleId = vta.vehicleId 
						
			WHERE vt.delete_status = 0 ";
			
			if(isset($_GET['vc_id']))
			{
				$sql = $sql."AND vt.vehicle_category_id = '".$_GET['vc_id']."'";
			}
			
			if(isset($_GET['vt_id']))
			{
				$sql = $sql."AND vt.vehicle_type_id = '".$_GET['vt_id']."'";
			}
			
			$sql = $sql."ORDER BY rand()";
	}
	
	
	if(isset($_GET['details']))
	{
		
		error_reporting(0);
		$date1 = date('d-m-Y',strtotime($_POST['datetimepicker1']));

		$date2 = date('d-m-Y',strtotime($_POST['datetimepicker3']));
		
		$interval = $date2 - $date1;
		
		$sql = "SELECT vt.*,vct.*,vtb.*,vtd.*,vtm.*,vta.*,vts.* ,vtf.*
															
			FROM  vehicle_tbl vt 
			
			LEFT JOIN vehicle_category_tbl vct
			ON vt.vehicle_category_id = vct.vehicle_category_id 
			
			LEFT JOIN vehicle_type_tbl vtb
			ON vt.vehicle_type_id = vtb.vehicle_type_id 
			
			LEFT JOIN vehicle_tbl_documents vtd
			ON vt.vehicleId = vtd.vehicleId

			LEFT JOIN vehicle_tbl_model vtm 
			ON vt.vehicleId = vtm.vehicleId

			LEFT JOIN vehicle_tbl_schedule vts
			ON vt.vehicleId = vts.vehicleId 
			
			LEFT JOIN vehicle_tbl_features vtf
			ON vt.vehicleId = vtf.vehicleId 
			
			LEFT JOIN vehicle_tbl_address vta
			ON vt.vehicleId = vta.vehicleId 
									
			WHERE vt.delete_status = 0 
			AND vta.city= '".$_POST['location']."'
			AND multiDayRenting >= '".$interval."'";
			
			if(isset($_GET['vc_id']))
			{
				$sql = $sql."AND vt.vehicle_category_id = '".$_GET['vc_id']."'";
			}
			
			if(isset($_GET['vt_id']))
			{
				$sql = $sql."AND vt.vehicle_type_id = '".$_GET['vt_id']."'";
			}
			
			$sql = $sql."ORDER BY rand()";
	}
	//die($sql);		
	$ans = mysqli_query($obj->con,$sql);
//END FETCH index page random vehicles ORDER BY RAND()
	
?>
<section>
        	<div class="space no-space">
        		<div class="container-fluid">
                	<div class="cola-page">
                        <div class="row merged">
                            <div class="col-lg-12 ">
                   
								<div class="pickup-time">
                                  	
									<span class="find">
                                  		<i class="fa fa-location-arrow"></i>
                                  		Find a car next to you ...
                                  		<a href="#" title="" class="theme-btn slim">
                                  		<i class="fa fa-map-marker"></i>
                                  		use my location</a>
                                  	</span>
									
									<?php
									if(isset($_GET['vc_id']))
									{
										echo "<form action='bookCar.php?details&vc_id=".$_GET['vc_id']."' method='post'>";
									}
									
									elseif(isset($_GET['vt_id']))
									{
										echo "<form action='bookCar.php?details&vt_id=".$_GET['vt_id']."' method='post'>";
									}
									
									else
									{
										echo "<form action='bookCar.php?details' method='post'>";
									}
									?>
									
									
									
                                    <div class="date-n-time">
										<p>
                                            <label><i class="fa fa-map-marker"></i></label>
                                            <input id="location" name="location" type="text" placeholder="location" autocomplete="off" list="cityname" value="<?php if(isset($_GET['details'])) {echo $_POST['location'];}?>" required=true>
											<datalist id="cityname">
												<?php
													$sql_city = "Select * from cities ORDER BY city";
													$ans_city = mysqli_query($obj->con,$sql_city);
													
													while($row_city = mysqli_fetch_array($ans_city))
													{
														echo "<option value='".$row_city['city']."'>";
													}
												?>
											</datalist>
                                        </p> 
										
                                        <p>
                                            <label><i class="fa fa-calendar"></i></label>
                                            <input id="datetimepicker1" name="datetimepicker1" type="text" placeholder="Pickup" autocomplete="off" value="<?php if(isset($_GET['details'])) {echo $_POST['datetimepicker1'];}?>" required=true>
                                        </p>  
                                        <p>  
                                            <label><i class="fa fa-clock-o"></i></label>
                                            <input id="datetimepicker2" name="datetimepicker2" type="text" placeholder="Time" autocomplete="off" value="<?php if(isset($_GET['details'])) {echo $_POST['datetimepicker2'];}?>" required=true>
                                        </p>
                                        <p>
                                            <label><i class="fa fa-calendar"></i></label>
                                            <input id="datetimepicker3" name="datetimepicker3" type="text" placeholder="Return" autocomplete="off" value="<?php if(isset($_GET['details'])) {echo $_POST['datetimepicker3'];}?>" required=true>
                                        </p>  
                                        <p>  
                                            <label><i class="fa fa-clock-o"></i></label>
                                            <input id="datetimepicker4" name="datetimepicker4" type="text" placeholder="Time" autocomplete="off" value="<?php if(isset($_GET['details'])) {echo $_POST['datetimepicker4'];}?>" required=true>
                                        </p>
										
											<input type="submit"  id="findVehicle_btn" name="findVehicle_btn"value="Search" class="theme-btn" style="margin-left:15px;"/>
											
										
                                      
                                    </div>
									</form>
									 <p>
                                        <a href="#" title="" class="theme-btn slim fiter-car" style="float:left;margin-top:-20px;margin-right:15px;"><i class="fa fa-filter"></i>filters</a>
									</p>	
                                </div>
                                <div class="cars-listing">
                                	<div class="filter-popup">
                                		<div class="filter-meta">
                                			<span>price / day</span>
											<div id="slider-range"></div>
											<input type="text" id="amount" readonly>
										</div><!-- range slider -->
                               			<div class="filter-meta">
                               				<span>style</span>
                               				<ul>
                               					<li><a href="#" title=""><img src="images/resources/mockup-1.png" alt=""><i>micro</i></a></li>
                               					<li><a href="#" title=""><img src="images/resources/mockup-2.png" alt=""><i>Hatchback</i></a></li>
                               					<li><a href="#" title=""><img src="images/resources/mockup-3.png" alt=""><i>Coupe</i></a></li>
                               					<li><a href="#" title=""><img src="images/resources/mockup-4.png" alt=""><i>CUV</i></a></li>
                               					<li><a href="#" title=""><img src="images/resources/mockup-5.png" alt=""><i>SUV</i></a></li>
                               					<li><a href="#" title=""><img src="images/resources/mockup-6.png" alt=""><i>Cabriolet</i></a></li>
                               					<li><a href="#" title=""><img src="images/resources/mockup-7.png" alt=""><i>Super Car</i></a></li>
                               					<li><a href="#" title=""><img src="images/resources/mockup-8.png" alt=""><i>Pickup</i></a></li>
                               					<li><a href="#" title=""><img src="images/resources/mockup-9.png" alt=""><i>Van</i></a></li>
                               					<li><a href="#" title=""><img src="images/resources/mockup-10.png" alt=""><i>Caravan</i></a></li>
                               				</ul>
                               			</div><!-- car mockup -->
                               			<div class="filter-meta">
                               				<span>fule type</span>
                               				<form method="post" class="fule-type">
												<p>
													<input type="checkbox">
													<label></label>
													Bi-Fuel
												</p>
												<p>
													<input type="checkbox">
													<label></label>
													Diesel
												</p>
												<p>
													<input type="checkbox">
													<label></label>
													Electric
												</p>
												<p>
													<input type="checkbox">
													<label></label>
													Hybrid
												</p>
												<p>
													<input type="checkbox">
													<label></label>
													Petrol
												</p>
											</form>
                               			</div><!-- enging select -->
                               			<div class="filter-meta">
                               				<span>Seats</span>
                               				<select>
                               					<option>2+</option>
                               					<option>4+</option>
                               					<option>6+</option>
                               				</select>
                               			</div><!-- seats -->
                               			<div class="filter-meta">
                               				<span>Additional</span>
                               				<form method="post" class="fule-type additional">
												<p>
													<input type="checkbox">
													<label></label>
													ABS 
												</p>
												<p>
													<input type="checkbox">
													<label></label>
													Cruise control 
												</p>
												<p>
													<input type="checkbox">
													<label></label>
													Front fog lights 
												</p>
												<p>
													<input type="checkbox">
													<label></label>
													Alarm
												</p>
												<p>
													<input type="checkbox">
													<label></label>
													Electric door mirrors 
												</p>
												<p>
													<input type="checkbox">
													<label></label>
													Heated door mirrors  
												</p>
												<p>
													<input type="checkbox">
													<label></label>
													Audio remote control
												</p>
												<p>
													<input type="checkbox">
													<label></label>
													Electrically drivers seat 
												</p>
												<p>
													<input type="checkbox">
													<label></label>
													Passenger airbag  
												</p>
												<p>
													<input type="checkbox">
													<label></label>
													Bluetooth connection  
												</p>
												<p>
													<input type="checkbox">
													<label></label>
													Folding rear seats  
												</p>
												<p>
													<input type="checkbox">
													<label></label>
													Rear electric windows  
												</p>
												<p>
													<input type="checkbox">
													<label></label>
													Climate control 
												</p>
												<p>
													<input type="checkbox">
													<label></label>
													Front electric windows  
												</p>
												<p>
													<input type="checkbox">
													<label></label>
													Remote central locking 
												</p>
											</form>
                               			</div><!-- additional info -->
                               			<div class="filter-meta">
                               				<span>Preferences</span>
                               				<form method="post" class="fule-type">
												<p>
													<input type="checkbox">

													<label></label>
													80% acceptance rate
												</p>
												<p>
													<input type="checkbox">
													<label></label>
													Replies within 1 hr
												</p>
												<p>
													<input type="checkbox">
													<label></label>
													Long time renting
												</p>
												
											</form>
                               			</div><!-- preferences -->
                               			<div class="filter-btns">
                               				<a href="#" title="" class="del-filter"><i class="fa fa-trash-o"></i>Delete Filters</a>
                               				<a href="#" title="" class="cancel">Cancel</a>
                               				<a href="#" title="" class="theme-btn slim"><i class="fa fa-refresh"></i>refresh search</a>
                               			</div>
                                	</div>
                                	<span><?php echo mysqli_num_rows($ans); ?> of <?php 
									$totalVehicleSql = "SELECT * FROM vehicle_tbl WHERE delete_status=0 AND is_active = 1";
									$totalVehicleAns = mysqli_query($obj->con,$totalVehicleSql);echo mysqli_num_rows($totalVehicleAns); ?> Vehicles available for renting</span>
                                    
									<?php
									
									while($row = mysqli_fetch_array($ans))
									{
									?>
                                    <div class="cars-list">
                                        <figure>
                                            <img alt="" src="./vehicle/<?php echo $row['photo1_link'];?>" style="height:200px;">
                                        </figure>
                                        <div class="car-inf">
                                            <h5><br /><?php echo $row['companyName']." ".$row['modelName']."<span> ".$row['modelNumber']."</span>";?></h5>
                                            <p>I keep the car very tidy and always great</p>
                                            <span class="locatio"><i class="fa fa-location-arrow"></i><?php echo $row['street']." ,".$row['city']." ,".$row['state']?></span>
                                            <div class="car-owner">
                                               
                                                <div class="car-meta">
                                                	
                                                	<ul class="stars-rating">
                                                        <li><i class="fa fa-star checked"></i></li>
                                                        <li><i class="fa fa-star checked"></i></li>
                                                        <li><i class="fa fa-star checked"></i></li>
                                                        <li><i class="fa fa-star checked"></i></li>
                                                        <li><i class="fa fa-star checked"></i></li>
                                                    </ul>
                                                    <span>89% Satisfaction Rate </span>
                                                </div>	
                                            </div>
                                            <span class="tag bottom">&#8377;<?php echo $row['rate'];?>/KM</span>
											<form id="bookVehicle" action="dbclass.php" method="post" style="float:right;margin-top:20px;margin-right:15px;">						
												<input type="hidden" id="vehicleId" name="vehicleId" value="<?php echo $row['vehicleId'];?>" />
												<input type="hidden" id="vehicleOwnerId" name="vehicleOwnerId" value="<?php echo $row['owner_id'];?>" />
												<input type="hidden" id="rate" name="rate" value="<?php echo $row['rate'];?>" />
												<input type="hidden" id="my_location" name="my_location" class="myids" value="<?php if(isset($_GET['details'])) {echo $_POST['location'];}?>"/>
												<input type="hidden" id="my_datetimepicker1" name="my_datetimepicker1" class="myids1" value="<?php if(isset($_GET['details'])) {echo $_POST['datetimepicker1'];}?>"/>
												<input type="hidden" id="my_datetimepicker2" name="my_datetimepicker2" class="myids2" value="<?php if(isset($_GET['details'])) {echo $_POST['datetimepicker2'];}?>"/>
												<input type="hidden" id="my_datetimepicker3" name="my_datetimepicker3" class="myids3" value="<?php if(isset($_GET['details'])) {echo $_POST['datetimepicker3'];}?>"/>
												<input type="hidden" id="my_datetimepicker4" name="my_datetimepicker4" class="myids4" value="<?php if(isset($_GET['details'])) {echo $_POST['datetimepicker4'];}?>"/>
												<?php
												if(isset($_GET['details']))
												{
												?>
												<input type="submit" id="bookVehicle_btn" name="bookVehicle_btn" class="myButton" value="Book now" >
												<?php
												}
												?>
											</form>
											
                                        </div>
                                    </div>
                                    <?php 
										}
									?>
                                    
									
                               		
                                </div>
								
								
                            </div>
                            
                        </div>
                    </div>
        		</div>
        	</div>
        </section><!-- points section -->
    </div>

	
<?php
	
	include_once('footer.php');
?>