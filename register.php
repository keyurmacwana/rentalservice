<?php
	include_once('header.php');
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("#btnCreateAccount").click(function(e) { 
		console.log("BUTTON CLICK");
		e.preventDefault();
        var proceed = true;
        //simple validation at client's end
        //loop through each field and we simply change border color to red for invalid fields       
        $("#customer-form input[required=true]").each(function(){			
            $(this).css('border-color',''); 
            if(!$.trim($(this).val())){ //if this field is empty 
                $(this).css('border-color','red'); //change border color to red   
                proceed = false; //set do not proceed flag
            }
			//check invalid full name
			var cname = /^[a-zA-Z ]*$/;	
			if ((this.name) == "cname")
			{						
				if($(this).attr("type")=="text" && !cname.test($.trim($(this).val()))){               
					$(this).css('border-color','red'); //change border color to red   
					proceed = false; //set do not proceed flag              
				}
			} 			
            //check invalid email
			if ((this.name) == "email")
			{
				var email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/; 
				if($(this).attr("type")=="email" && !email.test($.trim($(this).val()))){
					$(this).css('border-color','red'); //change border color to red   
					proceed = false; //set do not proceed flag              
            	} 
			}  
			//check invalid mobile 
			if ((this.name) == "mobile")
			{
			 
				var mobile = /^\d{10}$/; 
				if($(this).attr("type")=="text" && !mobile.test($.trim($(this).val()))){				 
					$(this).css('border-color','red'); //change border color to red   
					proceed = false; //set do not proceed flag              
				}
			}
        });
       console.log(proceed);
        if(proceed) //everything looks good! proceed...
        {

            //get input field values data to be sent to server
            post_data = {
                'cname'    : $('input[name=cname]').val(),
				'email'    : $('input[name=email]').val(),
				'mobile'    : $('input[name=mobile]').val(),
				'password'    : $('input[name=password]').val()
            };
			console.log(post_data);
            //Ajax post data to server
            $.post('register_customer.php', post_data, function(response){ 
				console.log(response);
                if(response.type == 'error'){ //load json data from server and output message     
                    output = '<div class="error" style="background: #FFE8E8;padding: 5px 10px 5px 10px;margin: 0px 0px 5px 0px;border: none;font-weight: bold;color: #FF0000;border-left: 3px solid #FF0000;">'+response.text+'</div>';
					 $("#customer-form  input[required=true]").val(''); 
                }else{
                    output = '<div class="success" style="background: #D8FFC0;padding: 5px 10px 5px 10px;margin: 0px 0px 5px 0px;border: none;font-weight: bold;color: #2E6800;border-left: 3px solid #2E6800;">'+response.text+'</div>';
                    //reset values in all input fields
                    $("#customer-form  input[required=true]").val(''); 
                    $("#customer-form #customersub").slideUp().delay(2000).slideDown(500); //hide form after success
                }
                $("#customer-form #registercustomerresult").hide().html(output).slideDown();
            }, 'json');
        }
    });
    
    //reset previously set border colors and hide all message on .keyup()
    $("#customer-form  input[required=true]").keyup(function() { 
        $(this).css('border-color',''); 
        $("#registercustomerresult").slideUp();
    });
	
	
});
</script>
<div class="sub-head blackish ext-low-opacity">
			<div class="bg-image" style="background-image: url(images/resources/subhead-bg.jpg)"></div>
        	<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="cola-head">
							<div class="head-meta">
								<h4>Register</h4>
								<p>( Create an account today. )</p>
							</div>
							
						</div>
					</div>
				</div>
			</div>
        </div><!-- subhead -->
        
        <section>
        	<div class="space">
        		<div class="container">
        			<div class="row">
        				<div class="col-md-12">
                        	<div class="login-sec">
                            	<div class="row">
                                	<div class="col-md-4">
                                    	<div class="log-with-fb">
                                        	<h4>Login in seconds:</h4>
                                            <a title="" href="#" class="facebook"><i class="fa fa-facebook"></i>facebook</a>
											<a title="" href="#" class="google"><i class="fa fa-google-plus"></i>Google</a>
											<a title="" href="#" class="twitter"><i class="fa fa-twitter"></i>Twitter</a>
                                        </div>
                                    </div>
                                    <div class="offset-lg-1 col-lg-7 col-md-8">
                                    	<div class="cola-form">
                                        	<h4><i class="flaticon-sign-in"></i>Register:</h4>
											
											<div class="box-authentication" id="customer-form">
											<div id="registercustomerresult"></div>
											<div id="customersub">
                                            <form method="post" id="form-create-account" action="">
                                            	<label>Full Name:</label>
                                                <input id="cname" name="cname" maxlength="100" type="text" style="width: 100%;" class="form-control"  required=true>
                                            	<label>Email:</label>
                                                <input id="email" name="email" maxlength="100" type="text" style="width: 100%;" class="form-control" required=true>
                                                <label>Mobile:</label>
                                                <input id="mobile" name="mobile" maxlength="10" type="text" style="width: 100%;" class="form-control" required=true>
                                                <label>password:</label>
                                                <input id="password" name="password" maxlength="25" type="password" style="width: 100%;" class="form-control" required=true>
                                               
                                                <button class="button theme-btn" type="submit" id="btnCreateAccount" name="btnCreateAccount">register</button>
                                            </form>
                                            <span>Are you a member already? <a href="login.php" title="">Login here</a></span>
											</div>
											</div>
											
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
        			</div>
        		</div>
        	</div>
        </section><!-- form section -->
        
        

<?php
	include_once('footer.php');
?>