<?php
	include_once('header.php');
?>
 <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    
	$("#btnLogin").click(function(e) { 
		e.preventDefault();
        var proceed1 = true;
        //simple validation at client's end
        //loop through each field and we simply change border color to red for invalid fields       
        $("#login-form input[required=true]").each(function(){			
            $(this).css('border-color',''); 
            if(!$.trim($(this).val())){ //if this field is empty 
                $(this).css('border-color','red'); //change border color to red   
                proceed1 = false; //set do not proceed flag
            }						            			
        });
       
        if(proceed1) //everything looks good! proceed...
        {
            //get input field values data to be sent to server
            post_data1 = {              
				'email'    : $('input[name=lemail]').val(),				
				'password'    : $('input[name=lpassword]').val()<!--,
			<!--	'rememberMe'    : $('input[name=rememberMe]').'1'-->
				
            };
			
			console.log(post_data1);
            //Ajax post data to server
            $.post('customer_login.php', post_data1, function(response){  	
				//console.log(response);
                if(response.type == 'error'){ //load json data from server and output message 
					 
                    output = '<div class="error" style="background: #FFE8E8;padding: 5px 10px 5px 10px;margin: 0px 0px 5px 0px;border: none;font-weight: bold;color: #FF0000;border-left: 3px solid #FF0000;">'+response.text+'</div>';
					 $("#login-form  input[required=true]").val(''); 
                }else{
                    output = '<div class="success" style="background: #D8FFC0;padding: 5px 10px 5px 10px;margin: 0px 0px 5px 0px;border: none;font-weight: bold;color: #2E6800;border-left: 3px solid #2E6800;">'+response.text+'</div>';
                    //reset values in all input fields
                    
                    $("#login-form #loginsub").slideUp().delay(2000).slideDown(500); //hide form after success
					
						$("#login-form  input[required=true]").val(''); 
					window.location.href = 'index.php';
                }
                $("#login-form #logincustomerresult").hide().html(output).slideDown();
            }, 'json');
        }
    });
    
    //reset previously set border colors and hide all message on .keyup()
    $("#login-form  input[required=true]").keyup(function() { 
        $(this).css('border-color',''); 
        $("#logincustomerresult").slideUp();
    });
});
</script>
 
        
		<div class="sub-head blackish ext-low-opacity">
			<div class="bg-image" style="background-image: url(images/resources/subhead-bg.jpg)"></div>
        	<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="cola-head">
							<div class="head-meta">
								<h4>Login</h4>
								<p>( Login to your account. )</p>
							</div>
							
							<div class="info-links">
								<a href="how-this-work.php" title="">how does this work</a>
								<a href="vehicleOwner/vehicleOwnerSignup.php" title="">submit your car</a>
								<span><i class="fa fa-phone"></i>call (012) 345 - 6789</span>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div><!-- subhead -->
        
        <section>
        	<div class="space">
        		<div class="container">
        			<div class="row">
        				<div class="col-md-12">
                        	<div class="login-sec">
                            	<div class="row">
                                	<div class="col-md-4">
                                    	<div class="log-with-fb">
                                        	<h4>Login in seconds:</h4>
                                            <a class="facebook" href="#" title=""><i class="fa fa-facebook"></i>facebook</a>
											<a class="google" href="#" title=""><i class="fa fa-google-plus"></i>Google</a>
											<a class="twitter" href="#" title=""><i class="fa fa-twitter"></i>Twitter</a>
                                        </div>
                                    </div>
                                    <div class="offset-md-1 col-md-7">
                                    	<div class="cola-form">
                                        	<h4><i class="flaticon-key"></i>Login:</h4>
											
											<div class="box-authentication" id="login-form">
											<div id="logincustomerresult"></div>
											<div id="loginsub">
                                            <form action="" id="form-login" method="post">
                                            	<label>Email/Mobile</label>
                                                <input id="lemail" name="lemail" type="text" style="width: 100%;" class="form-control" required=true>
                                                <label>password</label>
                                                <input id="lpassword" name="lpassword" type="password" style="width: 100%;" class="form-control" required=true>
                                                <input type="checkbox"  name="rememberMe" id="rememberMe" value="1" class="form-control">
                                                <label for="checkbox">Remember me</label>
                                                <button class="button theme-btn" type="submit" id="btnLogin" name="btnLogin">login</button>
                                            </form>
											</div>
											</div>
                                            <span>Aren’t you a member yet? <a href="register.php" title="">Register here</a></span>
                                            <span>Forgot your password? <a href="#" title="">Click here</a></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
        			</div>
        		</div>
        	</div>
        </section><!-- form section -->
        
        <section>
			<div class="space no-space dark-bg">
				<div class="container">
					<div class="row">
						<div class="col-lg-4 col-sm-4">
							<div class="servize">
								<i class="flaticon-placeholder"></i>
								<div class="serviz-meta">
									<span>Tucson, AZ 80210: 501 Lane</span>
									<i>Canada</i>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-sm-4">
							<div class="servize">
								<i class="flaticon-24-hours-1"></i>
								<div class="serviz-meta">
									<span>Tool free number 24/7</span>
									<i>+1-111-222-333</i>
								</div>
							</div>
						</div>
						<div class="col-lg-4 col-sm-4">
							<div class="servize">
								<i class="flaticon-24-hours"></i>
								<div class="serviz-meta">
									<span>Full time services</span>
									<i>24/7</i>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section><!-- footer top -->
        
<?php
	include_once('footer.php');
?>