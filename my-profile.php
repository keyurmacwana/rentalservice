<?php 
	include_once('header.php');
?>

<div class="sub-head blackish ext-low-opacity">
			<div class="bg-image" style="background-image: url(images/resources/subhead-bg.jpg)"></div>
        	<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="cola-head">
							<div class="head-meta">
								<h4>Selena G.</h4>
								<p>( Cambridge )</p>
							</div>
							<a href="#" title="" class="theme-btn"><i class="fa fa-share-alt"></i>share profile</a>
							<div class="info-links">
								<a href="#" title="">how does this work</a>
								<a href="#" title="">submit your car</a>
								<span><i class="fa fa-phone"></i>call (012) 345 - 6789</span>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div><!-- subhead -->
        
        <section>
        	<div class="space">
        		<div class="container">
        			<div class="row">
                    	<div class="col-md-12">
                        	<div class="profile">
                            	<div class="row">
									<div class="col-lg-3">
                                    	<div class="profile-dp">
                                        	<figure><img src="images/resources/addImage.png" alt=""></figure>
                                            <div class="profile-info">
												<span class="profile-name">Selena G.</span>
												<ul class="stars-rating">
													<li><i class="fa fa-star checked"></i></li>
													<li><i class="fa fa-star checked"></i></li>
													<li><i class="fa fa-star checked"></i></li>
													<li><i class="fa fa-star checked"></i></li>
													<li><i class="fa fa-star checked"></i></li>
												</ul>
												<ins>5 Reviews</ins>
											</div>
                                            <ul class="verification">
                                            	<li><i class="flaticon-world"></i>Online on 4 Oct, 2017 at 08:36</li>
                                                <li><i class="flaticon-tick"></i>Account connected on Facebook</li>
                                                <li><i class="flaticon-tick"></i>Phone number verified</li>
                                            </ul>
                                            <a href="#" title="" class="theme-btn">send message</a>
                                        </div>
										
                                    </div>
                                	
                                    <div class="col-lg-6">
										<div class="profile-bg">
											<ul class="nav nav-tabs profile-btn">
												<li class="nav-item"><a class="active" href="#link1" data-toggle="tab">Dashboard</a></li>
												 <li class="nav-item"><a class="" href="#link2" data-toggle="tab">Profile</a></li>
												 <li class="nav-item"><a class="" href="#link3" data-toggle="tab">Rides</a></li>
												 <li class="nav-item"><a class="" href="#link4" data-toggle="tab">Reviews</a></li>
											</ul>
											<!-- Tab panes -->
											<div class="tab-content profile">
											  <div class="tab-pane active fade show " id="link1" >
												<div class="ride-request">
													<ul>
														<li>Users since<span>21 Oct 2014</span></li>
														<li>city<span>Cardiff</span></li>
														<li>Rides created<span>0</span></li>
														<li>Rides as driver<span>o rides / 0 km</span></li>
														<li>Rides as passenger<span>6 rides / 1598 km</span></li>
													</ul>
													<span class="flag"><i class="flaticon-racing-flag"></i>flag</span>
												</div>
											  </div>
											  <div class="tab-pane fade" id="link2" >
													<div class="ride-request">
														<ul>
															<li>Users since<span>21 Oct 2014</span></li>
															<li>city<span>Cardiff</span></li>
															<li>Rides created<span>0</span></li>
															<li>Rides as driver<span>o rides / 0 km</span></li>
															<li>Rides as passenger<span>6 rides / 1598 km</span></li>
														</ul>
														<span class="flag"><i class="flaticon-racing-flag"></i>flag</span>
													</div>
												</div>
											  <div class="tab-pane fade" id="link3">
													<div class="colla-table">
													<h5 class="little-title">Published rides:</h5>
													<p>There are no upcoming published rides.</p>
												</div>
												<div class="colla-table">
													<h5 class="little-title">Requested rides:</h5>
													<p>There are no upcoming requested rides.</p>
												</div>
												</div>
												  <div class="tab-pane fade" id="link4">
														<div class="reviewers">
														<h5 class="little-title">reviews</h5>
														<p>This user has no ratings so far. </p>
													</div>
												  </div>
											</div>
										</div>	
                                    </div>
                                    <div class="col-lg-3 col-md-12">
										<div class="sidebar right">
											<div class="note-box">
												<span><i class="flaticon-car-3"></i>rent a car</span>
												<p>Rent the perfect car for hour, day or week. Insurance included & roadside assistance.</p>
												<a href="#" title="" class="theme-btn">how does it work</a>
											</div>
											<div class="progress-box">
												<h5 class="little-title">complete your profile</h5>
												<div class="progress">
													<div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:20%">20%</div>
												</div>
												<div class="profile-list">
													<p>Increase your chance to get noticed on the platform by completing your profile:</p>
													<ul>
														<li class="complete"><i class="fa fa-check-circle-o"></i>Name and email</li>
														<li class="complete"><i class="fa fa-check-circle-o"></i>Profile picture</li>
														<li class="incomplete"><i class="fa fa-ban"></i>Training</li>
														<li class="incomplete"><i class="fa fa-ban"></i>Profession</li>
														<li class="incomplete"><i class="fa fa-ban"></i>Complete address</li>
														<li class="incomplete"><i class="fa fa-ban"></i>About me</li>
													</ul>
													<a href="profile-edit.php" title="" class="theme-btn">complete profile</a>
												</div>
											</div>
										</div>	
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
        		</div>
        	</div>
        </section><!-- profile section -->
<?php
	include_once('footer.php');
?>