<?php
	include_once('header.php');
	if(isset($_GET['is_active']))
	{
		
		$obj -> changeStatus_termsandconditions_tbl($_GET['is_active'],$_GET['termId']);
		
	}
	
	if(isset($_GET['termsAndConditionDelete']))
	{
		$obj -> termsAndConditionDelete($_GET['termsAndConditionDelete'],$_GET['termCategoryId']);
	}

	if(isset($_GET['termsAndConditionEdit']))
	{	
		$data_termsAndConditionEdit =  $obj -> termsAndConditionEdit($_GET['termsAndConditionEdit']);	
	}
	
?>
<!-- Main Body content starts here -->
        <div id="wrapper">
            <!-- Sidebar -->
            <div id="sidebar-wrapper">
                <aside class="sidebar">
                    <nav class="sidebar-nav" id="sidebarscroll">
                        <ul class="metismenu ripple" id="menu">
						
							<li>
								<a href="dashboard.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-folder-open fa-lg notify"></span>
								<span class="sidebar-nav-item aText" ><font color="grey">Dashboard</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>

							<li >
								<a href="vehicle_type.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-car fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey">Vehicle Type</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
							
							<li >
								<a href="vehicle_rates_category.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-rupee fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey" >Vehicle Category</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
							
							<li >
								<a href="manageFuelList.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-beer fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey" >Manage Fuel Type</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
							
							<li>
								<a href="register_owner.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-book fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey" >Manage Owner</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
							
							<li>
								<a href="newOwnerRequest.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-plus fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey" >New Owner Request</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
							
							<li >
								<a href="termsAndConditionList.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-pencil fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey" >Terms List</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
							
							<li style="background-color:#505464;">
								<a href="termsAndCondition.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-bullhorn fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey" >Terms & Conditons</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
							
							
							
							<li >
								<a href="homePageBanner.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-camera fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey" >Home Page Banner</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
						
                            
                        </ul>
                    </nav>
                </aside>
            </div>
            <!-- # Sidebar-wrapper -->
            
			<!-- Page Content-wrapper -->
			<div id="page-content-wrapper">
	
				<!-- Breadcrumb  -->
				<div class="row csk-breadcrumb">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
						<h4 class="page-title">Terms and Conditions</h4>
					</div>
					<div class="col-lg-9 col-md-8 col-sm-8 hidden-xs">
						<ol class="breadcrumb">
							<li><a href="dashboard.php">Dashboard</a></li>
							<li><a href="#">Terms and Conditions</a></li>
						</ol>
					</div>
				</div>
				<!-- #Breadcrumb -->
	
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-defaut">
							<div class="panel-heading">
								<h3 class="panel-title">Terms and Conditions</h3>
							</div>
							<div class="panel-body">
								<form action="dbclass.php" method="post" >
									<input type="hidden" name="adminSignature" id="adminSignature" value="<?php echo $data['register_id'];?>">
									
									<?php 
										if(isset($_GET['termsAndConditionEdit']))
										{ 
											echo "<input type='hidden' name='termId' id='termId' value='".$data_termsAndConditionEdit['termId']."'>";
										} 
									?>
									
										<div class="row">
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Term Category</label>
													<select name="termCategoryId" class="form-control"  id="termCategoryId">
													<?php
													
														$sql_termCategoryList = "SELECT * FROM termsandconditions_category WHERE is_taken = 0 AND delete_status=0";
														$ans_termCategoryList = mysqli_query($obj->con,$sql_termCategoryList);
	 
														while($row_termCategoryList = mysqli_fetch_array($ans_termCategoryList))
														{	
													?>													
														<option value="<?php echo $row_termCategoryList['termCategoryId'];?>"><?php echo $row_termCategoryList['termCategory'];?></option>
														
													<?php
														} //END OF WHILE
														if(isset($_GET['termsAndConditionEdit']))
														{
															echo "<option value='".$data_termsAndConditionEdit['termCategoryId ']."' selected>".$data_termsAndConditionEdit['termCategory'] ."</option>";
														}
													?>
													
													</select>
													
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Term For</label>
													<select name="termFor" class="form-control"  id="termFor">
														<option value="0"
														<?php 
															if(isset($_GET['termsAndConditionEdit']))
															{
																if($data_termsAndConditionEdit['termFor'] == 0) 
																	{echo "selected";}
															} 
														?>>Vehicle Owner</option>
														<option value="1"
														<?php 
															if(isset($_GET['termsAndConditionEdit']))
															{
																if($data_termsAndConditionEdit['termFor'] == 1) 
																	{echo "selected";}
															} 
														?>>Vehicle Renter</option>
													</select>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<label class="control-label">Status</label>
													<select name="is_active" class="form-control"  id="is_active">
														<option value="0"
														<?php 
															if(isset($_GET['termsAndConditionEdit']))
															{
																if($data_termsAndConditionEdit['is_active'] == 0) 
																	{echo "selected";}
															} 
														?>>De-Active</option>
														<option value="1"
														<?php 
															if(isset($_GET['termsAndConditionEdit']))
															{
																if($data_termsAndConditionEdit['is_active'] == 1) 
																	{echo "selected";}
															} 
														?>>Active</option>
													</select>
												</div>
											</div>
										</div>
										<div class="row">
											<!-- col-md-12 Starts Here -->
											<div class="col-md-12">
												
																<textarea class="" name="termDetail" id="termDetail" title="Contents" required="" cols="168"><?php if(isset($_GET['termsAndConditionEdit']))
														{
															echo $data_termsAndConditionEdit['termDetail'];
														}
												?></textarea>
															
											</div>	
										</div>
									<div class="form-actions">
										<button type="submit" class="btn btn-success" name="termAndCondition" id="termAndCondition"> <i class="fa fa-check"></i> Save</button>
										<button type="button" class="btn btn-danger">Cancel</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- #row -->
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default panel-with-options">
			
				<h3 >&emsp;Terms and Conditions List</h3>
			
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered datatable">
						<thead>
							<tr>
								<th>SR NO.</th>
								<th>Term Category</th>
								<th>Terms For</th>
								<th>Description</th>
								<th>Status</th>
								<th>Date Created</th>
								<th>Action</th>
							</tr>
						</thead>
						<tfoot>
						<tr>
								<th>SR NO.</th>
								<th>Term Category</th>
								<th>Terms For</th>
								<th>Description</th>
								<th>Status</th>
								<th>Date Created</th>
								<th>Action</th>
							</tr>
						</tfoot>
						<tbody>
							
							<?php 
								$sql_termsandconditions_tbl = "SELECT tnc.*,tncl.termCategory FROM  termsandconditions_tbl tnc LEFT JOIN termsandconditions_category tncl ON tnc.termCategoryId=tncl.termCategoryId WHERE tnc.delete_status=0";		
		
								$ans_termsandconditions_tbl = mysqli_query($obj->con,$sql_termsandconditions_tbl);
	
								$counter = 1;
								while($row_termsandconditions_tbl = mysqli_fetch_array($ans_termsandconditions_tbl))
								{
							?>
								<tr>
									<td><?php echo $counter ++; ?></td>
									<td><?php echo $row_termsandconditions_tbl['termCategory']; ?></td>
									<td><?php 
											if($row_termsandconditions_tbl['termFor'] == 0)
											{
												echo "<label class='btn btn-block btn-info btn-rounded'>Vehicle Owner</label>";
									
											}
											
											else
											{
												echo "<label class='btn btn-block btn-warning btn-rounded'>Vehicle Renter</label>";
											}
										?></td>
									<td><?php echo "<div style='display: inline;'>".$row_termsandconditions_tbl['termDetail']."</div>"; ?></td>
									<td><?php 
											if($row_termsandconditions_tbl['is_active'] == 1)
											{?>
												<span class="label label-success"><a href="termsAndCondition.php?is_active=<?php echo $row_termsandconditions_tbl['is_active'];?>&termId=<?php echo $row_termsandconditions_tbl['termId']?>" style="color:white;">Active</a></span>
											<?php
											}
											else
											{?>
												<span class="label label-danger"><a href="termsAndCondition.php?is_active=<?php echo $row_termsandconditions_tbl['is_active'];?>&termId=<?php echo $row_termsandconditions_tbl['termId']?>" style="color:white;">De-Active</a></span>
											<?php
											}
											?>
									</td>
									<td><?php echo $row_termsandconditions_tbl['dateCreated']; ?></td>
									<td>
										<a href="termsAndCondition.php?termsAndConditionEdit=<?php echo $row_termsandconditions_tbl['termId']; ?>"title="EDIT"><i class="fa fa-pencil fa-1x "></i></a> || <a href="termsAndCondition.php?termsAndConditionDelete=<?php echo $row_termsandconditions_tbl['termId']; ?>&termCategoryId=<?php echo $row_termsandconditions_tbl['termCategoryId']; ?>" title="DELETE"><i class="fa fa-trash fa-1x"></i></a>
									</td>
								</tr>
							<?php
							}
							?>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
				
			
<?php
	include_once('footer.php');
?>