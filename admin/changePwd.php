<?php
	include_once('header.php');
?>
<!-- Main Body content starts here -->
        <div id="wrapper">
            <!-- Sidebar -->
            <div id="sidebar-wrapper">
                <aside class="sidebar">
                    <nav class="sidebar-nav" id="sidebarscroll">
                        <ul class="metismenu ripple" id="menu">
						
								<li >
									<a href="dashboard.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-folder-open fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Dashboard</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
	
								<li >
									<a href="vehicle_type.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-car fa-lg notify"></span>
									<span class="sidebar-nav-item aText"><font color="grey">Vehicle Type</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li >
									<a href="vehicle_rates_category.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-rupee fa-lg notify"></span>
									<span class="sidebar-nav-item aText"><font color="grey" >Rates and Category</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li>
									<a href="register_owner.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-book fa-lg notify"></span>
									<span class="sidebar-nav-item aText"><font color="grey" >Manage Owner</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li>
									<a href="newOwnerRequest.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-plus fa-lg notify"></span>
									<span class="sidebar-nav-item aText"><font color="grey" >New Owner Request</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li >
									<a href="termsAndConditionList.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-pencil fa-lg notify"></span>
									<span class="sidebar-nav-item aText"><font color="grey" >Terms List</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li >
									<a href="termsAndCondition.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-bullhorn fa-lg notify"></span>
									<span class="sidebar-nav-item aText"><font color="grey" >Terms & Conditons</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li >
									<a href="homePageBanner.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-camera fa-lg notify"></span>
									<span class="sidebar-nav-item aText"><font color="grey" >Home Page Banner</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
                            
                        </ul>
                    </nav>
                </aside>
            </div>
            <!-- # Sidebar-wrapper -->
            
			<!-- Page Content-wrapper -->
			<div id="page-content-wrapper">



<div class="row csk-breadcrumb">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
						<h4 class="page-title">Profile</h4>
					</div>
					<div class="col-lg-9 col-md-8 col-sm-8 hidden-xs">
						<ol class="breadcrumb">
							<li><a href="dashboard.php">Dashboard</a></li>
							<li><a href="#">Profile</a></li>
						</ol>
					</div>
				</div>
				<!-- # Breadcrumb -->
				<!-- Profile -->
				<div class="profile">
					<div class="row">
						<div class="col-md-12 profile">
							<div class="cover-footer">
								<div class="google-panel">
									<div class="panel panel-danger contacts">
										<div class="panel-heading">
											
											<div class="profile-line">
												<div class="profile-picture">
													<?php echo "<img src='../web/".$_SESSION['profilePhoto']."' class='img-responsive' alt='avatar' width='100px' height='100px'>" ;?>
<!--													<img src="datas/images/avatars/captain-avatar.png" class="img-responsive" alt="avatar">
-->
												</div>
												<h3><?php echo $_SESSION['uname'];?></h3>
											</div>
											
										</div>
										<div class="panel-tabs">
											<ul class="nav nav-tabs">
<!--												<li class="active"><a href="#tab1default" data-toggle="tab">All</a></li>
												<li><a href="#tab2default" data-toggle="tab">My Skills</a></li>
												<li><a href="#tab3default" data-toggle="tab">About</a></li>
-->												<li><a href="#tab4default" data-toggle="tab">Settings</a></li>
											</ul>
										</div>
										<div class="panel-body">
											<?php echo $_SESSION['msg'];$_SESSION['msg'] = ""?>
											<form action="dbclass.php" method="post">
																<div class="form-group">
																	<label class="control-label">Enter Password</label>
																	<input type="password" id="pwd" name="pwd" class="form-control">
																</div>
																<div class="form-group">
																	<label class="control-label">ReEnter Password</label>
																	<input type="password" id="pwd2" name="pwd2" class="form-control">
																</div>
																<div class="form-group">
																	<button type="changePwd" id="changePwd" name="changePwd" class="btn btn-success">Change Password</button>
																</div>
																<input type="hidden" name="uname" id="uname" <?php echo "value='".$_SESSION['uname']."'";?> >
											</form>
											<form action="profile.php" method="post">
																<div class="form-group">
																	<button type="submit" id="submit" name="submit" >Cancel</button>
																</div>
											</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- #Profile -->

<?php
	include_once('footer.php');
?>