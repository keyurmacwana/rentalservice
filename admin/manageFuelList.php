<?php
	include_once('header.php');
	
	if(isset($_GET['f_id']))
	{
		$data_fuelType =  $obj -> getFuelId($_GET['f_id']);
		
	
	}
	
	if(isset($_GET['d']))
	{
		$obj -> deleteFuel($_GET['d']);
	}		
	
	if(isset($_GET['stat']))
	{
	
		$obj -> changeStatusFuel($_GET['stat'],$_GET['fuelType_id']);
		
	}
?>
<!-- Main Body content starts here -->
<div id="wrapper">
	<!-- Sidebar -->
	<div id="sidebar-wrapper">
		<aside class="sidebar">
			<nav class="sidebar-nav" id="sidebarscroll">
				<ul class="metismenu ripple" id="menu">
				
						<li >
							<a href="dashboard.php" aria-expanded="true">
							
							<span class="sidebar-nav-item-icon fa fa-folder-open fa-lg notify"></span>
							<span class="sidebar-nav-item aText" ><font color="grey" >Dashboard</font></span>
							<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
						</li>

						<li >
							<a href="vehicle_type.php" aria-expanded="true">
							
							<span class="sidebar-nav-item-icon fa fa-car fa-lg notify"></span>
							<span class="sidebar-nav-item aText"><font color="grey">Vehicle Type</font></span>
							<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
						</li>
						
						<li >
							<a href="vehicle_rates_category.php" aria-expanded="true">
							
							<span class="sidebar-nav-item-icon fa fa-rupee fa-lg notify"></span>
							<span class="sidebar-nav-item aText"><font color="grey">Rates and Category</font></span>
							<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
						</li>
						
						<li style="background-color:#505464;">
							<a href="manageFuelList.php" aria-expanded="true">
							
							<span class="sidebar-nav-item-icon fa fa-beer fa-lg notify"></span>
							<span class="sidebar-nav-item aText"><font color="grey" >Manage Fuel Type</font></span>
							<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
						</li>
						
						<li>
							<a href="register_owner.php" aria-expanded="true">
							
							<span class="sidebar-nav-item-icon fa fa-book fa-lg notify"></span>
							<span class="sidebar-nav-item aText"><font color="grey" >Manage Owner</font></span>
							<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
						</li>
						
						<li>
							<a href="newOwnerRequest.php" aria-expanded="true">
							
							<span class="sidebar-nav-item-icon fa fa-plus fa-lg notify"></span>
							<span class="sidebar-nav-item aText"><font color="grey" >New Owner Request</font></span>
							<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
						</li>
						
						<li >
									<a href="termsAndConditionList.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-pencil fa-lg notify"></span>
									<span class="sidebar-nav-item aText"><font color="grey" >Terms List</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
						</li>
						
						<li >
							<a href="termsAndCondition.php" aria-expanded="true">
							
							<span class="sidebar-nav-item-icon fa fa-bullhorn fa-lg notify"></span>
							<span class="sidebar-nav-item aText"><font color="grey" >Terms & Conditons</font></span>
							<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
						</li>
						
						<li >
									<a href="homePageBanner.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-camera fa-lg notify"></span>
									<span class="sidebar-nav-item aText"><font color="grey" >Home Page Banner</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
						</li>
				</ul>
			</nav>
		</aside>
	</div>
	<!-- # Sidebar-wrapper -->
	
	<!-- Page Content-wrapper -->
	<div id="page-content-wrapper">


<!-- Breadcrumb  -->
<div class="row csk-breadcrumb">
	<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
		<h4 class="page-title">Fuel Types</h4>
	</div>
	<div class="col-lg-9 col-md-8 col-sm-8 hidden-xs">
		<ol class="breadcrumb">
			<li><a href="dashboard.php">Dashboard</a></li>
			<li><a href="#">Fuel Types</a></li>
		</ol>
	</div>
</div>
<!-- #Breadcrumb -->
<!-- row -->
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-defaut">
			<div class="panel-body">
				<form action="dbclass.php" method="post" name="frmVehicleType" id="frmVehicleType">
					<div class="form-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									
									<label class="control-label">Fuel Type</label>
									<input type="text" class="form-control" placeholder="Fuel Name" name="fuelType_Name" id="fuelType_Name" value="<?php if(isset($_GET['f_id'])){ echo $data_fuelType['fuelType_Name'];} ?>">
									
									
									<br>
									<button type="submit" class="btn btn-success" name="fuelType_btn" id="fuelType_btn"> <i class="fa fa-check"></i> Save</button>
									<button type="reset" class="btn btn-danger">Reset</button>
									
								</div>
							</div>
							<div class="col-md-6" style="margin-top: 20px;">
								<div class="form-actions">
									<?php
										if(isset($_GET['f_id']))
										{
											echo "<input type='hidden' name='fuelType_id' id='fuelType_id' value='".$data_fuelType['fuelType_id']."'>";
										}
									?>
									<input type="hidden" class="form-control" placeholder="adminSignature" name="adminSignature" id="adminSignature" value="<?php $data['register_id']; ?>">
									
									
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- #row -->

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default panel-with-options">
			
			<h3>&emsp;Fuel List</h3>
			
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered datatable">
						<thead>
							<tr>
								<th>SR NO.</th>
								<th>Fuel Type</th>
								<th>Status</th>
								<th>Date Added</th>
								<th>Action</th> 	
							</tr>
						</thead>
						<tfoot>
						<tr>
							<th>SR NO.</th>
							<th>Fuel Type</th>
							<th>Status</th>
							<th>Date Added</th>
							<th>Action</th> 
						</tr>
						</tfoot>
						<tbody>
							
							<?php
							
								$sql_fuelType = "select * from fuelType_tbl WHERE delete_status ='0'";		
								$ans_fuelType = mysqli_query($obj->con,$sql_fuelType);
	 
								$counter = 1;
								while($row_fuelType = mysqli_fetch_array($ans_fuelType))
								{
							?>
								<tr>
								<td><?php echo $counter ++; ?></td>
								<td><?php echo $row_fuelType['fuelType_Name']; ?></td>
								<td>
									<?php 
									if($row_fuelType['is_active'] == 1)
									{?>
										<span class="label label-success"><a href="manageFuelList.php?stat=0&fuelType_id=<?php echo $row_fuelType['fuelType_id'];?>" style="color:white;">Active</a></span>
									<?php
									}
									else
									{?>
										<span class="label label-danger"><a href="manageFuelList.php?stat=1&fuelType_id=<?php echo $row_fuelType['fuelType_id'];?>" style="color:white;">De-Active</a></span>
									<?php
									}
									?>
									
								</td>
								<td><?php echo date("d-m-Y", strtotime($row_fuelType['added_date'])); ?></td>
								<td>
								<a href="manageFuelList.php?f_id=<?php echo $row_fuelType['fuelType_id']; ?>" title="EDIT"><i class="fa fa-pencil fa-1x "></i></a> || 
									<a href="manageFuelList.php?d=<?php echo $row_fuelType['fuelType_id']; ?>" title="DELETE"><i class="fa fa-trash fa-1x"></i></a></td>
								</tr>
							<?php
							}
							?>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
	include_once('footer.php');
?>