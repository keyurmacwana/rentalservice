<?php
	include_once('header.php');
	if(isset($_GET['is_active']))
	{
		
		$obj -> changeStatus_termsandconditionsList_tbl($_GET['is_active'],$_GET['termCategoryId']);
		
	}
	
	if(isset($_GET['termsAndConditionListDelete']))
	{
		$obj -> termsAndConditionListDelete($_GET['termsAndConditionListDelete']);
	}

	if(isset($_GET['termsAndConditionListEdit']))
	{	
		$data_termsAndConditionListEdit =  $obj -> termsAndConditionListEdit($_GET['termsAndConditionListEdit']);	
	}
	
?>
<!-- Main Body content starts here -->
        <div id="wrapper">
            <!-- Sidebar -->
            <div id="sidebar-wrapper">
                <aside class="sidebar">
                    <nav class="sidebar-nav" id="sidebarscroll">
                        <ul class="metismenu ripple" id="menu">
						
							<li  >
								<a href="dashboard.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-folder-open fa-lg notify"></span>
								<span class="sidebar-nav-item aText" ><font color="grey">Dashboard</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
	
							<li >
								<a href="vehicle_type.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-car fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey">Vehicle Type</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
							
							<li >
								<a href="vehicle_rates_category.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-rupee fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey" >Vehicle Category</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
							
							<li >
								<a href="manageFuelList.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-beer fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey" >Manage Fuel Type</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
							
							<li>
								<a href="register_owner.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-book fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey" >Manage Owner</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
							
							<li>
								<a href="newOwnerRequest.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-plus fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey" >New Owner Request</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
							
							<li style="background-color:#505464;">
								<a href="termsAndConditionList.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-pencil fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey" >Terms List</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
							
							<li >
								<a href="termsAndCondition.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-bullhorn fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey" >Terms & Conditons</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
							
							
							
							<li >
								<a href="homePageBanner.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-camera fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey" >Home Page Banner</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
						
                            
                        </ul>
                    </nav>
                </aside>
            </div>
            <!-- # Sidebar-wrapper -->
            
			<!-- Page Content-wrapper -->
			<div id="page-content-wrapper">
	
				<!-- Breadcrumb  -->
				<div class="row csk-breadcrumb">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
						<h4 class="page-title">Terms and Conditions</h4>
					</div>
					<div class="col-lg-9 col-md-8 col-sm-8 hidden-xs">
						<ol class="breadcrumb">
							<li><a href="dashboard.php">Dashboard</a></li>
							<li><a href="#">Terms and Conditions List</a></li>
						</ol>
					</div>
				</div>
				<!-- #Breadcrumb -->
	
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-defaut">
							<div class="panel-heading">
								<h3 class="panel-title">Add New Term Category</h3>
							</div>
							<div class="panel-body">
								<form action="dbclass.php" method="post" >
									<input type="hidden" name="adminSignature" id="adminSignature" value="<?php echo $data['register_id'];?>">
									
									<?php 
										if(isset($_GET['termsAndConditionListEdit']))
										{ 
											echo "<input type='hidden' name='termCategoryId' id='termCategoryId' value='".$data_termsAndConditionListEdit['termCategoryId']."'>";
										}
									?>
									
										<div class="row">
											<div class="col-md-4">
												<div class="form-group">
													<input type="text" id="termCategory" name="termCategory" class="form-control" value="<?php if(isset($_GET['termsAndConditionListEdit'])){echo $data_termsAndConditionListEdit['termCategory'];} ?>">
												</div>
											</div>
											
										</div>
									
									<div class="form-actions">
										<button type="submit" class="btn btn-success" name="termAndConditionAdd" id="termAndConditionAdd"> <i class="fa fa-check"></i> Save</button>
										<button type="button" class="btn btn-danger">Cancel</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- #row -->
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default panel-with-options">
			
				<h3 >&emsp;Terms and Conditions Category</h3>
			
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered datatable">
						<thead>
							<tr>
								<th>SR NO.</th>
								<th>Term Category</th>
								<th>Status</th>
								<th>Date Created</th>
								<th>Action</th>
							</tr>
						</thead>
						<tfoot>
						<tr>
								<th>SR NO.</th>
								<th>Term Category</th>
								<th>Status</th>
								<th>Date Created</th>
								<th>Action</th>
							</tr>
						</tfoot>
						<tbody>
							
							<?php 
								$sql_termsandconditions_tbl = "SELECT * FROM   termsandconditions_category WHERE delete_status=0";		
								$ans_termsandconditions_tbl = mysqli_query($obj->con,$sql_termsandconditions_tbl);
	
								$counter = 1;
								while($row_termsandconditions_tbl = mysqli_fetch_array($ans_termsandconditions_tbl))
								{
							?>
								<tr>
									<td><?php echo $counter ++; ?></td>
									<td><?php echo $row_termsandconditions_tbl['termCategory']; ?></td>
									
									<td><?php 
											if($row_termsandconditions_tbl['is_active'] == 1)
											{?>
												<span class="label label-success"><a href="termsAndConditionList.php?is_active=<?php echo $row_termsandconditions_tbl['is_active'];?>&termCategoryId=<?php echo $row_termsandconditions_tbl['termCategoryId']?>" style="color:white;">Active</a></span>
											<?php
											}
											else
											{?>
												<span class="label label-danger"><a href="termsAndConditionList.php?is_active=<?php echo $row_termsandconditions_tbl['is_active'];?>&termCategoryId=<?php echo $row_termsandconditions_tbl['termCategoryId']?>" style="color:white;">De-Active</a></span>
											<?php
											}
											?>
									</td>
									<td><?php echo $row_termsandconditions_tbl['dateCreated']; ?></td>
									<td>
										<a href="termsAndConditionList.php?termsAndConditionListEdit=<?php echo $row_termsandconditions_tbl['termCategoryId']; ?>"title="EDIT"><i class="fa fa-pencil fa-1x "></i></a> || <a href="termsAndConditionList.php?termsAndConditionListDelete=<?php echo $row_termsandconditions_tbl['termCategoryId']; ?>" title="DELETE"><i class="fa fa-trash fa-1x"></i></a>
									</td>
								</tr>
							<?php
							}
							?>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
				
			
<?php
	include_once('footer.php');
?>