<?php
	include_once('header.php');
	
	if(isset($_GET['acceptRequest']))
	{
		$obj -> acceptOwnerRequest($_GET['acceptRequest']);
	}

	if(isset($_GET['deleteOwnerRequest']))
	{
		$obj -> changeStatusVehicleOwner($_GET['is_active'],$_GET['vehicleOwnerId']);
	}
?>
<!-- Main Body content starts here -->
        <div id="wrapper">
            <!-- Sidebar -->
            <div id="sidebar-wrapper">
                <aside class="sidebar">
                    <nav class="sidebar-nav" id="sidebarscroll">
                        <ul class="metismenu ripple" id="menu">
						
								<li>
								<a href="dashboard.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-folder-open fa-lg notify"></span>
								<span class="sidebar-nav-item aText" ><font color="grey">Dashboard</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
	
							<li >
								<a href="vehicle_type.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-car fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey">Vehicle Type</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
							
							<li >
								<a href="vehicle_rates_category.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-rupee fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey" >Vehicle Category</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
							
							<li >
								<a href="manageFuelList.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-beer fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey" >Manage Fuel Type</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
							
							<li >
								<a href="register_owner.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-book fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey" >Manage Owner</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
							
							<li style="background-color:#505464;">
								<a href="newOwnerRequest.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-plus fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey" >New Owner Request</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
							
							<li >
								<a href="termsAndConditionList.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-pencil fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey" >Terms List</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
							
							<li >
								<a href="termsAndCondition.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-bullhorn fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey" >Terms & Conditons</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
							
							
							
							<li >
								<a href="homePageBanner.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-camera fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey" >Home Page Banner</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
                            
                        </ul>
                    </nav>
                </aside>
            </div>
            <!-- # Sidebar-wrapper -->
            
			<!-- Page Content-wrapper -->
			<div id="page-content-wrapper">

			
				<!-- Breadcrumb  -->
				<div class="row csk-breadcrumb">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
						<h4 class="page-title">Register Vehicle Owner</h4>
					</div>
					<div class="col-lg-9 col-md-8 col-sm-8 hidden-xs">
						<ol class="breadcrumb">
							<li><a href="dashboard.php">Dashboard</a></li>
							<li><a href="#">Register Vehicle Owner</a></li>
						</ol>
					</div>
				</div>
				<!-- #Breadcrumb -->
<!--row-->
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default panel-with-options">
			
				<h3 >&emsp;New Owner Registration Request</h3>
			
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered datatable">
						<thead>
							<tr>
								<th>SR NO.</th>
								<th>Date Requested</th>
								<th>User Name</th>
								<th>First Name</th>
								<th>Last Name</th>
								<th>Email</th>
								<th>Mobile Number</th>
								<th>Action</th>
							</tr>
						</thead>
						<tfoot>
						<tr>
								<th>SR NO.</th>
								<th>Date Requested</th>
								<th>User Name</th>
								<th>First Name</th>
								<th>Last Name</th>
								<th>Email</th>
								<th>Mobile Number</th>
								<th>Action</th>
							</tr>
						</tfoot>
						<tbody>
							
							<?php 
								$sql_ownerRequest_tbl = "select * 
													FROM vehicleownersignuprequest_tbl
													Order By date_requested DESC";
								
								$ans_ownerRequest_tbl = mysqli_query($obj->con,$sql_ownerRequest_tbl);
								
								$counter = 1;
								while($row_ownerRequest_tbl = mysqli_fetch_array($ans_ownerRequest_tbl))
								{
							?>
								<tr>
									<td><?php echo $counter ++; ?></td>
									<td><?php echo date("d-m-Y", strtotime($row_ownerRequest_tbl['date_requested'])); ?></td>
									<td><?php echo $row_ownerRequest_tbl['userName']; ?></td>
									<td><?php echo $row_ownerRequest_tbl['firstName']; ?></td>
									<td><?php echo $row_ownerRequest_tbl['lastName']; ?></td>
									<td><?php echo $row_ownerRequest_tbl['email']; ?></td>
									<td><?php echo $row_ownerRequest_tbl['mobileNumber']; ?></td>
									
									<td>
										<a href="newOwnerRequest.php?acceptRequest=<?php echo $row_ownerRequest_tbl['vehicleOwnerRequestId'];?>" title="DELETE"><i class="fa fa-plus fa-1x "></i></a> || <a href="newOwnerRequest.php?deleteOwnerRequest=<?php echo $row_ownerRequest_tbl['vehicleOwnerRequestId']; ?>" title="DELETE"><i class="fa fa-trash fa-1x"></i></a>
									</td>
								</tr>
							<?php
							}
							?>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>


			
<?php
	include_once('footer.php');
?>