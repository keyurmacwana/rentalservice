<?php
	session_start();
	include_once('dbclass.php');
	$obj = new dbclass;

	if(isset($_POST['submit'])){
		$file = $_FILES['imgUpload'];
//		print_r($file);

		$fileName = $_FILES['imgUpload']['name'];
		$fileTmpName = $_FILES['imgUpload']['tmp_name'];
		$fileSize = $_FILES['imgUpload']['size'];
		$fileError = $_FILES['imgUpload']['error'];
		$fileType = $_FILES['imgUpload']['type'];
		$fileExt = explode('.',$fileName);
		$fileActualExt = strtolower(end($fileExt));

//		echo $fileActualExt;

		$allowed = array('jpg','jpeg','png');
		$blank = "";

			if (in_array($fileActualExt, $allowed)) {
				if ($fileError === 0) {
					if ($fileSize < 100000000) {
						$fileNameNew = $_SESSION['uname'].".".$fileActualExt;
						$fileDestination = 'C:/xampp/htdocs/rentalservice/web/'.$fileNameNew;
						move_uploaded_file($fileTmpName, $fileDestination);
						$obj->update_adm_profile($_SESSION['uname'],$fileNameNew);
						$_SESSION['msg'] = "Profile upload success!";
						header("Location:profile.php?");

		
					}
					else{
						$_SESSION['msg'] = "File size too big!";
						header("Location:profile.php");
					}
				}
				else{
					$_SESSION['msg'] = "Error uploding file, please try again!";
					header("Location:profile.php");
				}
			} 

			else{
				$_SESSION['msg'] = "Please select proper file";
				header("Location:profile.php");
			}
		
	}
?>