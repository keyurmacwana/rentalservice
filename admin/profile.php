<?php
	include_once('header.php');
?>
<!-- Main Body content starts here -->
        <div id="wrapper">
            <!-- Sidebar -->
            <div id="sidebar-wrapper">
                <aside class="sidebar">
                    <nav class="sidebar-nav" id="sidebarscroll">
                        <ul class="metismenu ripple" id="menu">
						
								<li>
									<a href="dashboard.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-folder-open fa-lg notify"></span>
									<span class="sidebar-nav-item aText" ><font color="grey">Dashboard</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
	
								<li >
									<a href="vehicle_type.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-car fa-lg notify"></span>
									<span class="sidebar-nav-item aText"><font color="grey">Vehicle Type</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li >
									<a href="vehicle_rates_category.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-rupee fa-lg notify"></span>
									<span class="sidebar-nav-item aText"><font color="grey" >Rates and Category</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li>
									<a href="register_owner.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-book fa-lg notify"></span>
									<span class="sidebar-nav-item aText"><font color="grey" >Manage Owner</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li>
									<a href="newOwnerRequest.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-plus fa-lg notify"></span>
									<span class="sidebar-nav-item aText"><font color="grey" >New Owner Request</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li >
									<a href="termsAndConditionList.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-pencil fa-lg notify"></span>
									<span class="sidebar-nav-item aText"><font color="grey" >Terms List</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li >
									<a href="termsAndCondition.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-bullhorn fa-lg notify"></span>
									<span class="sidebar-nav-item aText"><font color="grey" >Terms & Conditons</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
								
								<li >
									<a href="homePageBanner.php" aria-expanded="true">
									
									<span class="sidebar-nav-item-icon fa fa-camera fa-lg notify"></span>
									<span class="sidebar-nav-item aText"><font color="grey" >Home Page Banner</font></span>
									<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
								</li>
                            
                        </ul>
                    </nav>
                </aside>
            </div>
            <!-- # Sidebar-wrapper -->
            
			<!-- Page Content-wrapper -->
			<div id="page-content-wrapper">

<?php
	clearstatcache();
	
?>
<?php
//	include_once('dbclass.php');
//	$ss = ;
	//echo $ss;
	$sql = "SELECT * FROM register_tbl where username = '".$_SESSION['uname']."'";
//	echo "<h1>".$sql."</h1>";
	$obj = new dbclass();
	$rs = mysqli_query($obj->con,$sql);
	//echo $rs;
	$row = mysqli_fetch_array($rs);
?>

<!-- Breacrumb -->
				<div class="row csk-breadcrumb">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
						<h4 class="page-title">Profile</h4>
					</div>
					<div class="col-lg-9 col-md-8 col-sm-8 hidden-xs">
						<ol class="breadcrumb">
							<li><a href="dashboard.php">Dashboard</a></li>
							<li><a href="#">Profile</a></li>
						</ol>
					</div>
				</div>
				<!-- # Breadcrumb -->
				<!-- Profile -->
				<div class="profile">
					<div class="row">
						<div class="col-md-12 profile">
							<div class="cover-footer">
								<div class="google-panel">
									<div class="panel panel-danger contacts">
										<div class="panel-heading">
											<div class="cover-picture"></div>
											<div class="profile-line">
												<div class="profile-picture">
													<?php echo "<img src='../web/".$data['profile']."' class='img-responsive' alt='avatar' width='100px' height='50px'>" ;?>
												</div>
												<h3><?php echo $_SESSION['uname'];?></h3>
											</div>
											
										</div>
										<div class="panel-tabs">
											<ul class="nav nav-tabs">
												<li class="active"><a href="#tab1default" data-toggle="tab">Settings</a></li>
												<li><a href="#tab2default" data-toggle="tab">Change Password</a></li>
											</ul>
										</div>
										<div class="panel-body">
											<div class="tab-content">
												<div class="tab-pane fade in active" id="tab1default">
													<main id="home" class="contents">
													<div class="col-md-6">
														<?php echo $_SESSION['msg'];$_SESSION['msg'] = ""?>
														<form action="dbclass.php" method="post" enctype="multipart/form-data">

																<div class="row">
                                   
                                        
                                        
										<img 
										src="<?php if($data['profile'] != "")
										{
											echo "../web/".$data['profile'] ; 
										}
										else
										{
											 echo "..datas/images/coverimg.jpg";
										}?>" 
										id="realimage" height="60px" width="100px" onclick="triggerclick()">
										
                                        <input type="file" name="imgUpload" id="imgUpload" onchange="displayimage(this)" style="display: none">
                                    </div>

																<div class="form-group">
																	<label class="control-label">User Name</label>
																	<input type="text" id="username1" name="username1" class="form-control" value="<?php echo $row['username'] ?>" disabled="disabled">
																	<input type="hidden" id="username" name="username" class="form-control" value="<?php echo $row['username'] ?>">
																</div>
																<div class="form-group">
																	<label class="control-label">Full Name</label>
																	<input type="text" id="full_name" name="full_name" class="form-control" value="<?php echo $row['full_name'] ?>">
																</div>
													</div>
													<div class="col-md-6">
																<div class="form-group">
																	<label class="control-label">Email </label>
																	<input type="email" id="email" name="email" class="form-control" value="<?php echo $row['email'] ?>">
																</div>
																<div class="form-group">
																	<label class="control-label">Mobile</label>
																	<input type="number" id="mobile" name="mobile" class="form-control" value="<?php echo $row['mobile'] ?>">
																</div>
																<hr>
													</div>
													<div class="col-md-12">
														<div>
																<button type="submit" id="update_admin" name="update_admin" class="btn btn-success">Save Changes</button>
														</div>

														</form>
													</div>
													</main>
												</div>
												<div class="tab-pane fade" id="tab2default">
													<div class="row">
														<div class="col-md-6">
															<form action="dbclass.php" method="post">
																				<div class="form-group">
																					<label class="control-label">Enter Password</label>
																					<input type="password" id="pwd" name="pwd" class="form-control">
																				</div>
																				<div class="form-group">
																					<label class="control-label">ReEnter Password</label>
																					<input type="password" id="pwd2" name="pwd2" class="form-control">
																				</div>
																				<div class="form-group">
																					<button type="changePwd" id="changePwd" name="changePwd" class="btn btn-success">Change Password</button>
																				</div>
																				<input type="hidden" name="uname" id="uname" <?php echo "value='".$_SESSION['uname']."'";?> >
															</form>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
						 <script type="text/javascript">
        function triggerclick(){
            document.querySelector('#imgUpload').click();
        }
        function displayimage(e){
            if(e.files[0]){
                var reader = new FileReader();
                reader.onload = function(e){
                    document.querySelector('#realimage').setAttribute('src', e.target.result);
                }
                reader.readAsDataURL(e.files[0]);
            }
        }
        </script>

        <script type="text/javascript">
        function triggerclickmulti(){
            document.querySelector('#gallery-photo-add').click();
        }
        </script>
        <script type="text/javascript">
            function  change_subcategory(){
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.open("GET","ajax.php?subcat=" + document.getElementById("category").value,false);
                xmlhttp.send(null);
                document.getElementById("psize").innerHTML=xmlhttp.responseText;
            }
        </script>

<?php
	include_once('footer.php');
?>