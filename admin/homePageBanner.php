<?php
	include_once('header.php');
	
//DELTE Banner 
	if(isset($_GET['d']))
	{
		$obj -> deleteHomePageBanner($_GET['d']);
		
	}
//END delete banner

//BEGIN delete banner
	if(isset($_GET['editHomePageBanner']))
	{	
		$editHomePageBanner = $_GET['editHomePageBanner'];
		$sql_editHomePageBanner = "SELECT * FROM home_page_banner WHERE bannerId = '$editHomePageBanner'";
		$ans_editHomePageBanner = mysqli_query($obj->con,$sql_editHomePageBanner);
		$row_editHomePageBanner = mysqli_fetch_array($ans_editHomePageBanner);	
	}
//END delete banner

//BEGIN CHANGE STATUS
	if(isset($_GET['is_active']))
	{
		$obj -> changeHomePageBannerStatus($_GET['is_active'],$_GET['bannerId']);
		
	}
//END CHANGE STATUS
?>
<!-- Main Body content starts here -->
        <div id="wrapper">
            <!-- Sidebar -->
            <div id="sidebar-wrapper">
                <aside class="sidebar">
                    <nav class="sidebar-nav" id="sidebarscroll">
                        <ul class="metismenu ripple" id="menu">
						
							<li >
								<a href="dashboard.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-folder-open fa-lg notify"></span>
								<span class="sidebar-nav-item aText" ><font color="grey">Dashboard</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
	
							<li >
								<a href="vehicle_type.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-car fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey">Vehicle Type</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
							
							<li >
								<a href="vehicle_rates_category.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-rupee fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey" >Vehicle Category</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
							
							<li >
								<a href="manageFuelList.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-beer fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey" >Manage Fuel Type</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
							
							<li>
								<a href="register_owner.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-book fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey" >Manage Owner</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
							
							<li>
								<a href="newOwnerRequest.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-plus fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey" >New Owner Request</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
							
							<li >
								<a href="termsAndConditionList.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-pencil fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey" >Terms List</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
							
							<li >
								<a href="termsAndCondition.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-bullhorn fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey" >Terms & Conditons</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
							
							
							
							<li  style="background-color:#505464;">
								<a href="homePageBanner.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-camera fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey" >Home Page Banner</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
						
								
                            
                        </ul>
                    </nav>
                </aside>
            </div>
            <!-- # Sidebar-wrapper -->
            
			<!-- Page Content-wrapper -->
			<div id="page-content-wrapper">


<!-- Breadcrumb  -->
<div class="row csk-breadcrumb">
	<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
		<h4 class="page-title">Home Page Banner</h4>
	</div>
	<div class="col-lg-9 col-md-8 col-sm-8 hidden-xs">
		<ol class="breadcrumb">
			<li><a href="dashboard.php">Dashboard</a></li>
			<li><a href="#">Home Page Banner</a></li>
		</ol>
	</div>
</div>
<!-- #Breadcrumb -->
<!-- row -->
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-defaut">
			<div class="panel-body">
				<form action="dbclass.php" method="post" enctype="multipart/form-data">
					
					<input type="hidden" name="adminSignature" id="adminSignature" value="<?php echo $data['register_id'];?>">
					<?php if(isset($_GET['editHomePageBanner']))
							{
					?>
					<input type="hidden" name="bannerId" id="bannerId" value="<?php echo $row_editHomePageBanner['bannerId'];?>">
					<input type="hidden" name="oldImgName" id="oldImgName" value="<?php echo $row_editHomePageBanner['bannerImageLink'];?>">
					<?php
							}
					?>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label">Main Text Front</label>
								<input type="text" class="form-control" placeholder="Main Text #1" name="mainText1" id="mainText1" value="<?php if(isset($_GET['editHomePageBanner'])){echo $row_editHomePageBanner['mainText1'];}?>">
								
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label">Main Text Highlight</label>
								<input type="text" class="form-control" placeholder="Main Text #2" name="mainText2" id="mainText2" value="<?php if(isset($_GET['editHomePageBanner'])){echo $row_editHomePageBanner['mainText2'];}?>">
								
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label">Main Text End</label>
								<input type="text" class="form-control" placeholder="Main Text #3" name="mainText3" id="mainText3" value="<?php if(isset($_GET['editHomePageBanner'])){echo $row_editHomePageBanner['mainText3'];}?>">
								
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="control-label">Descrptive Text</label>
								<input type="text" class="form-control" placeholder="eg:Quality service is our goal" name="subText" id="subText" value="<?php if(isset($_GET['editHomePageBanner'])){echo $row_editHomePageBanner['subText'];}?>">
								
							</div>	
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label">Link Text</label>
								<input type="text" class="form-control" placeholder="eg:Contact Us" name="hrefText" id="hrefText" value="<?php if(isset($_GET['editHomePageBanner'])){echo $row_editHomePageBanner['hrefText'];}?>">
								
							</div>	
						</div>
						<div class="col-md-8">
							<div class="form-group">
								<label class="control-label">Link</label>
								<input type="text" class="form-control" placeholder="http://Link" name="hrefLink" id="hrefLink" value="<?php if(isset($_GET['editHomePageBanner'])){echo $row_editHomePageBanner['hrefLink'];}?>">
								
							</div>	
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<label class="control-label">Upload Image</label>
							<input type="file" id="bannerImageLink" name="bannerImageLink" class="form-control"   accept="image/*">
						</div>
					</div>
					<div class="form-actions">
						<button type="submit" class="btn btn-success" name="homePageBanner_btn" id="homePageBanner_btn"> <i class="fa fa-check"></i> Save</button>
						<button type="button" class="btn btn-danger">Cancel</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- #row -->

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default panel-with-options">
			
				<h3 >&emsp;Home Page Banner</h3>
			
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered datatable">
						<thead>
							<tr>
								<th>SR NO.</th>
								<th>Status</th>
								<th>Main Text</th>
								<th>Sub Text</th>
								<th>Link Text</th>
								<th>Link</th>
								<th>Date Added</th>
								<th>Action</th>	
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>SR NO.</th>
								<th>Status</th>
								<th>Main Text</th>
								<th>Sub Text</th>
								<th>Link Text</th>
								<th>Link</th>
								<th>Date Added</th>
								<th>Action</th>
							</tr>
						</tfoot>
						<tbody>
							
							<?php
							
								$sql_home_page_banner = "SELECT * FROM home_page_banner WHERE delete_status=0";		
								$ans_home_page_banner = mysqli_query($obj->con,$sql_home_page_banner);
	 
								$counter = 1;
								while($row_home_page_banner = mysqli_fetch_array($ans_home_page_banner))
								{
									$bannerId = $row_home_page_banner['bannerId'];
							?>
								<tr>
								<td><?php echo $counter ++; ?></td>
								<td>
									<?php 
									if($row_home_page_banner['is_active'] == 1)
									{?>
										<span class="label label-success"><a href="homePageBanner.php?is_active=<?php echo $row_home_page_banner['is_active'];?>&bannerId=<?php echo $row_home_page_banner['bannerId'];?>" style="color:white;">Active</a></span>
									<?php
									}
									else
									{?>
										<span class="label label-danger"><a href="homePageBanner.php?is_active=<?php echo $row_home_page_banner['is_active'];?>&bannerId=<?php echo $row_home_page_banner['bannerId'];?>" style="color:white;">De-Active</a></span>
									<?php
									}
									?>
									
								</td>
								<td><?php echo $row_home_page_banner['mainText1']." <b>".$row_home_page_banner['mainText2']." </b>".$row_home_page_banner['mainText3']; ?></td>
								
								<td><?php echo $row_home_page_banner['subText'];?></td>
								<td><?php echo $row_home_page_banner['hrefText'];?></td>
								<td><?php echo $row_home_page_banner['hrefLink'];?></td>
								<td><?php echo date("d-m-Y", strtotime($row_home_page_banner['added_date'])); ?></td>
								
								<td>
								<a href="homePageBanner.php?editHomePageBanner=<?php echo $row_home_page_banner['bannerId']; ?>" title="EDIT"><i class="fa fa-pencil fa-1x "></i></a> || 
									<a href="homePageBanner.php?d=<?php echo $row_home_page_banner['bannerId']; ?>" title="DELETE"><i class="fa fa-trash fa-1x"></i></a></td>
								</tr>
							<?php
							}
							?>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
	include_once('footer.php');
?>