<?php
	include_once('header.php');
	
	
	
	if(isset($_GET['is_active']))
	{
		
		$obj -> changeStatusVehicleOwner($_GET['is_active'],$_GET['vehicleOwnerId']);
		
	}
	
	if(isset($_GET['vehicleOwnerIdDelete']))
	{
		$obj -> deleteVehicleOwner($_GET['vehicleOwnerIdDelete']);
	}

	if(isset($_GET['vehicleOwnerIdEdit']))
	{
		$data_vehicleOwnerIdEdit =  $obj -> vehicleOwnerIdEdit($_GET['vehicleOwnerIdEdit']);	
	}
?>
<!-- Main Body content starts here -->
        <div id="wrapper">
            <!-- Sidebar -->
            <div id="sidebar-wrapper">
                <aside class="sidebar">
                    <nav class="sidebar-nav" id="sidebarscroll">
                        <ul class="metismenu ripple" id="menu">
						
							<li>
								<a href="dashboard.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-folder-open fa-lg notify"></span>
								<span class="sidebar-nav-item aText" ><font color="grey">Dashboard</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
	
							<li >
								<a href="vehicle_type.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-car fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey">Vehicle Type</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
							
							<li >
								<a href="vehicle_rates_category.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-rupee fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey" >Vehicle Category</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
							
							<li >
								<a href="manageFuelList.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-beer fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey" >Manage Fuel Type</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
							
							<li style="background-color:#505464;">
								<a href="register_owner.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-book fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey" >Manage Owner</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
							
							<li>
								<a href="newOwnerRequest.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-plus fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey" >New Owner Request</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
							
							<li >
								<a href="termsAndConditionList.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-pencil fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey" >Terms List</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
							
							<li >
								<a href="termsAndCondition.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-bullhorn fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey" >Terms & Conditons</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
							
							
							
							<li >
								<a href="homePageBanner.php" aria-expanded="true">
								
								<span class="sidebar-nav-item-icon fa fa-camera fa-lg notify"></span>
								<span class="sidebar-nav-item aText"><font color="grey" >Home Page Banner</font></span>
								<span class="ink animate" style="height: 220px; width: 220px; top: -92px; left: 82px;"></span></a>
							</li>
                            
                        </ul>
                    </nav>
                </aside>
            </div>
            <!-- # Sidebar-wrapper -->
            
			<!-- Page Content-wrapper -->
			<div id="page-content-wrapper">

			
				<!-- Breadcrumb  -->
				<div class="row csk-breadcrumb">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
						<h4 class="page-title">Register Vehicle Owner</h4>
					</div>
					<div class="col-lg-9 col-md-8 col-sm-8 hidden-xs">
						<ol class="breadcrumb">
							<li><a href="dashboard.php">Dashboard</a></li>
							<li><a href="#">Register Vehicle Owner</a></li>
						</ol>
					</div>
				</div>
				<!-- #Breadcrumb -->
				<!-- row -->
				
				<!-- row -->
			
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-defaut">
							<div class="panel-heading">
								<h3 class="panel-title">Register Vehicle Owner</h3>
							</div>
							<div class="panel-body">
								<form action="dbclass.php" method="post" >
									<input type="hidden" name="adminSignature" id="adminSignature" value="<?php echo $data['register_id'];?>">
									<?php 
										if(isset($_GET['vehicleOwnerIdEdit']))
										{ 
											echo "<input type='hidden' name='vehicleOwnerId' id='vehicleOwnerId' value='".$data_vehicleOwnerIdEdit['vehicleOwnerId']."'>";
										} 
									?>
									<div class="form-body">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label">First Name</label>
													<input type="text" class="form-control" placeholder="First Name" name="firstName" id="firstName" value="<?php if(isset($_GET['vehicleOwnerIdEdit'])){ echo $data_vehicleOwnerIdEdit['firstName'];} ?>">
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label">Last Name</label>
													<input type="text" class="form-control" placeholder="Last Name" name="lastName" id="lastName" value="<?php if(isset($_GET['vehicleOwnerIdEdit'])){ echo $data_vehicleOwnerIdEdit['lastName'];} ?>">
												</div>
											</div>
										</div>
										
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label">Username</label>
													<input type="text" class="form-control" placeholder="Username" name="userName" id="userName" value="<?php if(isset($_GET['vehicleOwnerIdEdit'])){ echo $data_vehicleOwnerIdEdit['userName'];} ?>">
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label">Password</label>
													<input type="text" class="form-control" placeholder="Password" name="password" id="password" >
												</div>
											</div>
										</div>
										
										<div class="row">
											
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label">Email</label>
													<input type="email" class="form-control" placeholder="email" name="email" id="email" value="<?php if(isset($_GET['vehicleOwnerIdEdit'])){ echo $data_vehicleOwnerIdEdit['email'];} ?>">
												</div>
											</div>
											
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label">Mobile</label>
													<input type="number" class="form-control" placeholder="Mobile" name="mobileNumber" id="mobileNumber" value="<?php if(isset($_GET['vehicleOwnerIdEdit'])){ echo $data_vehicleOwnerIdEdit['mobileNumber'];} ?>">
												</div>
											</div>
											
										</div>
										
										
										
										
										
										<div class="row">
											
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label">Status</label>
													<select name="is_active" class="form-control"  id="is_active">
														<option value="0" 
														<?php 
															if(isset($_GET['vehicleOwnerIdEdit']))
															{
																if($data_vehicleOwnerIdEdit['is_active'] == 0) 
																	{echo "selected";}
															} 
														?>>De-Active</option>
														<option value="1"
														<?php 
															if(isset($_GET['vehicleOwnerIdEdit']))
															{
																if($data_vehicleOwnerIdEdit['is_active'] == 1) 
																	{echo "selected";}
															} 
														?>>Active</option>
													</select>
												</div>
											</div>
										</div>
									</div>
									
								
									
									<div class="form-actions">
										<button type="submit" class="btn btn-success" name="ownerRegister" id="ownerRegister"> <i class="fa fa-check"></i> Save</button>
										<button type="button" class="btn btn-danger">Cancel</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			
				<!-- #row -->
				
				
				
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default panel-with-options">
			
				<h3 >&emsp;Registered Vehicle Owner List</h3>
			
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered datatable">
						<thead>
							<tr>
								<th>SR NO.</th>
								<th>User Name</th>
								<th>Status</th>
								<th>First Name</th>
								<th>Last Name</th>
								<th>Email</th>
								<th>Mobile Number</th>
								<th>Admin Signature</th>
								<th>Date Created</th>
								<th>Action</th>
							</tr>
						</thead>
						<tfoot>
						<tr>
								<th>SR NO.</th>
								<th>User Name</th>
								<th>Status</th>
								<th>First Name</th>
								<th>Last Name</th>
								<th>Email</th>
								<th>Mobile Number</th>
								<th>Admin Signature</th>
								<th>Date Created</th>
								<th>Action</th>
							</tr>
						</tfoot>
						<tbody>
							
							<?php 
								$sql_owner_tbl = "select vo.*, admn.username from owner_tbl vo 
													LEFT JOIN register_tbl admn on vo.adminSignature = admn.register_id 
													WHERE vo.delete_status = '0'
													Order By vo.dateCreated DESC";
								
								$ans_owner_tbl = mysqli_query($obj->con,$sql_owner_tbl);
								
								$counter = 1;
								while($row_owner_tbl = mysqli_fetch_array($ans_owner_tbl))
								{
							?>
								<tr>
									<td><?php echo $counter ++; ?></td>
									<td><?php echo $row_owner_tbl['userName']; ?></td>
									<td><?php 
											if($row_owner_tbl['is_active'] == 1)
											{?>
												<span class="label label-success"><a href="register_owner.php?is_active=<?php echo $row_owner_tbl['is_active'];?>&vehicleOwnerId=<?php echo $row_owner_tbl['vehicleOwnerId']?>" style="color:white;">Active</a></span>
											<?php
											}
											else
											{?>
												<span class="label label-danger"><a href="register_owner.php?is_active=<?php echo $row_owner_tbl['is_active'];?>&vehicleOwnerId=<?php echo $row_owner_tbl['vehicleOwnerId']?>" style="color:white;">De-Active</a></span>
											<?php
											}
											?>
									</td>
									<td><?php echo $row_owner_tbl['firstName']; ?></td>
									<td><?php echo $row_owner_tbl['lastName']; ?></td>
									<td><?php echo $row_owner_tbl['email']; ?></td>
									<td><?php echo $row_owner_tbl['mobileNumber']; ?></td>
									<td><?php echo $row_owner_tbl['userName']; ?></td>
									<td><?php echo date("d-m-Y", strtotime($row_owner_tbl['dateCreated'])); ?></td>
									<td>
										<a href="register_owner.php?vehicleOwnerIdEdit=<?php echo $row_owner_tbl['vehicleOwnerId']; ?>title="EDIT"><i class="fa fa-pencil fa-1x "></i></a> || <a href="register_owner.php?vehicleOwnerIdDelete=<?php echo $row_owner_tbl['vehicleOwnerId']; ?>" title="DELETE"><i class="fa fa-trash fa-1x"></i></a>
									</td>
								</tr>
							<?php
							}
							?>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
				
			
<?php
	include_once('footer.php');
?>