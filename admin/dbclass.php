<?php
	
	class dbclass
	{
		var $con, $row_data , $row_vehicle_type;
		
		function __construct()
		{
			$this->con = mysqli_connect('localhost','root','');
			if(!$this->con)
			{
				die("Connection no established.".mysqli_error($this->con));
			}
			
			$this->db = mysqli_select_db($this->con,"rentalservice");
		}
		
		
		
		function getLogin($uname,$pwd)
		{
		
			$sql = "SELECT * FROM  register_tbl WHERE username = '".$uname."' AND password = '".$pwd."' ";
			
			$ans = mysqli_query($this->con,$sql);
			$row = mysqli_fetch_array($ans);
			
			if(mysqli_num_rows($ans) > 0) 
			{			
				session_start();
				
				$_SESSION['uname'] = $uname;
				//$_SESSION['profilePhoto'] = $row['profile'];
				//$_SESSION['pwd'] = $row['password'];
				$_SESSION['msg'] = "";

				header('location:dashboard.php');
			} 
			
			else
			{
				echo "Invalid Username or Password!";
				header('location:adminLogin.php');
			}
	
		}
		
		function getAdminData($uname)
		{
			
			$sql = "SELECT * FROM  register_tbl WHERE username = '".$uname."' LIMIT 1";
			
			$ans = mysqli_query($this->con,$sql);
			
			$row_data = mysqli_fetch_array($ans); 
			
			return $row_data;
		}

		function getVehicleRates2()
		{
			$sql = "SELECT * FROM  vehiclecatagorymopad ";
			
			$ans = mysqli_query($this->con,$sql); 
			
			return $ans;
		}

		function getVehicleRates4()
		{
			$sql = "SELECT * FROM  vehiclecatagorycar";
			
			$ans = mysqli_query($this->con,$sql);
			
			return $ans;
		}
		
		function update_adm($uname,$full_name,$email,$mobile,$file)
		{
			session_start();

			

			$_SESSION['msg'] = "No file selected!";

			$sql = "UPDATE register_tbl SET full_name = '".$full_name."' , email = '".$email."' , mobile = '".$mobile."'  ";
			if(!empty($_FILES["imgUpload"]["name"]))
			{
				$img=$_FILES["imgUpload"]["name"];

				//echo $img."</br>";
				$ext = pathinfo($img, PATHINFO_EXTENSION);


				//echo $ext."</br>";
				$img = basename($img,".".$ext);
				//echo $img."</br>";

				$img = rand(1111111111,9999999999).$img.rand(1111111111,9999999999).".".$ext;
				//echo $img."</br>";


				$tmp_name=$_FILES["imgUpload"]["tmp_name"];

				if(is_uploaded_file($tmp_name))
				{
					copy($tmp_name,"../web/".$img);
					$sql = $sql. ", profile = '".$img."' ";
					$_SESSION['msg'] = "Photo updated successfuly!";
				}
				
			}	

			$sql = $sql. " WHERE username = '".$_SESSION['uname']."'";
			

			$rs = mysqli_query($this->con,$sql);
			
			if(!$rs)
			{
				die ("update failed:".mysqli_error($this->con));
			}

			else
			{
				header('location:profile.php');

			} 
		}#end of admin update

		

		function update_adminPwd($pwd,$pwd2,$uname)
		{
			
			$sql = "UPDATE register_tbl SET password = '$pwd' WHERE username = '$uname'";
			
			$rs = mysqli_query($this->con,$sql);
			
			if(!$rs)
			{
				die ("update failed:".mysqli_error($this->con));
			}

			else
			{

				$_SESSION['msg'] = "Password Successfully changed";
				header('location:profile.php');

//				echo $sql;
				
			} 
		}

		function passwordAdminReset($pwd,$pwd2,$uname)
		{
			
			$sql = "UPDATE register_tbl SET password = '$pwd' WHERE username = '$uname'";
			
			$rs = mysqli_query($this->con,$sql);
			
			if(!$rs)
			{
				die ("update failed:".mysqli_error($this->con));
			}

			else
			{
			header('location:adminLogin.php?passwordUpdated');

//				echo $sql;
				
			} 
		}

		function forgotAdminPassword($userName,$recoveryemail)
		{
			$sql = "SELECT * FROM  register_tbl WHERE username = '".$userName."' AND email = '".$recoveryemail."' ";
			
			$ans = mysqli_query($this->con,$sql);
			$row = mysqli_fetch_array($ans);
			
			if(mysqli_num_rows($ans) > 0)
			{
				$userName = base64_encode($userName);
				$timeStamp = time();
				$link = "http://localhost/rentalservice/admin/passwordRecover.php?userName=".$userName;
				$link .= "&var1=".$timeStamp;
				$to = $recoveryemail;
				$subject = "Password recovery request!";
				$message = "You have requested for password recovery. If you did not apply for this please change your password immediately and repot the issue.
Your password is : ".$pwd."
Reset link: ".$link."
This link will expire in 10 minutes";
//				$headers = "From: your@email-address.com";

				if (mail($to, $subject, $message)) {
					header('Location:adminLogin.php?msg=An Email has been sent, please check your email inbox!');
				}
				else
				{
					header('Location:forgot.php?msg=email sending failed!');
				}
				
			}

			else{
				header('Location:forgot.php?msg=Username or Email incorrect!');
			}
		}#end of Password Reset
		
		
		
		//Start Vehicle Type
		function insertVehicleType($vehicle_type,$active_date)
		{
			$sql = "INSERT INTO vehicle_type_tbl (vehicle_type,active_date) values ('$vehicle_type','$active_date')";
			
			$rs = mysqli_query($this->con,$sql);
			
			if(!$rs)
			{
				die ("Not Inserted Vehicle Type.".mysqli_error($this->con));
			}

			else
			{
				header('location:vehicle_type.php');
			} 
			
		}
		
		function updateVehicleType($vehicle_type_id,$vehicle_type,$active_date)
		{
			$sql = "UPDATE vehicle_type_tbl SET vehicle_type = '$vehicle_type' , active_date = '$active_date' WHERE vehicle_type_id = '$vehicle_type_id' ";
			
			$rs = mysqli_query($this->con,$sql);
			
			if(!$rs)
			{
				die ("Not Updated Vehicle Type.".mysqli_error($this->con));
			}

			else
			{
				header('location:vehicle_type.php');
			} 
		}
		
		function deleteVehicleType($vehicle_type_id)
		{
			$sql_vehicle_type	=	"UPDATE vehicle_type_tbl SET delete_status = '1' WHERE vehicle_type_id = '$vehicle_type_id' ";
			
			$ans_vehicle_type = mysqli_query($this->con,$sql_vehicle_type);
			
			if(!$ans_vehicle_type)
			{
				die ("Not Deleted Vehicle Type.".mysqli_error($this->con));
			}

			else
			{
				//header('location:vehicle_type.php');
				echo "<script>window.location = 'vehicle_type.php'</script>";
			}
		}
		
		function getVehicleTypeById($vehicle_type_id)
		{
			
			$sql_vehicle_type = "SELECT * FROM  vehicle_type_tbl WHERE vehicle_type_id = '$vehicle_type_id'  ";
			
			$ans_vehicle_type = mysqli_query($this->con,$sql_vehicle_type);
			
			$row_vehicle_type = mysqli_fetch_array($ans_vehicle_type); 
			
			return $row_vehicle_type;
		}
		
		function changeStatusVehicleType($stat,$vehicle_type_id)
		{
			if($stat == 1)
			{
				$stat = 0;
				$sql = "UPDATE vehicle_type_tbl SET is_active = '$stat' WHERE vehicle_type_id = '$vehicle_type_id' ";
			}
			
			else
			{
				$stat = 1;
				$sql = "UPDATE vehicle_type_tbl SET is_active = '$stat' WHERE vehicle_type_id = '$vehicle_type_id' ";
			}
			$rs = mysqli_query($this->con,$sql);
			
			if(!$rs)
			{
				die ("Status Not Updated Vehicle Type.".mysqli_error($this->con));
			}

			else
			{
				echo "<script>window.location = 'vehicle_type.php'</script>";
			} 
		}
		
		//End Vehicle Type
		
		
		//Start vehicle category
		
		function insertVehicleCategory($vehicle_type_id,$vehicle_category)
		{
			$sql = "INSERT INTO vehicle_category_tbl (vehicle_type_id,vehicle_category) values ('$vehicle_type_id','$vehicle_category')";
			
			echo "sql = ".$sql;
			
			$rs = mysqli_query($this->con,$sql);
			
			if(!$rs)
			{
				echo "<br>vehicle_type_id = ".$vehicle_type_id;
				die ("Not Inserted Vehicle Category.".mysqli_error($this->con));
			}

			else
			{
				header('location:vehicle_rates_category.php');
			} 
			
		}
		
		function updateVehicleCategory($vehicle_category_id,$vehicle_type_id,$vehicle_category)
		{
			$sql = "UPDATE vehicle_category_tbl SET vehicle_type_id = '$vehicle_type_id' , vehicle_category = '$vehicle_category'  WHERE vehicle_category_id = '$vehicle_category_id' ";
			
			$rs = mysqli_query($this->con,$sql);
			
			if(!$rs)
			{
				die ("Not Updated Vehicle Category.".mysqli_error($this->con));
			}

			else
			{
				
				header('location:vehicle_rates_category.php');
			} 
		}
		
		function deleteVehicleCategory($vehicle_category_id)
		{
			$sql = "UPDATE vehicle_category_tbl SET delete_status = '1' WHERE vehicle_category_id = '$vehicle_category_id'";
			
			$rs = mysqli_query($this->con,$sql);
			
			if(!$rs)
			{
				die ("Not DELETED Vehicle Category.".mysqli_error($this->con));
			}

			else
			{	
				echo "<script>window.location = 'vehicle_rates_category.php'</script>";
			} 
		}
		
		
		
		function getVehicleCategoryById($vehicle_category_id)
		{
			
			$sql_vehicle_category = "SELECT * FROM  vehicle_category_tbl WHERE vehicle_category_id = '$vehicle_category_id'  ";
			
			$ans_vehicle_category = mysqli_query($this->con,$sql_vehicle_category);
			
			$row_vehicle_category = mysqli_fetch_array($ans_vehicle_category); 
			
			return $row_vehicle_category;
		}
		
		function changeStatusVehicleCategory($stat,$vehicle_category_id)
		{
			
			$sql = "UPDATE vehicle_category_tbl SET is_active = '$stat' WHERE vehicle_category_id = '$vehicle_category_id' ";
			
			$rs = mysqli_query($this->con,$sql);
			
			if(!$rs)
			{
				die ("Status Not Updated Vehicle Type.".mysqli_error($this->con));
			}

			else
			{
				echo "<script>window.location = 'vehicle_rates_category.php'</script>";
			} 
		}
		
		
		//End vehicle category
		
//owner

		//create owner
		function registerOwner($adminSignature,$firstName,$lastName,$email,$userName,$password,$mobileNumber,$is_active)
		{
			
			$sql = "INSERT INTO owner_tbl (adminSignature,firstName,lastName,email,userName,mobileNumber,is_active) values ('$adminSignature','$firstName','$lastName','$email','$userName','$mobileNumber','$is_active')";
			
			$rs = mysqli_query($this->con,$sql);
			
			if(!$rs)
			{
				die ("Not Registered Owner.".mysqli_error($this->con));
			}

			else
			{
				header('location:register_owner.php');
			} 
		}
		
		//END create owner
		
		//Update vehicle owner
		function registerOwnerEdit($adminSignature,$firstName,$lastName,$email,$userName,$password,$mobileNumber,$is_active,$vehicleOwnerId)
		{
			
			$sql = "UPDATE owner_tbl SET adminSignature='$adminSignature',firstName='$firstName',lastName='$lastName',email='$email',userName='$userName',mobileNumber='$mobileNumber',is_active='$is_active' WHERE vehicleOwnerId='$vehicleOwnerId'";
			
		
			$rs = mysqli_query($this->con,$sql);
			
			if(!$rs)
			{
				die ("Not Updated Owner.".mysqli_error($this->con));
			}

			else
			{
				header('location:register_owner.php');
			} 
		}
		//END update vehicle owner
		//Change owner status
		
			function changeStatusVehicleOwner($is_active,$vehicleOwnerId)
			{
				if($is_active == 1)
				{
					$is_active = 0;
					$sql = "UPDATE owner_tbl SET is_active = '$is_active' WHERE vehicleOwnerId = '$vehicleOwnerId' ";
				}
				
				else
				{
					$is_active = 1;
					$sql = "UPDATE owner_tbl SET is_active = '$is_active' WHERE vehicleOwnerId = '$vehicleOwnerId' ";
				}
				$rs = mysqli_query($this->con,$sql);
				
				if(!$rs)
				{
					die ("Status Not Updated Vehicle Owner.".mysqli_error($this->con));
				}
	
				else
				{
					echo "<script>window.location = 'register_owner.php'</script>";
				} 
		}
		//END owner status
		
		//delete owner
		function deleteVehicleOwner($vehicleOwnerId)
		{
			$sql = "DELETE FROM  owner_tbl WHERE vehicleOwnerId = '$vehicleOwnerId'  ";
			$rs = mysqli_query($this->con,$sql);
				
			if(!$rs)
			{
				die (" Not Deleted Vehicle Owner.".mysqli_error($this->con));
			}
	
			else
			{
				echo "<script>window.location = 'register_owner.php'</script>";
			} 
		}
		//End delete owner
		
		//fetch vehicle owner details for edit
		
		function vehicleOwnerIdEdit($vehicleOwnerIdEdit)
		{
			
			$sql_vehicleOwnerIdEdit = "SELECT * FROM  owner_tbl WHERE vehicleOwnerId = '$vehicleOwnerIdEdit'  ";
			
			$ans_vehicleOwnerIdEdit = mysqli_query($this->con,$sql_vehicleOwnerIdEdit);
			
			$row_vehicleOwnerIdEdit = mysqli_fetch_array($ans_vehicleOwnerIdEdit); 
			
			return $row_vehicleOwnerIdEdit;
		}
		//END fetch vehicle owner details for edit
		
//END of owner

//BEGIN Terms And Conditions 
	//BEGIN ADD TERMS category
	
	function termAndConditionAddInsert($termCategory,$adminSignature)
	{
		$sql = "INSERT into termsandconditions_category(adminSignature,termCategory) VALUES('$adminSignature','$termCategory')";
		
		
		$rs = mysqli_query($this->con,$sql);
		
		if(!$rs)
		{
			die ("Not inserted Terms Category.".mysqli_error($this->con));
		}

		else
		{
			header('location:termsAndConditionList.php');
		} 
	}
	
	//END ADD TERMS category
	//BEGIN ADD TERMS category EDIT
	function termAndConditionAddEdit($termCategory,$adminSignature,$termCategoryId)
	{
		$sql = "UPDATE termsandconditions_category
				SET adminSignature = '$adminSignature',termCategory='$termCategory'
				WHERE termCategoryId='$termCategoryId'";
		
		
		$rs = mysqli_query($this->con,$sql);
		
		if(!$rs)
		{
			die ("Not Updated Terms Category.".mysqli_error($this->con));
		}

		else
		{
			header('location:termsAndConditionList.php');
		} 
	}
	//END ADD TERMS category EDIT
	
	//BEGIN DELETE Terms Category
		function termsAndConditionListDelete($termCategoryId)
		{
			
			$sql = "UPDATE termsandconditions_category SET delete_status=1 WHERE termCategoryId = '$termCategoryId'  ";
			$rs = mysqli_query($this->con,$sql);
				
			if(!$rs)
			{
				die ("Not Deleted.".mysqli_error($this->con));
			}
	
			else
			{
				echo "<script>window.location = 'termsAndConditionList.php'</script>";
			} 
		}
	//END BEGIN DELETE Terms Category
	
	//Change Terms Category STATUS
		 function changeStatus_termsandconditionsList_tbl($is_active,$termCategoryId)
		 {
		 	
		 	if($is_active == 1)
				{
					$is_active = 0;
					$sql = "UPDATE termsandconditions_category SET is_active = '$is_active' WHERE termCategoryId = '$termCategoryId' ";
				}
				
				else
				{
					$is_active = 1;
					$sql = "UPDATE termsandconditions_category SET is_active = '$is_active' WHERE termCategoryId = '$termCategoryId' ";
				}
				$rs = mysqli_query($this->con,$sql);
				
				if(!$rs)
				{
					die ("Status Not Updated Terms And Conditons.".mysqli_error($this->con));
				}
	
				else
				{
					echo "<script>window.location = 'termsAndConditionList.php'</script>";
				}
		 }
	//END Change Terms Category STATUS
	
	//fetch Terms Category details for edit
		
		function termsAndConditionListEdit($termCategoryId)
		{
			
			$sql_termsAndConditionListEdit = "SELECT * FROM  termsandconditions_category WHERE termCategoryId = '$termCategoryId'  ";
			
			$sql_termsAndConditionListEdit = mysqli_query($this->con,$sql_termsAndConditionListEdit);
			
			$sql_termsAndConditionListEdit = mysqli_fetch_array($sql_termsAndConditionListEdit); 
			
			return $sql_termsAndConditionListEdit;
		}
	//END fetch Terms Category details for edit
	
	//Begin Terms And Conditon Insert
	function termAndConditionInsert($adminSignature,$termFor,$is_active,$termDetail,$termCategoryId)
	{
		$sql = "INSERT INTO termsandconditions_tbl(adminSignature,is_active,termDetail,is_taken,termFor,termCategoryId)
				VALUES('$adminSignature','$is_active','$termDetail','$is_active','$termFor','$termCategoryId')";
			
			$rs = mysqli_query($this->con,$sql);
			
		$sql_termsandconditions_category="UPDATE termsandconditions_category SET is_taken='1' WHERE termCategoryId='$termCategoryId'";
		
		$rs = mysqli_query($this->con,$sql_termsandconditions_category);
			
			if(!$rs)
			{
				die ("Not inserted Terms And Conditions.".mysqli_error($this->con));
			}

			else
			{
				header('location:termsAndCondition.php');
			} 
	}
	//END terms and conditions  INSERT
	
	//BEGIN terms and conditons UPDATE
	
	function termAndConditionEdit($termId,$termFor,$is_active,$termDetail,$adminSignature,$termCategoty)
	{
		$sql = "UPDATE termsandconditions_tbl 
				SET adminSignature = $adminSignature, termFor = '$termFor' ,is_active='$is_active',termDetail='$termDetail'  WHERE termId=$termId";
			
			$rs = mysqli_query($this->con,$sql);
			
			if(!$rs)
			{
				die ("Not inserted Terms And Conditions.".mysqli_error($this->con));
			}

			else
			{
				header('location:termsAndCondition.php');
			} 
	}
	//END terms and conditions UPDATE
	
	//Change Terms and condition STATUS
		 function changeStatus_termsandconditions_tbl($is_active,$termId)
		 {
		 	
		 	if($is_active == 1)
				{
					$is_active = 0;
					$sql = "UPDATE termsandconditions_tbl SET is_active = '$is_active' WHERE termId = '$termId' ";
				}
				
				else
				{
					$is_active = 1;
					$sql = "UPDATE termsandconditions_tbl SET is_active = '$is_active' WHERE termId = '$termId' ";
				}
				$rs = mysqli_query($this->con,$sql);
				
				if(!$rs)
				{
					die ("Status Not Updated Terms And Conditons.".mysqli_error($this->con));
				}
	
				else
				{
					echo "<script>window.location = 'termsAndCondition.php'</script>";
				}
		 }
	//END Change Terms and condition STATUS
	
	//BEGIN DELETE Terms and conditions
		function termsAndConditionDelete($termId,$termCategoryId)
		{
			
			$sql = "UPDATE termsandconditions_tbl SET delete_status=1 WHERE termId = '$termId'  ";
			$rs = mysqli_query($this->con,$sql);
			
			$sql_termsandconditions_category = "UPDATE termsandconditions_category SET is_taken='0' WHERE termCategoryId='$termCategoryId'";
			
			$rs = mysqli_query($this->con,$sql_termsandconditions_category);
			
			if(!$rs)
			{
				die ("Not Deleted.".mysqli_error($this->con));
			}
	
			else
			{
				echo "<script>window.location = 'termsAndCondition.php'</script>";
			} 
		}
	//END BEGIN DELETE Terms and conditions
	
	//fetch Terms and conditions details for edit
		
		function termsAndConditionEdit($termId)
		{
			
			$sql_termsAndConditionEdit = "SELECT tnc.*,tncl.termCategory FROM  termsandconditions_tbl tnc LEFT JOIN termsandconditions_category tncl ON tnc.termCategoryId=tncl.termCategoryId WHERE termId = '$termId'  ";
			
			$sql_termsAndConditionEdit = mysqli_query($this->con,$sql_termsAndConditionEdit);
			
			$sql_termsAndConditionEdit = mysqli_fetch_array($sql_termsAndConditionEdit); 
			
			return $sql_termsAndConditionEdit;
		}
	//END fetch Terms and conditions details for edit
	
//End Terms And Conditions	

//BEGIN accept owner registration request
		function acceptOwnerRequest($vehicleOwnerRequestId)
		{
			$sql_insert = "INSERT INTO owner_tbl(userName,firstName,lastName,email,password,mobileNumber,is_active)
					SELECT  userName,firstName,lastName,email,password,mobileNumber,'1'
					FROM vehicleownersignuprequest_tbl
					WHERE vehicleownersignuprequest_tbl.vehicleOwnerRequestId = '$vehicleOwnerRequestId'";
		
			$rs = mysqli_query($this->con,$sql_insert);
			
			if(!$rs)
			{
				die ("Not Inserted Owner Request.".mysqli_error($this->con));
			}

			else
			{
				$sql_delete = "DELETE FROM vehicleownersignuprequest_tbl
								WHERE vehicleOwnerRequestId = '$vehicleOwnerRequestId'";
			
				$rs = mysqli_query($this->con,$sql_delete);
				
				if(!$rs)
				{
					die ("Not Inserted Owner Request.".mysqli_error($this->con));
				}
				
				else
				{
					echo "<script>window.location = 'newOwnerRequest.php'</script>";
				}
			} 		
			
		}
//END accpet owner registration request

//BEGIN INSERT Customer Home Page Banner
		function homePageBanner_Insert($mainText1,$mainText2,$mainText3,$subText,$hrefLink,$hrefText,$bannerImageLink,$adminSignature)
		{
			
			
			if(!empty($_FILES["bannerImageLink"]["name"]))
			{
				$img=$_FILES["bannerImageLink"]["name"];

				//echo $img."</br>";
				$ext = pathinfo($img, PATHINFO_EXTENSION);


				//echo $ext."</br>";
				$img = basename($img,".".$ext);
				//echo $img."</br>";

				$img = rand(1111111111,9999999999).$img.rand(1111111111,9999999999).".".$ext;
				//echo $img."</br>";


				$tmp_name=$_FILES["bannerImageLink"]["tmp_name"];

				if(is_uploaded_file($tmp_name))
				{
					copy($tmp_name,"../customerHomePageBanner/".$img);
					$sql_insert = "INSERT INTO home_page_banner(mainText1,mainText2,mainText3,subText,hrefLink,hrefText,bannerImageLink,adminSignature)
						VALUES('$mainText1','$mainText2','$mainText3','$subText','$hrefLink','$hrefText','$img','$adminSignature')";
				
				}
				
				
				
			}
			
			else
			{
				$sql_insert = "INSERT INTO home_page_banner(mainText1,mainText2,mainText3,subText,hrefLink,hrefText,adminSignature)
					VALUES('$mainText1','$mainText2','$mainText3','$subText','$hrefLink','$hrefText','$adminSignature')";
			}
			$rs = mysqli_query($this->con,$sql_insert);
			
			if(!$rs)
			{
				die ("Not Inserted Banner.".mysqli_error($this->con));
			}
			
			else
			{
				echo "<script>window.location = 'homePageBanner.php'</script>";
			}
		}
//END INSERT Customer Home Page Banner


//BEGIN EDIT Customer Home Page Banner
		function homePageBanner_Edit($mainText1,$mainText2,$mainText3,$subText,$hrefLink,$hrefText,$bannerImageLink,$bannerId,$adminSignature,$bannerImageOldLink)
		{
			
			
			if(!empty($_FILES["bannerImageLink"]["name"]))
			{
				$img=$_FILES["bannerImageLink"]["name"];

				//echo $img."</br>";
				$ext = pathinfo($bannerImageOldLink, PATHINFO_EXTENSION);


				//echo $ext."</br>";
				$img = basename($bannerImageOldLink,".".$ext);
				//echo $img."</br>";

				$img = $bannerImageOldLink.".".$ext;
				//echo $img."</br>";


				$tmp_name=$_FILES["bannerImageLink"]["tmp_name"];

				if(is_uploaded_file($tmp_name))
				{
					//copy($tmp_name,"../customerHomePageBanner/".$img);
					move_uploaded_file($tmp_name,"../customerHomePageBanner/".$img);
					$sql_update = "UPDATE home_page_banner SET mainText1='$mainText1',mainText2='$mainText2',mainText3='$mainText3',subText='$subText',hrefLink='$hrefLink',hrefText='$hrefText',bannerImageLink='$bannerImageLink',adminSignature='$adminSignature' WHERE bannerId ='$bannerId'";
				
				}
			}
			
			else
			{
				$sql_update = "UPDATE home_page_banner SET mainText1='$mainText1',mainText2='$mainText2',mainText3='$mainText3',subText='$subText',hrefLink='$hrefLink',hrefText='$hrefText',adminSignature='$adminSignature' WHERE bannerId ='$bannerId'";
			}
			
			$rs = mysqli_query($this->con,$sql_update);
			
			if(!$rs)
			{
				die ("Not Updated Banner.".mysqli_error($this->con));
			}
			
			else
			{
				echo "<script>window.location = 'homePageBanner.php'</script>";
			}
		}
//END EDIT Customer Home Page Banner

//BEGIN DELETE Home Page Banner
		function deleteHomePageBanner($bannerId)
		{
			$sql = "UPDATE home_page_banner SET delete_status = 1 WHERE  bannerId='$bannerId'";
			
			$rs = mysqli_query($this->con,$sql);
			
			if(!$rs)
			{
				die ("Not Deleted Banner.".mysqli_error($this->con));
			}
			
			else
			{
				echo "<script>window.location = 'homePageBanner.php'</script>";
			}
		}
//END DELETE Home Page Banner
//BEGIN Change banner status
	function changeHomePageBannerStatus($is_active,$bannerId)
	{
		if($is_active == 1)
		{
			$is_active = 0;
		}
		
		else
		{
			$is_active = 1;
		}
			
		$sql = "UPDATE home_page_banner SET is_active = '$is_active' WHERE  bannerId='$bannerId'";
			
			$rs = mysqli_query($this->con,$sql);
			
			if(!$rs)
			{
				die ("Not Updated Banner Status.".mysqli_error($this->con));
			}
			
			else
			{
				echo "<script>window.location = 'homePageBanner.php'</script>";
			}
	}
//END change banner status

//BEGIN FUEL TYPE INSERT
	function fuelType_insert($adminSignature,$fuelType_Name)
	{
		$sql = "INSERT INTO fueltype_tbl(adminSignature,fuelType_Name)
				VALUES('$adminSignature','$fuelType_Name')";
			
			$rs = mysqli_query($this->con,$sql);
			
			if(!$rs)
			{
				die ("Not inserted Fuel.".mysqli_error($this->con));
			}

			else
			{
				header('location:manageFuelList.php');
			} 
	}
//END FUEL type INSERT

//BEGIN FUEL TYPE UPDATE
	function fuelType_update($adminSignature,$fuelType_Name,$fuelType_id)
	{
		$sql = "UPDATE fueltype_tbl
				SET adminSignature = '$adminSignature',
				fuelType_Name = '$fuelType_Name'
				WHERE fuelType_id = '$fuelType_id'";
			
			$rs = mysqli_query($this->con,$sql);
			
			if(!$rs)
			{
				die ("Not Updated Fuel.".mysqli_error($this->con));
			}

			else
			{
				header('location:manageFuelList.php');
			} 
	} 
//END FUEL type UPDATE

//BEGIN FUEL TYPE DELETE
	function deleteFuel($fuelType_id)
	{
		$sql = "UPDATE fueltype_tbl
				SET delete_status = '1'
				WHERE fuelType_id = '$fuelType_id'";
		
			$rs = mysqli_query($this->con,$sql);
			
			if(!$rs)
			{
				die ("Not Delete Fuel.".mysqli_error($this->con));
			}

			else
			{
				echo "<script>window.location = 'manageFuelList.php'</script>";
			} 
	}
//END FUEL type DELETE
//BEGIN FUEL TYPE STATUS
	function changeStatusFuel($stat,$fuelType_id)
	{
		$sql = "UPDATE fueltype_tbl
				SET is_active = '$stat'
				WHERE fuelType_id = '$fuelType_id'";
		
			$rs = mysqli_query($this->con,$sql);
			
			if(!$rs)
			{
				die ("Not Delete Fuel.".mysqli_error($this->con));
			}

			else
			{
				echo "<script>window.location = 'manageFuelList.php'</script>";
			} 
	}
//END FUEL type STATUS
//BEGIN GET FUEL ID
	function getFuelId($fuelType_id)
	{
		$sql_getFuelId = "SELECT * FROM  fueltype_tbl 
						WHERE fuelType_id = '$fuelType_id'  ";
			
			$ans_getFuelId = mysqli_query($this->con,$sql_getFuelId);
			
			$row_getFuelId = mysqli_fetch_array($ans_getFuelId); 
			
			return $row_getFuelId;
	}
//END GET FUEL ID
	}//	END of CLASS
	
	$obj = new dbclass();
	
	

	if(isset($_POST['btnLogin']))
	{
//		echo "button pressed";
		$uname = $_POST['userName'];
		$pwd = $_POST['password'];
		
		
		$obj -> getLogin($uname,$pwd);	
	}
	
	
	if(isset($_POST['update_admin']))
	{

		$uname = $_POST['username'];
		$full_name = $_POST['full_name'];
		$email = $_POST['email'];
		$mobile = $_POST['mobile'];

		$file = $_FILES['imgUpload'];

//		echo $file;
		$obj -> update_adm($uname,$full_name,$email,$mobile,$file);	
	}
	if (isset($_POST['changePwd'])) {
		
		$pwd =$_POST['pwd'];
		$pwd2 =$_POST['pwd2'];
		$uname = $_POST['uname'];

		session_start();

		if( $pwd == $pwd2 ){
			if ($pwd != $row['password']) {
				$obj -> update_adminPwd($pwd,$pwd2,$uname);
			}

			else{
				$_SESSION['msg'] = "New password cannot be your old password";
				
				
				
			header('Location:changePwd.php');
			}

			
		}
		else{
			$_SESSION['msg'] = "Password donot match";
			header('Location:changePwd.php');
		}
	}

	if (isset($_POST['passwordReset'])) {
		
		$pwd =$_POST['pwd'];
		$pwd2 =$_POST['pwd2'];
		$uname = $_POST['uname'];
		$uname = base64_decode($uname);

		if( $pwd == $pwd2 ){

			$obj -> passwordAdminReset($pwd,$pwd2,$uname);
					
		}
		else{

			die('password do not match');
		}
	}

	if(isset($_POST['forgotAdminPassword']))
	{

		$userName = $_POST['userName'];
		$recoveryemail = $_POST['recoveryemail'];
		
	
		$obj -> forgotAdminPassword($userName,$recoveryemail);	
	}
	
	
	//Start Vehicle Type
	if(isset($_POST['btnVehicleType']))
	{
		$vehicle_type_id = $_POST['vehicle_type_id'];
		$vehicle_type = $_POST['vehicle_type'];
		$active_date = $_POST['active_date'];
		$active_date = "";
		
		if($vehicle_type_id == "")
		{
				$obj -> insertVehicleType($vehicle_type,$active_date);
		
		}
		else
		{
			$obj -> updateVehicleType($vehicle_type_id,$vehicle_type,$active_date);
		
		}
	}
	//End Vehicle Type
	
	//Start Vehicle category
	if(isset($_POST['btnVehicleCategory']))
	{
		$vehicle_type_id = $_POST['vehicle_type_id'];
		$vehicle_category_id = $_POST['vehicle_category_id'];
		$vehicle_category = $_POST['vehicle_category'];

		
		
		if($vehicle_category_id == "")
		{
			$obj -> insertVehicleCategory($vehicle_type_id,$vehicle_category);
		
		}
		else
		{
			
			$obj -> updateVehicleCategory($vehicle_category_id,$vehicle_type_id,$vehicle_category);
		
		}
	}
	//End Vehicle category
	
//owner BEGIN
	
	//create owner
	if(isset($_POST['ownerRegister']))
	{
		$vehicleOwnerId = $_POST['vehicleOwnerId'];
		$adminSignature = $_POST['adminSignature'];
		$firstName = $_POST['firstName'];
		$lastName = $_POST['lastName'];
		$email = $_POST['email'];
		$userName = $_POST['userName'];
		$password = $_POST['password'];
		$mobileNumber = $_POST['mobileNumber'];
		$is_active = $_POST['is_active'];
		
		
		if(!isset($_POST['vehicleOwnerId']))
		{
			
			$obj -> registerOwner($adminSignature,$firstName,$lastName,$email,$userName,$password,$mobileNumber,$is_active);
		}
		
		else
		{
			
			$obj -> registerOwnerEdit($adminSignature,$firstName,$lastName,$email,$userName,$password,$mobileNumber,$is_active,$vehicleOwnerId);
		}
	
		
	}
	//END create owner
		
	
	
//End owner
//BEGIN ADD TERMS Category
	if(isset($_POST['termAndConditionAdd']))
	{
		$termCategoryId = $_POST['termCategoryId'];
		$termCategory = $_POST['termCategory'];
		$adminSignature = $_POST['adminSignature'];
		
		if(!isset($_POST['termCategoryId']))
		{
			
			$obj -> termAndConditionAddInsert($termCategory,$adminSignature);
		}
		
		else
		{
			
			$obj -> termAndConditionAddEdit($termCategory,$adminSignature,$termCategoryId);
		}
	}
//END ADD TERMS category
//Terms And Conditions
	if(isset($_POST['termAndCondition']))
	{
		$termId = $_POST['termId'];
		$termCategoryId = $_POST['termCategoryId'];
		$termFor = $_POST['termFor'];
		$is_active = $_POST['is_active'];
		$termDetail = $_POST['termDetail'];
		$adminSignature = $_POST['adminSignature'];
		
		if(!isset($_POST['termId']))
		{
			
			$obj -> termAndConditionInsert($adminSignature,$termFor,$is_active,$termDetail,$termCategoryId);
		}
		
		else
		{
		
			$obj -> termAndConditionEdit($termId,$termFor,$is_active,$termDetail,$adminSignature,$termCategoryId);
		}
	
		
	}
//End Terms And Conditions

	//Home Page Banner
	if(isset($_POST['homePageBanner_btn']))
	{
	
		$mainText1 = $_POST['mainText1'];
		$mainText2 = $_POST['mainText2'];
		$mainText3 = $_POST['mainText3'];
		$subText = $_POST['subText'];
		$hrefLink = $_POST['hrefLink'];
		$hrefText = $_POST['hrefText'];
		$bannerImageLink = $_FILES['bannerImageLink'];
		$bannerId = $_POST['bannerId'];
		$adminSignature = $_POST['adminSignature'];
		$bannerImageOldLink = $_POST['oldImgName'];

		
		if(!isset($_POST['bannerId']))
		{
			
			$obj -> homePageBanner_Insert($mainText1,$mainText2,$mainText3,$subText,$hrefLink,$hrefText,$bannerImageLink,$adminSignature);
		}
		
		else
		{
			$obj -> homePageBanner_Edit($mainText1,$mainText2,$mainText3,$subText,$hrefLink,$hrefText,$bannerImageLink,$bannerId,$adminSignature,$bannerImageOldLink);
		}
	
		
	}
//END Home Page Banner

//BEGIN Fuel Type

	if(isset($_POST['fuelType_btn']))
	{
		$fuelType_Name = $_POST['fuelType_Name'];
		$fuelType_id = $_POST['fuelType_id'];
		$adminSignature = $_POST['adminSignature'];
		//die("VALUE:".$fuelType_id);
		if(!isset($_POST['fuelType_id']))
		{
			
			$obj -> fuelType_insert($adminSignature,$fuelType_Name);
		}
		
		else
		{
			$obj -> fuelType_update($adminSignature,$fuelType_Name,$fuelType_id);
		}
	}
//END Fuel Type



	
	
?>