<?php 
	include_once('header.php');
?>
<div class="row">
					<!-- col-md-12 Starts Here -->
					<div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">Editor</h3>
								<small>Simple Editor</small>
							</div>
							<div class="panel-body">
								<form action="#" novalidate>
									<div class="form-group">
										<textarea name="text" class="summernote" id="contents" title="Contents" required=""></textarea>
									</div>
									<button type="submit" class="btn btn-success">submit</button>
								</form>
							</div>
						</div>
					</div>
					<!-- #col-md-12 Ends Here -->
				</div>
										
<?php
	include_once('footer.php');
?>