<?php
include_once('encrypt_decrypt.php');
include_once('dbclass.php');
if($_POST)
{
//	die("HERE");
	if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
        
        $output = json_encode(array( //create JSON data
            'type'=>'error', 
            'text' => 'Sorry Request must be Ajax POST'
        ));
        die($output); //exit script outputting json data
    } 
//	include_once('admin_blezo/connection.php');
	$cname     = filter_var($_POST["cname"]);											
	$email     = filter_var($_POST["email"], FILTER_SANITIZE_EMAIL);
	$mobile     = filter_var($_POST["mobile"]);
	$password    = filter_var($_POST["password"]);
	
	$enc_pwd = my_simple_crypt($password,'e');
    //additional php validation
	if(!$cname){ //cname validation
        $output = json_encode(array('type'=>'error', 'text' => 'Please enter a valid Full Name!'));
        die($output);
    }
    if(!filter_var($email, FILTER_VALIDATE_EMAIL)){ //email validation
        $output = json_encode(array('type'=>'error', 'text' => 'Please enter a valid Email Id!'));
        die($output);
    }
	if(!$mobile){ //mobile validation
        $output = json_encode(array('type'=>'error', 'text' => 'Please enter a valid Mobile Number!'));
        die($output);
    }
			
	$sql = "INSERT INTO customer_tbl(fullName,password,email,mobile) VALUES('$cname','$enc_pwd','$email','$mobile')";
	$result = mysqli_query( $obj->con,$sql );
	if(!$result)
	{	
		if (mysqli_errno($obj->con) == 1062)
		{
			$output = json_encode(array('type'=>'error', 'text' => 'Great! You are already register.'));
        	die($output);
		}	
		else
		{		
			$output = json_encode(array('type'=>'error', 'text' => 'Oops..! Something went wrong.'));
        	die($output);				
		}
	}	
	else
	{
		$output = json_encode(array('type'=>'message', 'text' => 'You are successfully Register.'));
        die($output);
	}
}
?>